package com.ieci.cepsa17.ext.templating;

import java.util.Locale;

import org.apache.log4j.Logger;

import com.ieci.cepsa17.wem.comun.util.CIUtil;
import com.ieci.cepsa17.util.StringUtil;
import com.vignette.ext.templating.util.RequestContext;

import es.ieci.cepsa.utilities.ext.templating.RequestContextUtil;
import es.ieci.cepsa.utilities.ext.templating.link.RequestContextModifierImpl;

public class Cepsa17RCMImpl extends RequestContextModifierImpl {
	// Extendemos el RCM del framework antiguo para simplificar la implementacion y asegurar la convivencia

	private static Logger logger = Logger.getLogger(Cepsa17RCMImpl.class);
	  
	public void populate(RequestContext requestContext){
		// Los sites nuevos tienen que tener un literal "esNuevo" cuyo valor (en todos los idiomas) sea distinto de "no"
		String esNuevo = CIUtil.getLiteral(requestContext, "esNuevo", "no"); 
		if(logger.isDebugEnabled()) {
			logger.debug("Site esNuevo: "+ esNuevo);
			RequestContextUtil.debugRequestContext(requestContext);
		}
		if ("no".equals(esNuevo)) {
			// Invocamos el control de variables de entorno del framework antiguo
			super.populate(requestContext);
		} else {
			// Parece que esto deberia establecerse por defecto pero por algun motivo, no lo hace
			String lang = requestContext.getParameter("vgnextlocale");			
			if (lang != null) 
				requestContext.setLocale(StringUtil.parseLocale(lang));
		}

	}
}
