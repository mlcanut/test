package com.ieci.cepsa17.comercial.wem.guialubricacion.view.bean;

import java.io.Serializable;
import com.ieci.cepsa17.wem.constants.BuscadorGuiaLubricacionConstants;

public class FormBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4025027415719668685L;
	
	private String categoria;
	private String matricula;
	private String tipo;
	private String marca;
	private String modelo;
	private String texto;
	private String idioma;
	private String anioBusqueda;
	
	// Caché de búsqueda por matrícula
	public String createCacheKeyMatricula() {
		return getMatricula();
	}
		
	// Caché lista de Categorías
	public String createCacheKeyListaCategorias() {
		return "CAT" + "-" + getIdioma();
	}
	
	// Caché de búsqueda de Tipos por Marca y/o Modelo
	public String createCacheKeyTiposMM() {
		if (null != getAnioBusqueda())
			return getCategoria() + "-" + getTexto() + "-" + getIdioma() + "-" + getAnioBusqueda();
		
		return getCategoria() + "-" + getTexto() + "-" + getIdioma();
	}
	
	// Caché de lista de Marcas
	public String createCacheKeyListaMarcas() {
		return getCategoria() + "-" + getIdioma();
	}
	
	// Caché de lista de Modelos
	public String createCacheKeyListaModelos() {
		return getMarca() + "-" + getIdioma();
	}
	
	// Caché de lista de Tipos
	public String createCacheKeyListaTipos() {
		return getModelo() + "-" + getIdioma();
	}

	// Caché de Imagen de Marca (sin idioma)
	public String createCacheKeyImagenMarca() {
		return"IMG" + "-"  +  getMarca();
	}
	
	// Caché de Imagen de Modelo (sin idioma)
	public String createCacheKeyImagenModelo() {
		return "IMG" + "-"  + getModelo();
	}

	// Caché de Imagen de Tipo (sin idioma)
	public String createCacheKeyImagenTipo() {
		return "IMG" + "-"  + getTipo();
	}
	
	// Caché de búsqueda de Tipos por Marca y/o Modelo Predictivo
	public String createCacheKeyTiposMMP() {
		return "P" + "-"  + getCategoria() + "-" + getTexto() + "-" + getIdioma();
	}
	
	// Caché de Recomendador
	public String createCacheKeyRecomendacion() {
		return "REC" + "-"  + getTipo() + "-" + getIdioma();
	}
	
	// Caché de lista de Marcas Populares
	public String createCacheKeyListaMarcasPopulares() {
		return "MP" + "-" + getIdioma();
	}
	
	
	// Caché de Map de Marcas Populares
	public String createCacheKeyMapTiposMM() {
		return "MAP" + "-" + "MP" + "-" + getIdioma();
	}
	
	
	/**
	 * Genera una instancia de <code>FormBean</code> a partir de los valores
	 * enviados por el formulario.
	 */
	
	public static FormBean createInstanceOlyslager() {
		FormBean result = new FormBean();

		return result;
	}

	public static FormBean createInstanceOlyslager(int tipoBusqueda, String identificador1, String identificador2) {
		FormBean result = new FormBean();
		
		if (tipoBusqueda == BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_MARCAS)	// Búsqueda de marcas por categoría
			result.setCategoria(identificador1);
		else if (tipoBusqueda == BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_MODELOS)	// Búsqueda de modelos por marca
			result.setMarca(identificador1);
		else if (tipoBusqueda == BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_TIPOS)		// Búsqueda de tipos por modelo
			result.setModelo(identificador1);
		else if (tipoBusqueda == BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_TIPOS_POR_MARCA_MODELO || 
				tipoBusqueda == BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_TIPOS_PREDICTIVO ) {	// Búsqueda de tipos por categoría y texto introducido por el usuario
			result.setCategoria(identificador1);
			result.setTexto(identificador2);
		}	
		else if (tipoBusqueda == BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_IMAGEN_MARCA)		// Búsqueda de imagen por marca
			result.setMarca(identificador1);
		else if (tipoBusqueda == BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_IMAGEN_MODELO)		// Búsqueda de imagen por modelo
			result.setModelo(identificador1);
		else if (tipoBusqueda == BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_IMAGEN_TIPO)		// Búsqueda de imagen por tipo
			result.setTipo(identificador1);
		else if (tipoBusqueda == BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_TIPOS_POR_MATRICULA)	// Búsqueda de tipo por matrícula
			result.setMatricula(identificador1);
		else if (tipoBusqueda == BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_MARCAS_MAS_BUSCADAS)	// Búsqueda de marcas más buscadas por categoría
			result.setCategoria(identificador1);
		else if (tipoBusqueda == BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_RECOMENDACION)	// Búsqueda de lubricantes recomendados por tipo
			result.setTipo(identificador1);
			
		return result;
	}
	
	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getAnioBusqueda() {
		return anioBusqueda;
	}

	public void setAnioBusqueda(String anioBusqueda) {
		this.anioBusqueda = anioBusqueda;
	}

}