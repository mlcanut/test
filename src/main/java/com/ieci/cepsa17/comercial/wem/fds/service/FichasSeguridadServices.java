package com.ieci.cepsa17.comercial.wem.fds.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.tempuri.EFicha;
import org.tempuri.EParamObtFichasProductos;

import com.ieci.cepsa17.comercial.wem.fds.view.bean.FormBean;
import com.ieci.cepsa17.wem.constants.BuscadorFichasSeguridadConstants;
import com.opensymphony.oscache.base.NeedsRefreshException;
import com.opensymphony.oscache.general.GeneralCacheAdministrator;

import es.cepsa.ws.fds.IFichasSeguridadService;
import es.cepsa.ws.fds.impl.FichasSeguridadServiceImpl;

public class FichasSeguridadServices {

	private IFichasSeguridadService service;
	private static FichasSeguridadServices instance;
	private GeneralCacheAdministrator cache;
	private static Logger logger = Logger.getLogger(FichasSeguridadServices.class);

	private FichasSeguridadServices(String endpoint, String name, String password) {

		if (logger.isInfoEnabled())
			logger.info("FichasSeguridadServices -- Endpoint - " + endpoint + " UserName - " + name);
		service = new FichasSeguridadServiceImpl(endpoint, name, password);
		cache = new GeneralCacheAdministrator();

	}

	public static FichasSeguridadServices getInstance(String endpoint, String name, String password) {

		if (instance == null)
			return new FichasSeguridadServices(endpoint, name, password);
		else
			return instance;

	}

	@SuppressWarnings("unchecked")
	public List<EFicha> obtenerFichas(FormBean request) {

		long millis1 = System.currentTimeMillis() % 1000;
		if (logger.isDebugEnabled()) {
			logger.debug("FichasSeguridadServices -- obtenerFichas :: createCacheKey" + request.createCacheKey());
		}

		List<EFicha> results = null;
		try {
			results = (List<EFicha>) cache.getFromCache(request.createCacheKey(), BuscadorFichasSeguridadConstants.TTL);
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("FichasSeguridadServices -- Recuperando elemento de la cache :: "
						+ request.createCacheKey() + " en " + (millis2 - millis1));
			}
		} catch (NeedsRefreshException e) {
			// Los datos de la cache no existen o han caducado
			cache.cancelUpdate(request.createCacheKey());
			results = service.obtFichasPorParametros(convertToFichasProductos(request));
			cache.putInCache(request.createCacheKey(), results);
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("FichasSeguridadServices -- Los datos no existen en la cache :: "
						+ request.createCacheKey() + " en " + (millis2 - millis1));
			}
		}

		return results;

	}

	public byte[] obtContenidoFichaPorID(String id) {

		if (logger.isDebugEnabled()) {
			logger.debug("FichasSeguridadServices -- obtContenidoFichaPorID :: " + id);
		}

		return service.obtContenidoFichaPorID(id);

	}

	private EParamObtFichasProductos convertToFichasProductos(FormBean request) {

		EParamObtFichasProductos aux = new EParamObtFichasProductos();

		if (logger.isDebugEnabled()) {
			logger.debug("FichasSeguridadServices -- convertToFichasProductos");
		}

		if (request != null) {

			aux.setCAS(request.getCas());
			aux.setCodigoCepsa(request.getCodCepsa());
			aux.setCodigoProducto(request.getCodProducto());
			aux.setCompania(request.getCompania());
			aux.setIdiomaAtrion(request.getIdioma());

			aux.setNombreComercial(request.getNombreComercial());
			aux.setNombreProducto(request.getNombreProducto());
			aux.setTipoFicha(request.getTipoFicha());
			if (request.getfValidacionD() != null)
				aux.setFechaValidacionDesde(request.getfValidacionD());
			if (request.getfValidacionD() != null)
				aux.setFechaValidacionHasta(request.getfValidacionD());

		}

		return aux;

	}

}