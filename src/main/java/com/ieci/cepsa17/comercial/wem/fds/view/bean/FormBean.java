package com.ieci.cepsa17.comercial.wem.fds.view.bean;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.ieci.cepsa17.wem.constants.BuscadorFichasSeguridadConstants;

public class FormBean implements Serializable {

	private static final long serialVersionUID = -32648235190861092L;

	private String nombreProducto;
	private String nombreComercial;
	private String codCepsa;
	private String codProducto;
	private String cas;
	private String compania;
	private String idioma;
	private String tipoFicha;
	private XMLGregorianCalendar fValidacionD;
	private XMLGregorianCalendar fValidacionH;

	// Backup para que cuando se muestra lo que envio el formulario salga en
	// formato dd/mm/aaaa
	private String fValidacionDOriginal;
	private String fValidacionHOriginal;

	public String createCacheKey() {
		return getNombreProducto() + "-" + getCodCepsa() + "-" + getCas() + "-" + getCompania() + "-" + getIdioma();
	}

	/**
	 * Genera una instancia de <code>FormBean</code> a partir de los valores
	 * enviados por el formulario.
	 */
	public static FormBean createInstanceFDS(String nombreProducto, String codigoCepsa, String cas, String compania,
			String idioma) {

		FormBean result = new FormBean();
		result.setNombreProducto(nombreProducto);
		result.setCodCepsa(codigoCepsa);
		result.setCas(cas);
		result.setCompania(compania);
		result.setIdioma(idioma);

		// Solo buscamos FDS
		result.setTipoFicha(BuscadorFichasSeguridadConstants.TIPO_FICHA_FDS);
		return result;

	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public String getCodCepsa() {
		return codCepsa;
	}

	public void setCodCepsa(String codCepsa) {
		this.codCepsa = codCepsa;
	}

	public String getCodProducto() {
		return codProducto;
	}

	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}

	public String getCas() {
		return cas;
	}

	public void setCas(String cas) {
		this.cas = cas;
	}

	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getTipoFicha() {
		return tipoFicha;
	}

	public void setTipoFicha(String tipoFicha) {
		this.tipoFicha = tipoFicha;
	}

	public XMLGregorianCalendar getfValidacionD() {
		return fValidacionD;
	}

	public void setfValidacionD(XMLGregorianCalendar fValidacionD) {
		this.fValidacionD = fValidacionD;
	}

	public XMLGregorianCalendar getfValidacionH() {
		return fValidacionH;
	}

	public void setfValidacionH(XMLGregorianCalendar fValidacionH) {
		this.fValidacionH = fValidacionH;
	}

	public String getfValidacionDOriginal() {
		return fValidacionDOriginal;
	}

	public void setfValidacionDOriginal(String fValidacionDOriginal) {
		this.fValidacionDOriginal = fValidacionDOriginal;
	}

	public String getfValidacionHOriginal() {
		return fValidacionHOriginal;
	}

	public void setfValidacionHOriginal(String fValidacionHOriginal) {
		this.fValidacionHOriginal = fValidacionHOriginal;
	}

	public static XMLGregorianCalendar stringToXMLGregorianCalendar(String fecha) {

		if (fecha != null && !"".equals(fecha)) {
			Date dob = null;
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			try {
				dob = df.parse(fecha);
			} catch (ParseException e1) {
				return null;
			}
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(dob);
			try {
				return DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			} catch (DatatypeConfigurationException e) {
				return null;
			}
		}

		return null;

	}

}