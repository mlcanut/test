package com.ieci.cepsa17.comercial.wem.guialubricacion.service;

import java.rmi.RemoteException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.ieci.cepsa17.comercial.wem.guialubricacion.view.bean.FormBean;
import com.ieci.cepsa17.wem.constants.BuscadorGuiaLubricacionConstants;
import com.opensymphony.oscache.base.CacheEntry;
import com.opensymphony.oscache.base.NeedsRefreshException;
import com.opensymphony.oscache.general.GeneralCacheAdministrator;

import es.cepsa.ws.olyslagerapi.IOlyslager;
import es.cepsa.ws.olyslagerapi.OlyslagerImpl;
import es.cepsa.ws.olyslagerapi.beans.ArrayCategory;
import es.cepsa.ws.olyslagerapi.beans.ArrayMake;
import es.cepsa.ws.olyslagerapi.beans.ArrayModel;
import es.cepsa.ws.olyslagerapi.beans.ArrayType;
import es.cepsa.ws.olyslagerapi.beans.Image;
import es.cepsa.ws.olyslagerapi.beans.Recommendation;

public class GuiaLubricantesServices {

	private IOlyslager service;
	private static GuiaLubricantesServices instance;
	//private static final GuiaLubricantesServices instance = null;
	//private static final GeneralCacheAdministrator cache = new GeneralCacheAdministrator();
	public GeneralCacheAdministrator cache;
	
	private static Logger logger = Logger.getLogger(GuiaLubricantesServices.class);
	
	private GuiaLubricantesServices(String endPoint, boolean basicAuthentication, String userName, String password, boolean proxy,
			String proxyHost, String proxyPort, String proxyUser, String proxyPassword) {

		if (logger.isDebugEnabled())
			logger.debug("GuiaLubricantesServices --> Endpoint - " + endPoint + " UserName - " + userName + " Password - " + password + " Proxy - " + proxy + " ProxyHost - " + proxyHost +
					"ProxyPort - " + proxyPort + " ProxyUser - " + proxyUser  + " ProxyPassword - " + proxyPassword);
		
		service = new OlyslagerImpl(endPoint, basicAuthentication, userName, password, proxy, proxyHost, proxyPort, proxyUser, proxyPassword);
		// service = new OlyslagerImpl(endPoint, basicAuthentication, userName, password, proxy, proxyHost, proxyPort, proxyUser, proxyPassword, timeOut); AMH
		cache = new GeneralCacheAdministrator();
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
		String flush = resourceBundle.getString(BuscadorGuiaLubricacionConstants.FLUSH_CACHE);
		if (logger.isDebugEnabled()) {
			logger.debug("GuiaLubricantesServices -- Flush leído de config.properties --> " + flush);
		}
		
		if (BuscadorGuiaLubricacionConstants.HAY_FLUSH_CACHE.equals(flush)) {
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices obtenerImagenMarca -- ANTES FLUSH ");
			}
			cache.flushAll();
			
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices obtenerImagenMarca -- DESPUES FLUSH ");
			}	
		}	
	} 
	
	public GeneralCacheAdministrator getCache() {
		return cache;
	}

	public void setCache(GeneralCacheAdministrator cache) {
		this.cache = cache;
	}

	public static GuiaLubricantesServices getInstance(String endPoint, boolean basicAuthentication, String userName, String password, boolean proxy,
			String proxyHost, String proxyPort, String proxyUser, String proxyPassword) {	// , String timeOut

		if (instance == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices --> getInstance NULO");
			}
			
			instance =  new GuiaLubricantesServices(endPoint, basicAuthentication, userName, password, proxy, proxyHost, proxyPort, proxyUser, proxyPassword);	//, String timeOut
		}	
		else  {
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices --> getInstance NO NULO " + instance);
			}	
		}
		
		return instance;
	} 

	public ArrayCategory obtenerCategorias(FormBean request) {
		long millis1 = System.currentTimeMillis() % 1000;
		if (logger.isDebugEnabled()) {
			logger.debug("GuiaLubricantesServices::obtenerCategorias -- createCacheKey" + request.createCacheKeyListaCategorias());
		}

		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
		ArrayCategory results = null;
		try {			
			String TTL = resourceBundle.getString(BuscadorGuiaLubricacionConstants.TTL);
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerCategorias -- Recuperando tiempo cache --> "  + TTL);
			}
			
			results = (ArrayCategory) cache.getFromCache(request.createCacheKeyListaCategorias(), Integer.parseInt(TTL));
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerCategorias -- Recuperando elemento de la cache :: " + request.createCacheKeyListaCategorias() + " en " + (millis2 - millis1) + " milisegundos");
			}
		} 
		catch (NeedsRefreshException e) {
			logger.debug("GuiaLubricantesServices --> Los datos de la cache no existen o han caducado"); 
			// Los datos de la cache no existen o han caducado
			cache.cancelUpdate(request.createCacheKeyListaCategorias());
			try {			
				
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerCategorias resourceBundle --> " + resourceBundle);
				}

				String usuario = resourceBundle.getString(BuscadorGuiaLubricacionConstants.USER_NAME);
				String password = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PASS);
				String idioma = request.getIdioma();
				
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerCategorias -- Datos de conexión con Olyslager:: usuario --> " +  usuario + " password --> " + password + " idioma --> " + idioma); 
				}
				
				results = service.getCategories(usuario, password, idioma);
				if (logger.isDebugEnabled()) {
					if (null != results.getCategories())
						logger.debug("GuiaLubricantesServices::obtenerCategorias results --> " + results.getCategories());
					else
						logger.debug("GuiaLubricantesServices::obtenerCategorias results -- NO se han recuperado categorias");
				}
			} 
			catch (RemoteException e1) {
				logger.error("ERROR GRAVE AL OBTENER LISTA CATEGORIAS - GuiaLubricantesServices : obtenerCategorias " + e1, e1); 
			}
			
			// Sólo guardamos en caché si tenemos un resultado no nulo (para evitar problemas de conexión y no poder actualizar los datos)
			if (null != results && null != results.getCategories()) {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerCategorias -- Los resultados se guardan en caché");
				}
				
				cache.putInCache(request.createCacheKeyListaCategorias(), results);
			}	
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerCategorias -- Los resultados no se guardan en caché por no se han recuperado datos");
				}
			}	
			
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerCategorias --> Los datos no existen en la cache :: " + request.createCacheKeyListaCategorias() + " en " + (millis2 - millis1) + " milisegundos");
			}

		}

		return results;
	}
	
	public ArrayType obtenerTiposPorMarcaModelo(FormBean request) {
		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
		ArrayType results = null;
		long millis1 = System.currentTimeMillis() % 1000;
		
		try {
			String TTL = resourceBundle.getString(BuscadorGuiaLubricacionConstants.TTL);
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModelo -- Recuperando tiempo cache --> "  + TTL);
			}
			
//			String indParametro = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PARAMETRIZACION_ANYO_CACHE);
//			if (logger.isDebugEnabled()) {
//				logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModelo -- Indicador parametrización búsqueda --> "  + indParametro);
//			}
//			
//			String anyoBusqueda = null; 
//			
//			if (BuscadorGuiaLubricacionConstants.HAY_PARAMETRIZACION_ANYO.equals(indParametro)) {
//				anyoBusqueda = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PARAMETRO_ANYO);
//				if (logger.isDebugEnabled()) {
//					logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModelo -- Año parametrización búsqueda --> "  + anyoBusqueda);
//				}
//				
//				request.setAnioBusqueda(anyoBusqueda);
//			}

			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModelo -- createCacheKey " + request.createCacheKeyTiposMM());
			}
						
			results = (ArrayType) cache.getFromCache(request.createCacheKeyTiposMM(), Integer.parseInt(TTL));
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModelo --> Recuperando elemento de la cache :: " + request.createCacheKeyTiposMM() + " en " + (millis2 - millis1) + " milisegundos");
			}
		} 
		catch (NeedsRefreshException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModelo --> Los datos de la cache no existen o han caducado"); 
			}
			
			// Los datos de la cache no existen o han caducado
			cache.cancelUpdate(request.createCacheKeyTiposMM());
			
			try {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModelo resourceBundle --> " + resourceBundle);
				}

				String usuario = resourceBundle.getString(BuscadorGuiaLubricacionConstants.USER_NAME);
				String password = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PASS);
				String idioma = request.getIdioma();
				
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerCategorias -- Datos de conexión con Olyslager:: usuario --> " +  usuario + " password --> " + password + " idioma --> " + idioma + " -- Categoria --> " + request.getCategoria() + " -- Texto --> " + request.getTexto()); 
				}
				
				results = service.getTypesFromSearch(usuario, password, idioma, request.getTexto(), request.getCategoria(), null); //request.getAnioBusqueda());
				if (logger.isDebugEnabled()) {
					if (null != results.getTypes())
						logger.debug("GuiaLubricantesServices --> " + results.getTypes());
					else
						logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModelo results -- NO se han recuperado tipos");
				}	
			} 
			catch (RemoteException e1) {
				logger.error("ERROR GRAVE AL OBTENER LISTA TIPOS BUSQUEDA - GuiaLubricantesServices : obtenerTiposPorMarcaModelo " + e1, e1); 
			}

			if (null != results && null != results.getTypes()) {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModelo -- Los resultados se guardan en caché");
				}
				
				cache.putInCache(request.createCacheKeyTiposMM(), results);
			}
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModelo -- Los resultados no se guardan en caché por no se han recuperado datos");
				}
			}	
			
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModelo --> Los datos no existen en la cache :: " + request.createCacheKeyTiposMM() + " en " + (millis2 - millis1) + " milisegundos");
			}
		}

		return results;
	}	
	
	public ArrayMake obtenerListaMarcas(FormBean request) {
		long millis1 = System.currentTimeMillis() % 1000;
		if (logger.isDebugEnabled()) {
			logger.debug("GuiaLubricantesServices::obtenerListaMarcas -- obtenerListaMarcas :: createCacheKey " + request.createCacheKeyListaMarcas());
		}
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
		ArrayMake results = null;
		try {
			String TTL = resourceBundle.getString(BuscadorGuiaLubricacionConstants.TTL);
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerListaMarcas -- Recuperando tiempo cache --> "  + TTL);
			}
			
			results = (ArrayMake) cache.getFromCache(request.createCacheKeyListaMarcas(), Integer.parseInt(TTL));
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerListaMarcas --> Recuperando elemento de la cache :: " + request.createCacheKeyListaMarcas() + " en " + (millis2 - millis1) + " milisegundos");
			}
		} 
		catch (NeedsRefreshException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerListaMarcas --> Los datos de la cache no existen o han caducado"); 
			}
			
			// Los datos de la cache no existen o han caducado
			cache.cancelUpdate(request.createCacheKeyListaMarcas());
			
			try {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion resourceBundle --> " + resourceBundle);
				}

				String usuario = resourceBundle.getString(BuscadorGuiaLubricacionConstants.USER_NAME);
				String password = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PASS);
				String categoria = request.getCategoria();
				String idioma = request.getIdioma();
								
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerListaMarcas -- Datos de conexión con Olyslager:: Usuario --> " +  usuario + " -- Password --> " + password + " -- Idioma --> " + idioma + " -- Categoria --> " + categoria); 
				}
	
				int intCategoria = Integer.parseInt(categoria);

				results = service.getMakes(usuario, password, idioma, intCategoria);
				if (logger.isDebugEnabled()) {
					if (null != results.getMakes())
						logger.debug("GuiaLubricantesServices::obtenerListaMarcas -- resultado --> " + results.getMakes());
					else
						logger.debug("GuiaLubricantesServices::obtenerListaMarcas results -- NO se han recuperado marcas");
				}	
			} 
			catch (RemoteException e1) {
				logger.error("ERROR GRAVE AL OBTENER LISTA TIPOS BUSQUEDA RemoteException - GuiaLubricantesServices : obtenerListaMarcas " + e1, e1); 
			}
			catch (Exception e2) {
				logger.error("ERROR GRAVE AL OBTENER LISTA TIPOS BUSQUEDA Exception - GuiaLubricantesServices : obtenerListaMarcas " + e2, e2); 
			}
			
			// Sólo guardamos en caché si tenemos un resultado no nulo (para evitar problemas de conexión y no poder actualizar los datos)
			if (null != results && null != results.getMakes()) {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerListaMarcas -- Los resultados se guardan en caché");
				}
				
				cache.putInCache(request.createCacheKeyListaMarcas(), results);
			}
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerListaMarcas -- Los resultados no se guardan en caché por no se han recuperado datos");
				}
			}	
			
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerListaMarcas --> Los datos no existen en la cache :: " + request.createCacheKeyListaMarcas() + " en " + (millis2 - millis1) + " milisegundos");
			}
		}

		return results;
	}
		
	public ArrayModel obtenerListaModelos(FormBean request) {
		long millis1 = System.currentTimeMillis() % 1000;
		if (logger.isDebugEnabled()) {
			logger.debug("GuiaLubricantesServices::obtenerListaModelos -- obtenerListaModelos :: createCacheKey " + request.createCacheKeyListaModelos());
		}

		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
		ArrayModel results = null;
		try {
			String TTL = resourceBundle.getString(BuscadorGuiaLubricacionConstants.TTL);
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerListaModelos -- Recuperando tiempo cache --> "  + TTL);
			}
			
			results = (ArrayModel) cache.getFromCache(request.createCacheKeyListaModelos(), Integer.parseInt(TTL));
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerListaModelos -- Recuperando elemento de la cache :: " + request.createCacheKeyListaModelos() + " en " + (millis2 - millis1) + " milisegundos");
			}
		} 
		catch (NeedsRefreshException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerListaModelos --> Los datos de la cache no existen o han caducado"); 
			}
			
			// Los datos de la cache no existen o han caducado
			cache.cancelUpdate(request.createCacheKeyListaModelos());
			try {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerListaModelos --> " + resourceBundle);
				}

				String usuario = resourceBundle.getString(BuscadorGuiaLubricacionConstants.USER_NAME);
				String password = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PASS);
				String idioma = request.getIdioma();
				
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerListaModelos -- Datos de conexión con Olyslager:: Usuario --> " +  usuario + " -- Password --> " + password + " -- Idioma --> " + idioma + " -- Marca --> " + request.getMarca()); 
				}

				results = service.getModels(usuario, password, idioma, request.getMarca());
				if (logger.isDebugEnabled()) {
					if (null != results.getModels())
						logger.debug("GuiaLubricantesServices::obtenerListaModelos - resultado --> " + results.getModels());
					else
						logger.debug("GuiaLubricantesServices::obtenerListaModelos - resultado -- NO se han recuperado modelos");
				}
			} 
			catch (RemoteException e1) {
				logger.error("ERROR GRAVE AL OBTENER LISTA MODELOS - GuiaLubricantesServices : obtenerListaModelos " + e1, e1); 
			}
			
			// Sólo guardamos en caché si tenemos un resultado no nulo (para evitar problemas de conexión y no poder actualizar los datos)
			if (null != results && null != results.getModels()) {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerListaModelos -- Los resultados se guardan en caché");
				}
				
				cache.putInCache(request.createCacheKeyListaModelos(), results);
			}	
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerListaModelos -- Los resultados no se guardan en caché por no se han recuperado datos");
				}
			}	
			
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerListaModelos -- Los datos no existen en la cache :: " + request.createCacheKeyListaModelos() + " en " + (millis2 - millis1) + " milisegundos");
			}
		}

		return results;
	}	
	
	public ArrayType obtenerListaTipos(FormBean request) {
		long millis1 = System.currentTimeMillis() % 1000;
		if (logger.isDebugEnabled()) {
			logger.debug("GuiaLubricantesServices::obtenerListaTipos -- createCacheKey " + request.createCacheKeyListaTipos());
		}

		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
		ArrayType results = null;
		try {
			String TTL = resourceBundle.getString(BuscadorGuiaLubricacionConstants.TTL);
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerListaTipos -- Recuperando tiempo cache --> "  + TTL);
			}
			
			results = (ArrayType) cache.getFromCache(request.createCacheKeyListaTipos(), Integer.parseInt(TTL));
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerListaTipos -- Recuperando elemento de la cache :: " + request.createCacheKeyListaTipos() + " en " + (millis2 - millis1) + " milisegundos");
			}
		} 
		catch (NeedsRefreshException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerListaTipos --> Los datos de la cache no existen o han caducado"); 
			}
			
			// Los datos de la cache no existen o han caducado
			cache.cancelUpdate(request.createCacheKeyListaTipos());
			try {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerListaTipos --> " + resourceBundle);
				}

				String usuario = resourceBundle.getString(BuscadorGuiaLubricacionConstants.USER_NAME);
				String password = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PASS);
				String idioma = request.getIdioma();
				
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerCategorias -- Datos de conexión con Olyslager:: Usuario --> " +  usuario + " -- Password --> " + password + " -- Idioma --> " + idioma + " -- Modelo --> " + request.getModelo()); 
				}
				
				results = service.getTypes(usuario, password, idioma, request.getModelo());
				if (logger.isDebugEnabled()) {
					if (null != results.getTypes())
						logger.debug("GuiaLubricantesServices::obtenerListaTipos - resultado --> --> " + results.getTypes());
					else
						logger.debug("GuiaLubricantesServices::obtenerListaTipos - resultado -- NO se han recuperado categorias");
				}	
			} 
			catch (RemoteException e1) {
				logger.error("ERROR GRAVE AL OBTENER LISTA TIPOS - GuiaLubricantesServices : obtenerListaTipos " + e1, e1); 
			}
			
			// Sólo guardamos en caché si tenemos un resultado no nulo (para evitar problemas de conexión y no poder actualizar los datos)
			if (null != results && null != results.getTypes()) {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerListaTipos -- Los resultados se guardan en caché");
				}
				
				cache.putInCache(request.createCacheKeyListaTipos(), results);
			}
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerListaTipos -- Los resultados no se guardan en caché por no se han recuperado datos");
				}
			}	
			
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerListaTipos -- Los datos no existen en la cache :: " + request.createCacheKeyListaTipos() + " en " + (millis2 - millis1) + " milisegundos");
			}
		}

		return results;
	}
	
	public Image obtenerImagenMarca(FormBean request) {
		long millis1 = System.currentTimeMillis() % 1000;
		if (logger.isDebugEnabled()) {
			logger.debug("GuiaLubricantesServices::obtenerImagenMarca -- createCacheKey " + request.createCacheKeyImagenMarca());
		}

		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
		Image results = null;
		try {
			String TTL = resourceBundle.getString(BuscadorGuiaLubricacionConstants.TTL);
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerImagenMarca -- Recuperando tiempo cache --> "  + TTL);
			}
			
			results = (Image) cache.getFromCache(request.createCacheKeyImagenMarca(), Integer.parseInt(TTL));
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerImagenMarca -- Recuperando elemento de la cache :: " + request.createCacheKeyImagenMarca() + " en " + (millis2 - millis1) + " milisegundos");
			}
		} 
		catch (NeedsRefreshException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerImagenMarca --> Los datos de la cache no existen o han caducado"); 
			}
			
			// Los datos de la cache no existen o han caducado
			cache.cancelUpdate(request.createCacheKeyImagenMarca());
			try {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices resourceBundle --> " + resourceBundle);
				}

				String usuario = resourceBundle.getString(BuscadorGuiaLubricacionConstants.USER_NAME);
				String password = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PASS);
				
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerImagenMarca -- Datos de conexión con Olyslager:: Usuario -->  " + usuario + " -- Password --> " + password + " -- Marca -->" + request.getMarca());
				}
				
				results = service.getImageMake(usuario, password, request.getMarca(),  false);
				if (logger.isDebugEnabled()) {
					if (null != results.getImg())
						logger.debug("GuiaLubricantesServices::obtenerImagenMarca - resultado --> " + results.getImg());
					else
						logger.debug("GuiaLubricantesServices::obtenerImagenMarca - resultado -- NO se ha recuperado imagen marca");
				}	
			} 
			catch (RemoteException e1) {
				logger.debug("ERROR AL OBTENER IMAGEN MARCA- PUEDE QUE NO EXISTA EN OLYSLAGER - GuiaLubricantesServices : obtenerImagenMarca " + e1, e1); 
			}
			
			// Sólo guardamos en caché si tenemos un resultado no nulo (para evitar problemas de conexión y no poder actualizar los datos)
			if (null != results) {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerImagenMarca -- Los resultados se guardan en caché");
				}
				
				cache.putInCache(request.createCacheKeyImagenMarca(), results);
			}
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerImagenMarca -- Los resultados no se guardan en caché por no se han recuperado datos");
				}
			}
			
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerImagenMarca -- Los datos no existen en la cache :: " + request.createCacheKeyImagenMarca() + " en " + (millis2 - millis1) + " milisegundos");
			}
		}

		return results;
	}
	
	public Image obtenerImagenModelo(FormBean request) {
		long millis1 = System.currentTimeMillis() % 1000;
		if (logger.isDebugEnabled()) {
			logger.debug("GuiaLubricantesServices::obtenerImagenModelo -- createCacheKey " + request.createCacheKeyImagenModelo());
		}

		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
		Image results = null;
		try {
			// FIXME: Probar CacheEntry.INDEFINITE_EXPIRY;
			String TTL = resourceBundle.getString(BuscadorGuiaLubricacionConstants.TTL);
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerImagenModelo --> Recuperando tiempo cache --> "  + TTL);
			}
			
			results = (Image) cache.getFromCache(request.createCacheKeyImagenModelo(), Integer.parseInt(TTL));
			
			// Se recupera la imagen de la caché siempre para evitar problemas a la hora de recuperar un gran número de ellas mediante llamadas a Olysalger
			//results = (Image) cache.getFromCache(request.createCacheKeyImagenModelo(), CacheEntry.INDEFINITE_EXPIRY);
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GGuiaLubricantesServices::obtenerImagenModelo -- Recuperando elemento de la cache :: " + request.createCacheKeyImagenModelo() + " en " + (millis2 - millis1) + " milisegundos");
			}
		} 
		catch (NeedsRefreshException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerImagenModelo --> Los datos de la cache no existen o han caducado"); 
			}
			// Los datos de la cache no existen o han caducado
			cache.cancelUpdate(request.createCacheKeyImagenModelo());
			try {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices resourceBundle --> " + resourceBundle);
				}

				String usuario = resourceBundle.getString(BuscadorGuiaLubricacionConstants.USER_NAME);
				String password = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PASS);
				
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerImagenModelo -- Datos de conexión con Olyslager:: Usuario -->  --> " + usuario + " -- Password -->  " + password + " -- Modelo --> " + request.getModelo());
				}
				
				results = service.getImageModel(usuario, password, request.getModelo());
				if (logger.isDebugEnabled()) {
					if (null != results.getImg())
						logger.debug("GuiaLubricantesServices::obtenerImagenModelo - resultado --> " + results.getImg());
					else
						logger.debug("GuiaLubricantesServices::obtenerImagenModelo results -- NO se ha recuperado imagen modelo");
				}	
			} 
			catch (RemoteException e1) {
				logger.debug("ERROR AL OBTENER IMAGEN MODELO - PUEDE QUE NO EXISTA EN OLYSLAGER GuiaLubricantesServices : obtenerImagenModelo " + e1, e1); 
			}
			
			// Sólo guardamos en caché si tenemos un resultado no nulo (para evitar problemas de conexión y no poder actualizar los datos)
			if (null != results) {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerImagenModelo -- Los resultados se guardan en caché");
				}
			
				cache.putInCache(request.createCacheKeyImagenModelo(), results);
			}
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerImagenModelo -- Los resultados no se guardan en caché por no se han recuperado datos");
				}
			}
			
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerImagenModelo --> Los datos no existen en la cache :: " + request.createCacheKeyImagenModelo() + " en " + (millis2 - millis1) + " milisegundos");
			}
		}

		return results;
	}
	
	public Image obtenerImagenTipo(FormBean request) {
		long millis1 = System.currentTimeMillis() % 1000;
		if (logger.isDebugEnabled()) {
			logger.debug("GuiaLubricantesServices::obtenerImageTipo -- createCacheKey " + request.createCacheKeyImagenTipo());
		}

		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
		Image results = null;
		try {
			String TTL = resourceBundle.getString(BuscadorGuiaLubricacionConstants.TTL);
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerImagenTipo --> Recuperando tiempo cache --> "  + TTL);
			}
			
			results = (Image) cache.getFromCache(request.createCacheKeyImagenTipo(), Integer.parseInt(TTL));
			
			// Se recupera la imagen de la caché siempre para evitar problemas a la hora de recuperar un gran número de ellas mediante llamadas a Olysalger
			//results = (Image) cache.getFromCache(request.createCacheKeyImagenTipo(), CacheEntry.INDEFINITE_EXPIRY);
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerImagenTipo -- Recuperando elemento de la cache :: " + request.createCacheKeyImagenTipo() + " en " + (millis2 - millis1) + " milisegundos");
			}
		} 
		catch (NeedsRefreshException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerImagenTipo imgTipo --> Los datos de la cache no existen o han caducado"); 
			}
			
			// Los datos de la cache no existen o han caducado
			cache.cancelUpdate(request.createCacheKeyImagenTipo());
			try {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerImagenTipo -- resourceBundle --> " + resourceBundle);
				}

				String usuario = resourceBundle.getString(BuscadorGuiaLubricacionConstants.USER_NAME);
				String password = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PASS);
				
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerImagenTipo -- Datos de conexión con Olyslager:: Usuario --> " + usuario + " -- Password --> " + password + " -- Tipo --> " + request.getTipo());
				}
					
				results = service.getImageType(usuario, password, request.getTipo());
				if (logger.isDebugEnabled()) {
					if (null != results.getImg())
						logger.debug("GuiaLubricantesServices::obtenerImagenTipo - resultado --> " + results.getImg());
					else
						logger.debug("GuiaLubricantesServices::obtenerCategorias - resultado -- NO se ha recuperado imagen tipo");
				}
			} 
			catch (RemoteException e1) {
				logger.debug("ERROR GRAVE AL OBTENER IMAGEN TIPO - PUEDE QUE NO EXISTA EN OLYSLAGER - GuiaLubricantesServices : obtenerImagenTipo " + e1, e1); 
			}
			
			// Sólo guardamos en caché si tenemos un resultado no nulo (para evitar problemas de conexión y no poder actualizar los datos)
			if (null != results) {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerImagenTipo -- Los resultados se guardan en caché");
				}
				
				cache.putInCache(request.createCacheKeyImagenTipo(), results);		
			}
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerImagenTipo -- Los resultados no se guardan en caché por no se han recuperado datos");
				}
			}
			
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerImagenTipo --> Los datos no existen en la cache :: " + request.createCacheKeyImagenTipo() + " en " + (millis2 - millis1) + " milisegundos");
			}
		}

		return results;
	}
	
	public ArrayType obtenerTiposPorMarcaModeloPredictivo(FormBean request) {
		long millis1 = System.currentTimeMillis() % 1000;
		if (logger.isDebugEnabled()) {
			logger.debug("GuiaLubricantesServices --> obtenerTiposPorMarcaModeloPredictivo :: createCacheKey " + request.createCacheKeyTiposMMP());
		}

		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
		ArrayType results = null;
		try {
			String TTL = resourceBundle.getString(BuscadorGuiaLubricacionConstants.TTL);
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModeloPredictivo --> Recuperando tiempo cache --> "  + TTL);
			}
			
			results = (ArrayType) cache.getFromCache(request.createCacheKeyTiposMMP(), Integer.parseInt(TTL));
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices --> Recuperando elemento de la cache :: " + request.createCacheKeyTiposMMP() + " en " + (millis2 - millis1) + " milisegundos");
			}
		} 
		catch (NeedsRefreshException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModeloPredictivo --> Los datos de la cache no existen o han caducado"); 
			}
			
			// Los datos de la cache no existen o han caducado
			cache.cancelUpdate(request.createCacheKeyTiposMMP());
			
			try {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModeloPredictivo resourceBundle --> " + resourceBundle);
				}

				String usuario = resourceBundle.getString(BuscadorGuiaLubricacionConstants.USER_NAME);
				String password = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PASS);
				String idioma = request.getIdioma();
				
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModeloPredictivo -- Datos de conexión con Olyslager:: usuario --> " +  usuario + " password --> " + password + " idioma --> " + idioma + " Texto --> " + request.getTexto() + " Categoria --> " + request.getCategoria()); 
				}
				
				results = service.getTypesFromInstanceSearch(usuario, password, idioma, request.getTexto(), request.getCategoria());
				if (logger.isDebugEnabled()) {
					if (null != results.getTypes())
						logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModeloPredictivo tipos --> " + results.getTypes());
					else
						logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModeloPredictivo tipos -- NO se han recuperado categorias");
				}	
			} 
			catch (RemoteException e1) {
				logger.error("ERROR GRAVE AL OBTENER LISTA TIPOS BUSQUEDA PREDICTIVA - GuiaLubricantesServices : obtenerTiposPorMarcaModeloPredictivo " + e1, e1); 
			}
	
			// Sólo guardamos en caché si tenemos un resultado no nulo (para evitar problemas de conexión y no poder actualizar los datos)
			if (null != results) {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModeloPredictivo -- Los resultados se guardan en caché");
				}
			
				cache.putInCache(request.createCacheKeyTiposMMP(), results);
			}
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModeloPredictivo -- Los resultados no se guardan en caché por no se han recuperado datos");
				}
			}
				
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerTiposPorMarcaModeloPredictivo --> Los datos no existen en la cache :: " + request.createCacheKeyTiposMMP() + " en " + (millis2 - millis1) + " milisegundos");
			}
		}

		return results;
	}	
		
	public Recommendation obtenerRecomendacion(FormBean request) {
		long millis1 = System.currentTimeMillis() % 1000;
		if (logger.isDebugEnabled()) {
			logger.debug("GuiaLubricantesServices::obtenerRecomendacion --> obtenerRecomendacion :: createCacheKey " + request.createCacheKeyRecomendacion());
		}

		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
		if (logger.isDebugEnabled()) {
			logger.debug("GuiaLubricantesServices::obtenerRecomendacion resourceBundle --> " + resourceBundle);
		}

		Recommendation results = null;
		
		try {
			String TTL = resourceBundle.getString(BuscadorGuiaLubricacionConstants.TTL);
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerRecomendacion --> Recuperando tiempo cache --> "  + TTL);
			}
			
			results = (Recommendation) cache.getFromCache(request.createCacheKeyRecomendacion(), Integer.parseInt(TTL));
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerRecomendacion --> Recuperando elemento de la cache :: " + request.createCacheKeyRecomendacion() + " en " + (millis2 - millis1) + " milisegundos");
			}
		} 
		catch (NeedsRefreshException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerRecomendacion --> Los datos de la cache no existen o han caducado"); 
			}
			
			// Los datos de la cache no existen o han caducado
			cache.cancelUpdate(request.createCacheKeyRecomendacion());
			
			try {
				String usuario = resourceBundle.getString(BuscadorGuiaLubricacionConstants.USER_NAME);
				String password = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PASS);
				String idioma = request.getIdioma();
				
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerRecomendacion -- Datos de conexión con Olyslager:: usuario --> " +  usuario + " password --> " + password + " Idioma --> " + idioma + " Tipo --> " + request.getTipo()); 
				}

				results = service.getRecommendation(usuario, password, idioma, request.getTipo());
				if (logger.isDebugEnabled()) {
					if (null != results.getComponents())
						logger.debug("GuiaLubricantesServices::obtenerRecomendacion results  --> " + results.getComponents());
					else
						logger.debug("GuiaLubricantesServices::obtenerRecomendacion results -- NO se han recuperado componentes");
				}	
			} 
			catch (RemoteException e1) {
				logger.error("ERROR GRAVE AL OBTENER RECOMENDACION - GuiaLubricantesServices : obtenerRecomendacion " + e1, e1); 
			}

			// Sólo guardamos en caché si tenemos un resultado no nulo (para evitar problemas de conexión y no poder actualizar los datos)
			if (null != results) {
				cache.putInCache(request.createCacheKeyRecomendacion(), results);
				
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerRecomendacion -- Los resultados se guardan en caché");
				}
			}
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerRecomendacion -- Los resultados no se guardan en caché por no se han recuperado datos");
				}
			}
			
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerRecomendacion --> Los datos no existen en la cache :: " + request.createCacheKeyRecomendacion() + " en " + (millis2 - millis1) + " milisegundos");
			}
		}

		return results;
	}	
	
	public ArrayMake obtenerListaMarcasPopulares(FormBean request) {
		long millis1 = System.currentTimeMillis() % 1000;
		if (logger.isDebugEnabled()) {
			logger.debug("GuiaLubricantesServices::obtenerListaMarcasPopulares --> obtenerListaMarcasPopulares :: createCacheKey " + request.createCacheKeyListaMarcasPopulares());
		}
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
		ArrayMake results = null;
		try {
			//results = (ArrayMake) cache.getFromCache(request.createCacheKeyListaMarcas(), BuscadorGuiaLubricacionConstants.TTL);
			
			String TTL = resourceBundle.getString(BuscadorGuiaLubricacionConstants.TTL);
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerListaMarcasPopulares --> Recuperando tiempo cache --> "  + TTL);
			}
			
			results = (ArrayMake) cache.getFromCache(request.createCacheKeyListaMarcasPopulares(), Integer.parseInt(TTL));
			
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GGuiaLubricantesServices::obtenerListaMarcasPopulares --> Recuperando elemento de la cache :: " + request.createCacheKeyListaMarcasPopulares() + " en " + (millis2 - millis1) + " milisegundos");
			}
		} 
		catch (NeedsRefreshException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("GuiaLubricantesServices::obtenerListaMarcasPopulares --> Los datos de la cache no existen o han caducado"); 
			}
			
			// Los datos de la cache no existen o han caducado
			cache.cancelUpdate(request.createCacheKeyListaMarcasPopulares());
			
			try {				
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerListaMarcasPopulares resourceBundle --> " + resourceBundle);
				}

				String usuario = resourceBundle.getString(BuscadorGuiaLubricacionConstants.USER_NAME);
				String password = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PASS);
				
				//LLamada al método de Olyslager que corresponda
				results = null; //service.getMakesXXX(usuario, password, BuscadorGuiaLubricacionConstants.LANGUAGE_ISO3_SPA, 1);
				
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerListaMarcasPopulares - resultado --> " + results.getMakes());
				}	
			} 
//			catch (RemoteException e1) {
//				logger.error("ERROR GRAVE AL OBTENER LISTA TIPOS BUSQUEDA RemoteException - GuiaLubricantesServices : obtenerListaMarcas " + e1); 
//			}
			catch (Exception e2) {
				logger.error("ERROR GRAVE AL OBTENER LISTA TIPOS BUSQUEDA Exception - GuiaLubricantesServices : obtenerListaMarcasPopulares " + e2, e2); 
			}
			
			// Sólo guardamos en caché si tenemos un resultado no nulo (para evitar problemas de conexión y no poder actualizar los datos)
			if (null != results) {
				if (logger.isDebugEnabled()) {
					logger.debug("GuiaLubricantesServices::obtenerListaMarcasPopulares -- Los resultados se guardan en caché");
				}	
			
				cache.putInCache(request.createCacheKeyListaMarcasPopulares(), results);
			}
			
			if (logger.isDebugEnabled()) {
				long millis2 = System.currentTimeMillis() % 1000;
				logger.debug("GuiaLubricantesServices::obtenerListaMarcasPopulares --> Los datos no existen en la cache :: " + request.createCacheKeyListaMarcasPopulares() + " en " + (millis2 - millis1) + " milisegundos");
			}
		}

		return results;
	}
	
}