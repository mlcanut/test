package com.ieci.cepsa17.util;

import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class StringUtil {
	
	private static final String[] searchList  = new String[] {"á", "é", "í", "ó", "ú"};
	private static final String[] replaceList = new String[] {"a", "e", "i", "o", "u"};
	
	private static Logger logger = Logger.getLogger(StringUtil.class);
	
	/**
	 * Parsea un string y sustituye los placeholders por los valores reales
	 * @param url
	 * @param placeHolderParameters
	 * @return
	 */
	public  static String parseString(String query, Map<String,String> placeHolderParameters){

		final Set<String> keys = placeHolderParameters.keySet();

		String replacedStr = query;
		logger.debug("StringUtil::QueryParser::parse " + query);

		for(String key : keys){

			replacedStr = replacedStr.replace(key, placeHolderParameters.get(key));
		}
		logger.debug("StringUtil::QueryParser:parsed:" + replacedStr);
	

		return replacedStr;
	}
	
	
	/**
	 * Sustituye caracteres especiales para que la cadena sea valida para ciertos usos
	 * (por ejemplo el GTM)
	 * 
	 * @param cadena
	 * @return La cadena normalizada
	 */
	public static String normalizarCadena(String cadena) {
		String result = "";
		if(cadena != null){
			result = cadena.toLowerCase();
			result = StringUtils.replaceEach(result, searchList, replaceList);
		}
		return result;
	}
	
	/**
	 * Este método nos indica si hay alg�n texto pasado, los espacios en blanco son ignorados 
	 * @param input el texto a analizar
	 * @return true --> texto vacio false -> hay texto
	 */
	public static boolean isEmptyIgnoreWhitespace(String input) {
		boolean isEmptyIgnoreWhitespace = false;
		if (((input==null ? "" : input.toString()).trim().length()==0))
			return true;
		return isEmptyIgnoreWhitespace;
	}	
	
	public static Locale parseLocale(String localeString){
		if (localeString == null) {
			return null;
		}
		StringTokenizer tokens = new StringTokenizer(localeString, "_");
		String[] token = new String[tokens.countTokens()];
		int count = 0;
		while (tokens.hasMoreTokens()) {
			token[count] = tokens.nextToken();
			count++;
		}
		if (count == 3)
			return new Locale(token[0], token[1], token[2]);
		if (count == 2)
			return new Locale(token[0], token[1]);
		if (!token[0].toUpperCase().equals(token[0])) {
			return new Locale(token[0]);
		}
		return null;
	}

}
