package com.ieci.cepsa17.util;

/**
 * Clase de utilidades para las instancias de contenido de tipo Art�culo
 * 
 * @author V�ctor M. Glez. Garc�a
 * @modifier Jaime Romo Hern�ndez
 */

import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.javabean.Channel;
import com.vignette.as.client.javabean.ContentInstance;
import com.vignette.ext.templating.util.RequestContext;

import es.ieci.cepsa.utilities.as.client.i18nbean.ArticuloContentInstance;
import es.ieci.cepsa.utilities.as.client.i18nbean.ChannelMetaContentInstance;
import es.ieci.cepsa.utilities.as.client.i18nbean.I18nContentInstanceRef;
import es.ieci.cepsa.utilities.cache.framework.ChannelMetaCache;
import es.ieci.cepsa.utilities.cache.framework.ElementContentInstance;
import es.ieci.cepsa.utilities.common.ContentInstanceUtil;
import es.ieci.cepsa.utilities.common.vap.UserLanguageUtils;


public class I18nContentInstanceUtils {
	

	/** Log */
	private static final Log logger = LogFactory.getLog(I18nContentInstanceUtils.class);

	/**
	 * Constructor (privado, no es instanciable)
	 *
	 */
	private I18nContentInstanceUtils() { 
	}

	/**
	 * Recibe como par�metro la RequestContext, una instancia de contenido
	 * ContentInstance, el valor dela propiedad a recuperar y un valor
	 * predeterminado Devuelve el valor de la propiedad solicitada dependiendo
	 * del lenguaje del locale
	 * 
	 * La propiedad se recupera de la relaci�n multiidioma de primer nivel
	 * @param rc RequestContext a partir de la cual obtendremos el lenguaje del locale.
	 * @param ci Contenido internacionalizado
	 * @param property String que indica el valor de la propiedad internacionalizada
	 * a recuperar.
	 * @param predeterminado String a mostrar por defecto si no se encuentra la etiqueta
	 * @return String representando el valor de la propiedad dependiendo del idioma especificado por el Locale.
	 * 
	 */
	public static String getPropertyI18n(RequestContext rc, ContentInstance ci, String property, String predeterminado) {
		return getPropertyI18n(rc, ci, property, predeterminado, null);
	}
	
	
	/**
	 * Recibe como par�metro la RequestContext, una instancia de contenido
	 * ContentInstance, el valor dela propiedad a recuperar y un valor
	 * predeterminado Devuelve el valor de la propiedad solicitada dependiendo
	 * del lenguaje
	 * 
	 * La propiedad se recupera de la relaci�n multiidioma de primer nivel
	 * @param rc RequestContext a partir de la cual obtendremos el lenguaje del locale.
	 * @param ci Contenido internacionalizado
	 * @param property String que indica el valor de la propiedad internacionalizada
	 * a recuperar.
	 * @param predeterminado String a mostrar por defecto si no se encuentra la etiqueta
	 * @param locale Lenguaje alternativo al especificado en el rc, es opcional pero de estar completado se utilizar� como idioma secundario en caso de que el especifico en rc no exista.
	 * @return String representando el valor de la propiedad dependiendo del idioma especificado por el Lenguaje.

	 */
	public static String getPropertyI18nLang(RequestContext rc, ContentInstance ci, String property, String locale) {
		String valor = null;

		if (logger.isDebugEnabled())
			logger.debug("getPropertyI18n | TRACE --> property=" + property);
		if (ci instanceof I18nContentInstanceRef) {
			I18nContentInstanceRef i18nci = (I18nContentInstanceRef) ci;
			if (ci != null) {
				//valor = i18nci.getPropertyValueFromLocale(property, rc.getLocale().getLanguage().toString(), UserLanguageUtils.getDefaultLocale(rc).getLanguage().toString(), predeterminado);
				//if ((valor.equals("") || valor==null) && (locale != null))
				 if(locale!=null)
					valor = i18nci.getPropertyValueFromLocale(property, locale, UserLanguageUtils.getDefaultLocale(rc).getLanguage().toString());
				//else
					// valor = i18nci.getPropertyValueFromLocale(property, rc.getLocale().getLanguage().toString(), UserLanguageUtils.getDefaultLocale(rc).getLanguage().toString(), predeterminado);
			}
		} else {
			try {
				logger.warn("La instancia " + ci.getName() + " no es una instancia de I18nContentInstanceRef");
			} catch (ApplicationException e) {
				logger.warn("No se puede recuperar el nombre de la instancia de tipo: " + ci.getClass().getName());
			}
		}
		if (logger.isDebugEnabled())
			logger.debug("getPropertyI18n | TRACE --> Return: "+ valor + " )");
		return valor;
	}	
	
	
	
	/**
	 * Recibe como par�metro la RequestContext, una instancia de contenido
	 * ContentInstance, el valor dela propiedad a recuperar y un valor
	 * predeterminado Devuelve el valor de la propiedad solicitada dependiendo
	 * del lenguaje del locale
	 * 
	 * La propiedad se recupera de la relaci�n multiidioma de primer nivel
	 * @param rc RequestContext a partir de la cual obtendremos el lenguaje del locale.
	 * @param ci Contenido internacionalizado
	 * @param property String que indica el valor de la propiedad internacionalizada
	 * a recuperar.
	 * @param predeterminado String a mostrar por defecto si no se encuentra la etiqueta
	 * @param locale El locale alternativo al especificado en el rc, es opcional pero de estar completado se utilizar� como idioma secundario en caso de que el especifico en rc no exista.
	 * @return String representando el valor de la propiedad dependiendo del idioma especificado por el Locale.

	 */
	public static String getPropertyI18n(RequestContext rc, ContentInstance ci, String property, String predeterminado, Locale locale) {
		String valor = null;

		if (logger.isDebugEnabled())
			logger.debug("getPropertyI18n | TRACE --> property=" + property + ", porDefecto=" + predeterminado);
		if (ci instanceof I18nContentInstanceRef) {
			I18nContentInstanceRef i18nci = (I18nContentInstanceRef) ci;
			if (ci != null) {
				valor = i18nci.getPropertyValueFromLocale(property, rc.getLocale().getLanguage().toString(), UserLanguageUtils.getDefaultLocale(rc).getLanguage().toString(), predeterminado);
				if ((valor.equals("") || valor==null) && (locale != null))
					valor = i18nci.getPropertyValueFromLocale(property, locale.getLanguage().toString(), UserLanguageUtils.getDefaultLocale(rc).getLanguage().toString(), predeterminado); 
			}
		} else {
			try {
				logger.warn("La instancia " + ci.getName() + " no es una instancia de I18nContentInstanceRef");
			} catch (ApplicationException e) {
				logger.warn("No se puede recuperar el nombre de la instancia de tipo: " + ci.getClass().getName());
			}
		}
		if (logger.isDebugEnabled())
			logger.debug("getPropertyI18n | TRACE --> Return: "+ valor + " )");

		return valor;
	}	

	/**
	 * Recibe como par�metro la RequestContext, una instancia de contenido
	 * ContentInstance, el valor dela propiedad a recuperar y un valor
	 * predeterminado. Devuelve el valor de la propiedad de la SECCION del
	 * articulo solicitada, dependiendo del lenguaje del locale
	 * 
	 * @param rc RequestContext a partir de la cual obtendremos el lenguaje del
	 * locale.
	 * @param property String que indica el valor de la propiedad internacionalizada
	 * a recuperar.
	 * @param predeterminado String a mostrar por defecto si no se encuentra la etiqueta
	 * @return String representando el valor de la propiedad de la seccion, dependiendo del idiona especificado por el Locale.
	 */
	public static String getSeccionArticuloI18n(RequestContext rc,	ContentInstance ci, String property, String predeterminado,	String ciSeccion) {
		if (logger.isDebugEnabled())
			logger.debug("| getSeccionArticuloI18n | TRACE --> property=" + property + ", porDefecto=" + predeterminado);
		ArticuloContentInstance articulo = null;
		articulo = (ArticuloContentInstance) ci;
		String valor = null;

		if (articulo != null)
			valor = articulo.getSecctionValueFromLocale(property, rc.getLocale().getLanguage().toString(), 
					UserLanguageUtils.getDefaultLocale(rc).getLanguage().toString(), predeterminado, ciSeccion);

		if (logger.isDebugEnabled())
			logger.debug("| getSeccionArticuloI18n | TRACE --> Return: " + valor);

		return valor;
	}
	
	public static String getArticulosFromChannelID(String idChannel, int numResults,Locale locale,Locale defaultLoc){
		String resultado="";
		List resultados= ContentInstanceUtil.getContentsFromChannelID(idChannel, "ARTICULO", numResults);
		
		if(resultados!=null)
			resultado= ContentInstanceUtil.ContentList2XML(resultados, locale, defaultLoc);
		return resultado;
	}
	
	/**
	 * Recupera de cache una instancia de ChannelMeta a partir del id de canal
	 * @param channelId - Canal cuyo channel meta se quiere recuperar
	 * */
	public static ChannelMetaContentInstance getChannelMeta(String channelId) {
		if (logger.isDebugEnabled()) logger.debug("getChannelMeta( channel= " + channelId+ " )");	
		ElementContentInstance element = null;
		ChannelMetaContentInstance channelMeta = null;
		element = ChannelMetaCache.getElementFromCache(channelId);
		if (element!= null)
			channelMeta = (ChannelMetaContentInstance) element.getCi();
		return channelMeta;
	}
}
