package com.ieci.cepsa17.util.enviarmail;

public class MailConstants {

	public static final String BLANK = "";
	public static final String COLON = ":";
	public static final String SPACE = " ";

	public static final String MAIL_BUNDLE_PATH = "com.cepsa.portal.mail.config";

	public static final String PROPERTY_MAIL_INITIAL_CTX_FACTORY = "mail.initialContextFactory";
	public static final String PROPERTY_MAIL_PROVIDER_URL = "mail.providerURL";
	public static final String PROPERTY_MAIL_SECURITY_PRINCIPAL = "mail.securityPrincipal";
	public static final String PROPERTY_MAIL_SECURITY_CREDENTIALS = "mail.securityCredentials";

	public static final String SITIO_CORPORATIVO = "corporativo";
	public static final String EMAIL_CASOS_CRISIS_ASUNTO = "desplegable.descarga.con.selector.enviar.por.email.asunto";
	public static final String EMAIL_CASOS_CRISIS_ESTIMADO = "desplegable.descarga.con.selector.enviar.por.email.estimado";
	public static final String EMAIL_CASOS_CRISIS_BODY = "desplegable.descarga.con.selector.enviar.por.email.body";
	public static final String EMAIL_CASOS_CRISIS_COMENTARIOS = "desplegable.descarga.con.selector.enviar.por.email.comentarios";
	public static final String EMAIL_CASOS_CRISIS_ATENTAMENTE = "desplegable.descarga.con.selector.enviar.por.email.atentamente";
	public static final String EMAIL_CASOS_CRISIS_INFO_CONTENIDA = "desplegable.descarga.con.selector.enviar.por.email.info.contenida";

	private MailConstants() {
		super();
	}

}