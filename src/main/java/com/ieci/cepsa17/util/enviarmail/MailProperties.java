package com.ieci.cepsa17.util.enviarmail;

import java.util.ResourceBundle;

public class MailProperties {

	private String initialContextFactory;
	private String providerURL;
	private String securityPrincipal;
	private String securityCredentials;
	private String fromAddress;

	protected static final String MAIL_BUNDLE = MailConstants.MAIL_BUNDLE_PATH;

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getInitialContextFactory() {
		return initialContextFactory;
	}

	public void setInitialContextFactory(String initialContextFactory) {
		this.initialContextFactory = initialContextFactory;
	}

	public String getProviderURL() {
		return providerURL;
	}

	public void setProviderURL(String providerURL) {
		this.providerURL = providerURL;
	}

	public String getSecurityPrincipal() {
		return securityPrincipal;
	}

	public void setSecurityPrincipal(String securityPrincipal) {
		this.securityPrincipal = securityPrincipal;
	}

	public String getSecurityCredentials() {
		return securityCredentials;
	}

	public void setSecurityCredentials(String securityCredentials) {
		this.securityCredentials = securityCredentials;
	}

	public MailProperties() {

		ResourceBundle resourceBundle = ResourceBundle.getBundle(MAIL_BUNDLE);

		this.initialContextFactory = resourceBundle.getString(MailConstants.PROPERTY_MAIL_INITIAL_CTX_FACTORY);
		this.providerURL = resourceBundle.getString(MailConstants.PROPERTY_MAIL_PROVIDER_URL);
		this.securityPrincipal = resourceBundle.getString(MailConstants.PROPERTY_MAIL_SECURITY_PRINCIPAL);
		this.securityCredentials = resourceBundle.getString(MailConstants.PROPERTY_MAIL_SECURITY_CREDENTIALS);

	}

}