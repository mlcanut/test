package com.ieci.cepsa17.util.enviarmail;

import java.util.Locale;
import java.util.Properties;

import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.jms.connection.UserCredentialsConnectionFactoryAdapter;
import org.thymeleaf.TemplateEngine;

import com.ieci.cepsa17.wem.comun.util.CIUtil;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.javabean.Site;
import com.vignette.ext.templating.util.ContentUtil;

import cepsa.email.EmailData;
import cepsa.email.impl.jms.JmsEmailSender;

public class MailHelper {

	private TemplateEngine templateEngine;
	private MailProperties props;
	private static final Logger logger = LogManager.getLogger(MailHelper.class);

	public MailHelper(TemplateEngine templateEngine, MailProperties props) {
		this.templateEngine = templateEngine;
		this.props = props;
	}

	public void sendMail(String to, String from, String body, String fileName, String filePath, Locale locale)
			throws NamingException, ApplicationException {

		final Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY, props.getInitialContextFactory());
		env.put(Context.PROVIDER_URL, props.getProviderURL());
		env.put(Context.SECURITY_PRINCIPAL, props.getSecurityPrincipal());
		env.put(Context.SECURITY_CREDENTIALS, props.getSecurityCredentials());

		logger.debug("props.getProviderURL(): " + props.getProviderURL());

		javax.naming.Context context = new InitialContext(env);
		QueueConnectionFactory cfAux = (QueueConnectionFactory) context.lookup("jms/RemoteConnectionFactory");
		UserCredentialsConnectionFactoryAdapter cf = new UserCredentialsConnectionFactoryAdapter();
		cf.setTargetConnectionFactory(cfAux);
		cf.setCredentialsForCurrentThread(props.getSecurityPrincipal(), props.getSecurityCredentials());
		Queue destination = (Queue) context.lookup("jms/queue/mailQueue");

		Site site = ContentUtil.getSiteByName(MailConstants.SITIO_CORPORATIVO);
		String asunto = CIUtil.getLiteral(site, MailConstants.EMAIL_CASOS_CRISIS_ASUNTO, MailConstants.BLANK,
				locale.getLanguage()) + MailConstants.COLON + MailConstants.SPACE + fileName;

		EmailData emailData = new EmailData(from, to, asunto, body, true);
		emailData.addFileAttachment(fileName, filePath);
		JmsEmailSender jmsEmailSender = new JmsEmailSender("FormBuilder", cf, destination);
		jmsEmailSender.sendMessage(emailData);

		context.close();

	}

	public void prepareAndSend(String template, String to, String from, String fileName, String filePath, Locale locale,
			org.thymeleaf.context.Context context) throws NamingException, ApplicationException {

		String mailBody = getBody(template, context);
		this.sendMail(to, from, mailBody, fileName, filePath, locale);

	}

	public String getBody(String template, org.thymeleaf.context.Context context) {

		return this.templateEngine.process(template, context);

	}
}
