package com.ieci.cepsa17.util.enviarmail;

import java.util.Locale;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import com.ieci.cepsa17.wem.comun.util.CIUtil;
import com.vignette.as.client.javabean.Site;
import com.vignette.ext.templating.util.ContentUtil;

public class UtilEnviarMail {

	private static final Logger logger = LogManager.getLogger(UtilEnviarMail.class);
	private static MailHelper mailHelper;

	static {
		ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
		resolver.setTemplateMode("XHTML");
		resolver.setPrefix("com/ieci/cepsa17/util/enviarmail/templates/");
		resolver.setSuffix(".html");
		TemplateEngine engine = new TemplateEngine();
		engine.setTemplateResolver(resolver);

		MailProperties mailProp = new MailProperties();

		mailHelper = new MailHelper(engine, mailProp);
	}

	public static void send(String to, String from, String nombre, String comentarios, String nombreFichero,
			String pathFichero, Locale locale) {

		try {
			if (nombreFichero != null && !MailConstants.BLANK.equals(nombreFichero) && pathFichero != null
					&& !MailConstants.BLANK.equals(pathFichero)) {
				configureMail("correo_general", to, from, nombre, comentarios, nombreFichero, pathFichero, locale);
				logger.debug("Documento " + pathFichero + " enviado a " + to);
			} else {
				logger.debug("No se ha seleccionado documento para enviar");
			}
		} catch (Exception e) {
			logger.error("Se ha producido un error al enviar el mail: " + e.getMessage(), e);
		}

	}

	private static void configureMail(String template, String to, String from, String nombre, String comentarios,
			String nombreFichero, String pathFichero, Locale locale) throws Exception {

		String language = locale.getLanguage();

		Site site = ContentUtil.getSiteByName(MailConstants.SITIO_CORPORATIVO);

		String estimadoCtx = CIUtil.getLiteral(site, MailConstants.EMAIL_CASOS_CRISIS_ESTIMADO, MailConstants.BLANK,
				language) + MailConstants.SPACE + nombre + MailConstants.COLON;

		String bodyCtx = CIUtil.getLiteral(site, MailConstants.EMAIL_CASOS_CRISIS_BODY, MailConstants.BLANK, language);

		String comentariosCtx = CIUtil.getLiteral(site, MailConstants.EMAIL_CASOS_CRISIS_COMENTARIOS,
				MailConstants.BLANK, language) + MailConstants.COLON + MailConstants.SPACE + comentarios;

		String atentamenteCtx = CIUtil.getLiteral(site, MailConstants.EMAIL_CASOS_CRISIS_ATENTAMENTE,
				MailConstants.BLANK, language);

		String infoContenidaCtx = CIUtil.getLiteral(site, MailConstants.EMAIL_CASOS_CRISIS_INFO_CONTENIDA,
				MailConstants.BLANK, language);

		Context context = new Context();
		context.setVariable("estimado", estimadoCtx);
		context.setVariable("body", bodyCtx);
		context.setVariable("comentarios", comentariosCtx);
		context.setVariable("atentamente", atentamenteCtx);
		context.setVariable("infoContenida", infoContenidaCtx);
		context.setLocale(locale);

		mailHelper.prepareAndSend(template, to, from, nombreFichero, pathFichero, locale, context);

	}

}