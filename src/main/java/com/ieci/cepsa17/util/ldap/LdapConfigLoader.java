package com.ieci.cepsa17.util.ldap;

import java.util.MissingResourceException;
import java.util.ResourceBundle;


/**
 * Giving the associated value to its argument 'key'
 *
 * @author Miguel Lacosta
 * @version 1.0
 * @param key[String]
 *            Key property whose value we want to read
 * @return the associated value to argument 'key', null if 'key' is missing
 */

public class LdapConfigLoader {
	
private static final String BUNDLE_NAME = "ldap-config";
private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

private LdapConfigLoader() {
}

public static String getProperty(String key) {
	try {
		return RESOURCE_BUNDLE.getString(key);
	} catch (MissingResourceException e) {
		return null;
	}
}

}