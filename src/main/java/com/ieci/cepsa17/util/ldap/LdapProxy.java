package com.ieci.cepsa17.util.ldap;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.log4j.Logger;

import com.ieci.cepsa17.util.StringUtil;


public class LdapProxy {

	static Hashtable<String, Object> env;
	static String LDAP_LOG_ENABLED = "SI";
	static SearchControls sc;
	
	static {
		
		env = new Hashtable<String, Object>(6);
        env.put(Context.INITIAL_CONTEXT_FACTORY, LdapConfigLoader.getProperty("ldap.initial.context.factory"));
		env.put(Context.SECURITY_AUTHENTICATION, LdapConfigLoader.getProperty("ldap.security.authentication"));
        env.put(Context.SECURITY_PRINCIPAL, LdapConfigLoader.getProperty("ldap.securiry.principal"));
        env.put(Context.SECURITY_CREDENTIALS, LdapConfigLoader.getProperty("ldap.securiry.credentials"));
        env.put(Context.PROVIDER_URL, LdapConfigLoader.getProperty("ldap.provide.url"));

        // the following is helpful in debugging errors
        if (LDAP_LOG_ENABLED.equalsIgnoreCase(LdapConfigLoader.getProperty("ldap.log.enabled"))){
        	env.put("com.sun.jndi.ldap.trace.ber", System.err);
        }
        
		sc = new SearchControls();
		sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
    }
	
	private static Logger logger = Logger.getLogger(LdapProxy.class);
	
	/**
	 * 
	 * @param uid
	 * @return
	 */
	public static LdapUserVO readLdapUsers(String uid){

		logger.info("LdapProxy::readLdapUsers starting uid:" + uid);
	

		DirContext ctx;
		LdapUserVO result = null;

		try {
		    ctx = new InitialDirContext(env);

			//Hacemos consulta por grupo, hacemos n consultas
			String filterValue = LdapConfigLoader.getProperty("ldap.search.filters");
			//Parseamos el filtro sustituyendo sus parametros
			Map<String,String> placeHolderParameters = new HashMap<String, String>();
			placeHolderParameters.put("PARAM_UID", uid);
			filterValue = StringUtil.parseString(filterValue, placeHolderParameters);

				NamingEnumeration<?> ldapResults = null;
				ldapResults = ctx.search(LdapConfigLoader.getProperty("ldap.search.base"), filterValue, sc);

				SearchResult searchResult = null;

		        while(ldapResults.hasMoreElements()) {

		        	 result = new LdapUserVO();
		             searchResult = (SearchResult) ldapResults.nextElement();
		             Attributes attrs = searchResult.getAttributes();

		            
		            logger.debug(String.format("InCompanyHelper::readLdapUsers user: %s",attrs.get("cn").get().toString()));
		            logger.debug(String.format("InCompanyHelper::readLdapUsers uid: %s",attrs.get("uid").get().toString()));
		            logger.debug(String.format("InCompanyHelper::readLdapUsers ldap.uid: %s",attrs.get(LdapConfigLoader.getProperty("ldap.uid"))!=null?attrs.get(LdapConfigLoader.getProperty("ldap.uid")).get().toString():""));
					
		             //El uid del usuario se lee de un atributo configurable

		             if (attrs.get(LdapConfigLoader.getProperty("ldap.uid"))!=null && !"".equalsIgnoreCase(attrs.get(LdapConfigLoader.getProperty("ldap.uid")).get().toString())){
		            	String userId = attrs.get(LdapConfigLoader.getProperty("ldap.uid")).get().toString();
		            	logger.debug(String.format("InCompanyHelper::readLdapUsers id de usuario con el que se trabajara: %s",userId));
		            	//result.setEmail(attrs.get(LdapConfigLoader.getProperty("ldap.email")).get().toString());
		            	result.setUid(userId);
		            	result.setGroups(attrs.get(LdapConfigLoader.getProperty("ldap.groups")).getAll());
		            		            	
		            	
		             }else {
		            	 logger.debug(String.format("InCompanyHelper::readLdapUsers usuario %s no dispone del atributo: %s ,se ignora",attrs.get("uid").get().toString(), LdapConfigLoader.getProperty("ldap.uid")));
		             }
		        }

		} catch (final NamingException e) {
			logger.error("Se ha producido un error al acceder al ldap.", e);
		    return null;
		} catch (Exception e){
			logger.error("Se ha producido un error calculando los grupos directamente desde ldap para el uid: " + uid, e);
			return null;
		}

		return result;

	}
	
}
