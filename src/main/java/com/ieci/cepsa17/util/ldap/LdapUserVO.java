package com.ieci.cepsa17.util.ldap;

import java.util.Enumeration;

import javax.naming.NamingEnumeration;

public class LdapUserVO {

	private String uid;
	private NamingEnumeration groups;
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	
	public NamingEnumeration getGroups() {
		return groups;
	}
	public void setGroups(NamingEnumeration groups) {
		this.groups = groups;
	}
	public LdapUserVO() {
		super();
	}
	@Override
	public String toString() {
		return "LdapUserVO [uid=" + uid + ", groups=" + groups + "]";
	}
	
	
}
