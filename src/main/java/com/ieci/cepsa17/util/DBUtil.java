package com.ieci.cepsa17.util;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class DBUtil {

	private static Logger logger = Logger.getLogger(DBUtil.class);
	
	/**
	 * Obtiene una conexión desde un datasource
	 * @param dataSource datasource que gestiona las conexiones
	 * @return Connection
	 */
	public static Connection getConnection(String dataSource) {
		
		Context ctx;
		Connection conn = null;
		try {
			ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup(dataSource);
			conn = ds.getConnection();
		} catch (NamingException e) {
			logger.error("Error recuperando conex.",e);
		} catch (SQLException e) {
			logger.error("Error recuperando conex.",e);

		}
		return conn;
}
	/**
	 * Cierra una conexion
	 * @param conn
	 */
	public static void closeConnection(Connection conn) {
		
		try {
			conn.close();
		} catch (SQLException e) {
			logger.error("Error cerrando conex.",e);
		}
	}

	


}
