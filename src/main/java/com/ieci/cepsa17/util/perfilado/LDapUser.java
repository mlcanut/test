package com.ieci.cepsa17.util.perfilado;



import java.util.ArrayList;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ieci.cepsa17.util.StringUtil;
import com.ieci.cepsa17.util.ldap.LdapProxy;
import com.ieci.cepsa17.util.ldap.LdapUserVO;

public class LDapUser {
	

	 private static Logger logger = Logger.getLogger(LDapUser.class);
	
	private ResourceBundle props;

	private String nombre;
	private String apellido1;
	private String apellido2;
	private String descripcion;
	private String email;
	private String nif;
	private String pager;
	private String tipo;
	private boolean emailValidado;
	private boolean changePass;
	private boolean ptvFlag;
	private boolean professionalFlag;
	private boolean sapFlag;
	private ArrayList<String> socialNets;
	private ArrayList<String> documentId;
	private ArrayList<Map<String,String>> webProperty;
	private ArrayList<String> memberOf;
	private boolean defaultAction;
	
	private String siteName;
	
	private String uid;
	
	public void setProperties(ResourceBundle resourceBundle) {
		props = resourceBundle;
	}
	
	public String getUid() {
		return uid;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getEmail() {
		return email;
	}

	public String getNif() {
		return nif;
	}

	public String getPager() {
		return pager;
	}

	public String getTipo() {
		return tipo;
	}

	public boolean getEmailValidado() {
		return emailValidado;
	}

	public boolean getChangePass() {
		return changePass;
	}

	public boolean getPtvFlag() {
		return ptvFlag;
	}

	public boolean getProfessionalFlag() {
		return professionalFlag;
	}

	public boolean getSapFlag() {
		return sapFlag;
	}
	
	public boolean NifValidado() {
		if(getNif()==null) return false;
		if(documentId == null || documentId.size() == 0) return false; else {
			
			String charDocuId = documentId.toString();
			int count = StringUtils.countMatches(charDocuId,"identity");
			if(count != 1) {
				return false;
			}
			
			int posNif = charDocuId.indexOf(getNif());
			
			if(posNif < 0) {return false;} else {
				
				int posEnd = -1;
				for(int i = posNif; i < charDocuId.length();i++) {
					if('}' == (charDocuId.charAt(i))){
						posEnd = i;
						break;
					}
				}
				
				if(posEnd < 0) { return false;} else {
					return charDocuId.substring(posNif,posEnd) .contains("validated=1");
				}
			}
			
		}
		

	}

	public ArrayList<String> getSocialNets() {
		return socialNets;
	}

	public ArrayList<String> getDocumentId() {
		return documentId;
	}

	public ArrayList<Map<String,String>> getWebProperty() {
		return webProperty;
	}

	public ArrayList<String> getMemberOf() {
		return memberOf;
	}

	
	@SuppressWarnings("unchecked")
	public LDapUser(String uid, Map<String,Object> userMap) {

		if(!userMap.containsKey(RestFinalUtils.ERROR)) {
			this.uid = (String)userMap.get("uid");
			this.nombre = (String)userMap.get("cepsaUserNombre");
			this.apellido1 = (String)userMap.get("cepsaUserApellido1");
			this.apellido2 = (String)userMap.get("cepsaUserApellido2");
			this.descripcion = (String)userMap.get("description");
			this.email = (String)userMap.get("cepsaUserEmail");
			this.nif = (String)userMap.get("cepsaUserNif");
			this.pager = (String)userMap.get("pager");
			this.tipo = (String)userMap.get("userType");
			this.emailValidado = (Boolean)userMap.get("emailValidated");
			this.changePass = (Boolean)userMap.get("changePass");
			this.ptvFlag = (Boolean)userMap.get("ptvFlag");
			this.professionalFlag = (Boolean)userMap.get("professionalFlag");
			this.sapFlag = (Boolean)userMap.get("sapFlag");
			this.socialNets = (ArrayList<String>)userMap.get("socialNets");
			this.documentId = (ArrayList<String>)userMap.get("documentIds");
			this.webProperty = (ArrayList<Map<String,String>>)userMap.get("cepsaWebProperty");
			this.memberOf = (ArrayList<String>)userMap.get("cepsaWebMemberOf");
			this.defaultAction = false;
		} else {
			rellenarVacio(uid);
		}
	}

	public LDapUser(String uid) {
		rellenarVacio(uid);
	}
	
	private void rellenarVacio(String uid) {

			this.uid = uid;
			this.nombre = "";
			this.apellido1 = "";
			this.apellido2 = "";
			this.descripcion = "";
			this.email = "";
			this.nif = "";
			this.pager = "";
			this.tipo = "";
			this.emailValidado = true;
			this.changePass = false;
			this.ptvFlag = false;
			this.professionalFlag = false;
			this.sapFlag = false;
			this.socialNets = null;
			this.documentId = null;
			this.webProperty = null;
			this.memberOf = null;
			this.defaultAction = true;
	}
	@Override
	public String toString() {
		return "LDapUser [ descripcion="
				+ descripcion + ", tipo=" + tipo
				+ ", emailValidado=" + emailValidado + ", changePass=" + changePass + ", ptvFlag=" + ptvFlag
				+ ", professionalFlag=" + professionalFlag + ", sapFlag=" + sapFlag + ", socialNets=" + socialNets
				+ ", webProperty=" + webProperty + ", memberOf=" + memberOf + ", uid="
				+ uid + " nifValidado="+NifValidado()+"]";
	}
	
	public String completeToString() {
		return "LDapUser [nombre=" + nombre + ", apellido1=" + apellido1 + ", apellido2=" + apellido2 + ", descripcion="
				+ descripcion + ", email=" + email + ", nif=" + nif + ", pager=" + pager + ", tipo=" + tipo
				+ ", emailValidado=" + emailValidado + ", changePass=" + changePass + ", ptvFlag=" + ptvFlag
				+ ", professionalFlag=" + professionalFlag + ", sapFlag=" + sapFlag + ", socialNets=" + socialNets
				+ ", documentId=" + documentId + ", webProperty=" + webProperty + ", memberOf=" + memberOf + ", uid="
				+ uid + " nifValidado="+NifValidado()+"]";
	}
	
	public String getIdDireccionAterrizaje() {
		if(props == null) {
			props = ResourceBundle.getBundle(RestFinalUtils.WS_BUNDLE);
		}
		String sitePrefix ="";
		if (this.getSiteName()!=null && !"".equalsIgnoreCase(this.getSiteName())){
			sitePrefix = this.getSiteName() + ".";
		}
		if(!defaultAction) {
			if(!emailValidado) {
				logger.debug("Email no validado vamos a MiPerfil. id recuperado:"+props.getString(sitePrefix+RestFinalUtils.MIPERFIL));
				return props.getString(sitePrefix+RestFinalUtils.MIPERFIL);
			}/* else if(ptvFlag ||  (emailValidado && NifValidado() && sapFlag)) {
				logger.debug("id recuperado:"+props.getString(RestFinalUtils.MIRESUMEN));
				return props.getString(RestFinalUtils.MIRESUMEN);
			} */else {
				logger.debug("Email validado vamos a TeRecomendamos. id recuperado:"+props.getString(sitePrefix+RestFinalUtils.TERECOMENDAMOS));
				return props.getString(sitePrefix+RestFinalUtils.TERECOMENDAMOS);
			}
		} else {
			return props.getString(sitePrefix+RestFinalUtils.DEFAULT);
		}
		
	}
	
	/**
	 * 
	 * @return Set<String> con los grupos a los que pertenece el usuario
	 */
	public Set<String> getGroups(){
		ArrayList<String> currentMemberOf = this.getMemberOf();
		Set<String> groups = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
		if (currentMemberOf == null){
			//Atacamos al ldap
			logger.info("Atacamos directamente al ldap para el uid: "+this.getUid());
			LdapUserVO ldapUser = LdapProxy.readLdapUsers(this.getUid());
			if (ldapUser==null){
				logger.info("ha habido un problema consultando el ldap retornamos grupos vacios ");
				return groups;
			}
			while (ldapUser.getGroups().hasMoreElements()) {
			    String group = ldapUser.getGroups().nextElement().toString();
			    if (group!=null && group.indexOf(",ou=ZONAS,o=WACcepsa,dc=cepsaweb,dc=es")>=0){
			    	String currrentGroup = group.substring(3, group.indexOf(",ou=ZONAS,o=WACcepsa,dc=cepsaweb,dc=es"));
			    	logger.debug("-----> LDAP pertenece al grupo: "+currrentGroup);
			    	groups.add(currrentGroup);
			    }
			}
			
			return groups;
		}
		
		for (String temp : currentMemberOf) {
			if(temp != null && temp.indexOf(",ou=ZONAS,o=WACcepsa,dc=cepsaweb,dc=es")>=0){
				String currrentGroup = temp.substring(3, temp.indexOf(",ou=ZONAS,o=WACcepsa,dc=cepsaweb,dc=es"));
				groups.add(currrentGroup);
				logger.debug("pertenece al grupo: "+currrentGroup);
			}
			
		}
		
		return groups;
		
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	

	
	
}
