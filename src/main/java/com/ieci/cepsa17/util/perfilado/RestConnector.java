package com.ieci.cepsa17.util.perfilado;


import java.util.HashMap;
import java.util.Set;

import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import org.apache.log4j.Logger;

public class RestConnector {
	
	 private static Logger logger = Logger.getLogger(RestConnector.class);

	private Client client;
	private WebResource service;
	private String securityToken;
	private int connReadOut,connConOut;
	private String usuarioSeteado;
	public int getConnReadOut() {
		return connReadOut;
	}

	public void setConnReadOut(int connReadOut) {
		this.connReadOut = connReadOut;
	}

	public int getConnConOut() {
		return connConOut;
	}

	public void setConnConOut(int connConOut) {
		this.connConOut = connConOut;
	}

	public void setSecurityToken(String securityTVal) {
		securityToken = securityTVal;
	}

	public RestConnector(String url, String user) {
		setParameters(url);
		usuarioSeteado = user;
    }
	
	private void setParameters(String url) {
		ClientConfig config = new DefaultClientConfig();
	    client = Client.create(config);
		service = client.resource(UriBuilder.fromUri(url).build());
		logger.debug("constructedUrl:"+url);
		

	}
	
	public RestConnector(String url,HashMap<String, ?> form) {
    	StringBuilder sb = new StringBuilder();
    	sb.append(url);
		if(form != null && form.size() > 0) {
			sb.append('?');
	    	Set<String> keys = form.keySet();
	    	for(String key : keys) {
	    		sb.append(key);
	    		sb.append('=');
	    		sb.append(form.get(key));
	    		sb.append('&');
	    	}
	    	sb.delete(sb.length()-1, sb.length());
		}
    	
    	setParameters(sb.toString());
    }
    
	public String getRequest() {
	    client.setReadTimeout(connReadOut);
	    client.setConnectTimeout(connConOut);
		ClientResponse response = null;
		logger.debug("getRequest inicia llamada:");
		response = service.header(RestFinalUtils.AUTORIZACION, securityToken)
		    .get(ClientResponse.class);
		String paradevolver = response.getEntity(String.class);
        return paradevolver;
	}
	
	
}
