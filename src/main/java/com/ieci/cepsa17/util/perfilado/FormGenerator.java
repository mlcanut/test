package com.ieci.cepsa17.util.perfilado;


import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.core.util.StringKeyIgnoreCaseMultivaluedMap;


public class FormGenerator {

	
	@SuppressWarnings("rawtypes")
	public static MultivaluedMap<String, Object>  getHeadersFromString(Map<String,String> data) {

		  MultivaluedMap<String, Object> myHeaders = new StringKeyIgnoreCaseMultivaluedMap<Object>();
		  
		  Iterator<?> it = data.entrySet().iterator();
		  while (it.hasNext()) {
		  Map.Entry e = (Map.Entry)it.next();
		  myHeaders.add((String)e.getKey() ,(String)e.getValue());
		  }
	       
	     return myHeaders;
	}
}
