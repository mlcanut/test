package com.ieci.cepsa17.util.perfilado;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;



public class RestUtil {

	
	
	public static String setBase64Authorization(String user, String password) throws UnsupportedEncodingException {
		String toConv = user +":" + password;
		byte[] message = toConv.getBytes("UTF-8");
		String encoded = DatatypeConverter.printBase64Binary(message);
		String seguridad = RestFinalUtils.BASICA +" "+encoded;
		return seguridad;
	}
	
	public static HashMap<String,Object> jsonToMap(String json) {
		try {
			@SuppressWarnings("unchecked")
			HashMap<String, Object> result = new ObjectMapper().readValue(json, HashMap.class);
			return result;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getParametersFromLAPD(String response) {

		HashMap<String,Object> preParametros = jsonToMap(response);
		Map<String,Object> parametros;
		if(preParametros != null && (Integer)preParametros.get(RestFinalUtils.STATUS) == 200) {
			parametros = (Map<String, Object>) preParametros.get(RestFinalUtils.DATA);
		} else {
			parametros = new HashMap<String,Object>();
			parametros.put(RestFinalUtils.ERROR, "error recuperando info del usuario. Consultar log para mas datos.");
		}
		
		return parametros;
	}
	 
}
