package com.ieci.cepsa17.util.perfilado;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RedirectPerfiladoUtils {

	/** The logger class*/
	private static final Log logger = LogFactory.getLog(RedirectPerfiladoUtils.class);
	
	public static final String GET_LINK_URI_JSP = "/portal_comercial/comun/getLinkURI.jsp";
	public static final String SET_SESSION_ATTRS_JSP = "/portal_comercial/comun/setSessionAttrs.jsp";
	public static final String AUTH_RESOURCE_JSP = "/portal_comercial/security/authResource.jsp";
	public static final String CLEAN_CACHE_JSP = "/portal_comercial/comun/cleanCache.jsp";
	
	
	
	public static final String GET_LINK_URI_JSP_PARAM_OBJECT = "es.cepsa.cepsa17.comercial.link.object";
	public static final String GET_LINK_URI_JSP_PARAM_FORMAT = "es.cepsa.cepsa17.portal_comercial.link.format";
	public static final String GET_LINK_URI_JSP_PARAM_URI = "linkURI";
	
	public static final String AUTH_RESOURCE_PARAM_OBJECT = "cepsa_ath_vgnextoid";
	public static final String AUTH_RESOURCE_PARAM_RESULT = "cepsa_auth_result";
	public static final String CLEAN_CACHE_GROUP_PARAM = "cleancachegroup";
	public static final String CLEAN_TOTAL_CACHE_PARAM = "cleantotalcache ";
	
	
	//TODO leerlo del properties comun
	public static final String CEPSA17_CONTEXT = "/cepsa17";
	

	/**
	 * 
	 * @param request
	 * @param response
	 * @param cacheGroup
	 */
	public static final void cleanPrivateAreaCacheGroup(HttpServletRequest request,HttpServletResponse response, String cacheGroup, Boolean cleanTotalCache){
		logger.debug("cleanPrivateAreaCacheGroup: " + cacheGroup );
		
		//CALL JSP cleanCache.jsp
		request.setAttribute(CLEAN_CACHE_GROUP_PARAM, cacheGroup);
		request.setAttribute(CLEAN_TOTAL_CACHE_PARAM, cleanTotalCache);
		ServletContext templatingServletContext = request.getSession().getServletContext().getContext(CEPSA17_CONTEXT);
		try {
			templatingServletContext.getRequestDispatcher(CLEAN_CACHE_JSP).include(request, response);
		} catch (ServletException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
		
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param vcmId
	 * @return
	 */
	public static final boolean isAuthResource(HttpServletRequest request,HttpServletResponse response, String vcmId){
		
		logger.debug("isAuthResource");
		
		//Obtenemos el id del recurso al que vamos
		request.setAttribute(AUTH_RESOURCE_PARAM_OBJECT, vcmId);
		//Introducimos el resto de valores necesarios para comprobar la seguridad
		request.setAttribute(RestFinalUtils.TIPOUSUARIO, (String)request.getSession().getAttribute(RestFinalUtils.TIPOUSUARIO));
		request.setAttribute(RestFinalUtils.EMAILVALIDADO, (Boolean)request.getSession().getAttribute(RestFinalUtils.EMAILVALIDADO));
		request.setAttribute(RestFinalUtils.NIFVALIDADO, (Boolean)request.getSession().getAttribute(RestFinalUtils.NIFVALIDADO));
		
		logger.debug(RestFinalUtils.TIPOUSUARIO + (String)request.getSession().getAttribute(RestFinalUtils.TIPOUSUARIO));
		logger.debug(RestFinalUtils.EMAILVALIDADO + (Boolean)request.getSession().getAttribute(RestFinalUtils.EMAILVALIDADO));
		logger.debug(RestFinalUtils.NIFVALIDADO + (Boolean)request.getSession().getAttribute(RestFinalUtils.NIFVALIDADO));
		
		//CALL JSP authREsource
		ServletContext templatingServletContext = request.getSession().getServletContext().getContext(CEPSA17_CONTEXT);
		try {
			templatingServletContext.getRequestDispatcher(AUTH_RESOURCE_JSP).include(request, response);
		} catch (ServletException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
		
		logger.debug("isAuthResource:"+(Boolean)request.getAttribute(AUTH_RESOURCE_PARAM_RESULT));
		return  (Boolean)request.getAttribute(AUTH_RESOURCE_PARAM_RESULT);		
		
					
		
	}
	/**
	 * Calcula y almacena en sessio la URL por defecto de aterrizaje
	 * @param request
	 * @param response
	 * @param objectId
	 * @param format
	 * @return
	 */
	public static final String buildDefaultRedirect(HttpServletRequest request,HttpServletResponse response, String objectId, String format){
		
		logger.debug("getDefaultRedirect");
		if (request.getSession().getAttribute(RestFinalUtils.URLATERRIZAJE_DEFAULT)==null){
			request.getSession().setAttribute(RestFinalUtils.URLATERRIZAJE_DEFAULT, getLinkURI(request, response, objectId, format));
			
		}
		logger.debug("getDefaultRedirect return: "+ (String)request.getSession().getAttribute(RestFinalUtils.URLATERRIZAJE_DEFAULT));
		return (String)request.getSession().getAttribute(RestFinalUtils.URLATERRIZAJE_DEFAULT);
		
	}
	/**
	 * 
	 * @param request
	 * @param response
	 * @param objectId
	 * @param format
	 * @return
	 */
	public static final String getLinkURI(HttpServletRequest request,HttpServletResponse response, String objectId, String format){
		
		logger.debug("getLinkURI");
		
		if (objectId!=null && !objectId.isEmpty()){
			request.setAttribute(GET_LINK_URI_JSP_PARAM_OBJECT, objectId);
		}
		if (format!=null && !format.isEmpty()){
			request.setAttribute(GET_LINK_URI_JSP_PARAM_FORMAT,format);
		}
		//CALL JSP buildLink
		ServletContext templatingServletContext = request.getSession().getServletContext().getContext(CEPSA17_CONTEXT);
		try {
			templatingServletContext.getRequestDispatcher(GET_LINK_URI_JSP).include(request, response);
		} catch (ServletException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
		
		logger.debug("getLinkURI: " + (String)request.getAttribute(GET_LINK_URI_JSP_PARAM_URI));
		return (String)request.getAttribute(GET_LINK_URI_JSP_PARAM_URI);
				
	}
	/**
	 * Si en session no hay url de aterrizaje devuelve la url por defecto para redirijir en caso de que esté prohibida la request
	 * @param request
	 * @param defaultRedirect
	 * @return null si no está prohibida la pagina destino, y página de redirección si está prohibida
	 */
	public static String getDefaultRedirect(HttpServletRequest request, String defaultRedirect, String[] forbids ){
		
		
		logger.debug("getDefaultRedirect");
		
		boolean redirectForForbid = false;
		if (forbids!=null){
			for(String s : forbids){
				if(request.getQueryString().contains(s.trim())){
					redirectForForbid = true;
					logger.debug("La url a la que se intenta acceder NO está permitida en la config");
				}
			}
		}else{
			logger.debug("La url a la que se intenta acceder NO está permitida por las comunidades hay que redirijir directamente");
			redirectForForbid=true;
		}
		
		if(redirectForForbid){
			if(request.getSession().getAttribute(RestFinalUtils.URLATERRIZAJE) != null){
				logger.debug("getDefaultRedirect Redirijimos a url por aterrizaje:  " + (String)request.getSession().getAttribute(RestFinalUtils.URLATERRIZAJE));
				return (String)request.getSession().getAttribute(RestFinalUtils.URLATERRIZAJE);
			} else {
				logger.debug("getDefaultRedirect Redireijimso a url por defecto:  " + defaultRedirect);
				return defaultRedirect;
			}
			
		}
		return null;
		
	}
	/**
	 * Invoca directamente a la JSP para almacenar datos en la session de cepsa17
	 * @param request
	 * @param response
	 * @return
	 */
	public static void saveSessioneDataRequest(HttpServletRequest request, HttpServletResponse response){
		
		logger.debug("saveSessioneDataRequest");
		//CALL JSP SESSION
		ServletContext templatingServletContext = request.getSession().getServletContext().getContext(CEPSA17_CONTEXT);
		try {
			templatingServletContext.getRequestDispatcher(SET_SESSION_ATTRS_JSP).include(request, response);
		} catch (ServletException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
		logger.debug("saveSessioneDataRequest Has called SET_SESSION_ATTRS_JSP");
		
	}
	/**
	 * Almacenamos en session de vap y en session de cepsa17 los valores. Tambien calculamos aterrizaje. Retorna la página de aterrizaje calculada
	 * @param request
	 * @param response
	 * @param ldapUser
	 * @return Página de aterrizaje
	 */
	public static String saveSessionData(HttpServletRequest request, HttpServletResponse response, LDapUser ldapUser){
		
		
		logger.debug("saveSessionData");
		request.getSession().setAttribute(RestFinalUtils.TIPOUSUARIO, ldapUser.getTipo());
		request.getSession().setAttribute(RestFinalUtils.EMAILVALIDADO, ldapUser.getEmailValidado());
		request.getSession().setAttribute(RestFinalUtils.NIFVALIDADO, ldapUser.NifValidado());
		
		//CALL JSP buildLink
		ServletContext templatingServletContext = request.getSession().getServletContext().getContext(CEPSA17_CONTEXT);
		try {
			request.setAttribute(GET_LINK_URI_JSP_PARAM_OBJECT, ldapUser.getIdDireccionAterrizaje());
			//Almacenamos en session tambien el ID de la url de aterrizaje
			request.getSession().setAttribute(RestFinalUtils.ID_URLATERRIZAJE, ldapUser.getIdDireccionAterrizaje());
			templatingServletContext.getRequestDispatcher(GET_LINK_URI_JSP).include(request, response);
		} catch (ServletException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
		request.getSession().setAttribute(RestFinalUtils.URLATERRIZAJE, request.getAttribute(GET_LINK_URI_JSP_PARAM_URI));
		logger.debug("saveSessionData Has called GET_LINK_URI_JSP_PARAM_URI");
		//CALL JSP SESSION
		request.setAttribute(RestFinalUtils.TIPOUSUARIO, ldapUser.getTipo());
		request.setAttribute(RestFinalUtils.EMAILVALIDADO, ldapUser.getEmailValidado());
		request.setAttribute(RestFinalUtils.NIFVALIDADO, ldapUser.NifValidado());
		request.setAttribute(RestFinalUtils.URLATERRIZAJE, request.getAttribute(GET_LINK_URI_JSP_PARAM_URI));
		request.setAttribute(RestFinalUtils.ID_URLATERRIZAJE, ldapUser.getIdDireccionAterrizaje());
		try {
			templatingServletContext.getRequestDispatcher(SET_SESSION_ATTRS_JSP).include(request, response);
		} catch (ServletException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
		logger.debug("saveSessionData Has called SET_SESSION_ATTRS_JSP");
		
		logger.debug("saveSessionData return: " + request.getAttribute(GET_LINK_URI_JSP_PARAM_URI));
		return (String) request.getAttribute(GET_LINK_URI_JSP_PARAM_URI);
		
	}
	
	
}
