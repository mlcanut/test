package com.ieci.cepsa17.util.perfilado;

public class RestFinalUtils {
	public static final String WS_BUNDLE = "com.cepsa.portal.perfilado.config";
	public static final String MIPERFIL = "miperfil";
	public static final String TERECOMENDAMOS = "terecomendamos";
	public static final String MIRESUMEN = "miresumen";
	public static final String AUTORIZACION = "Authorization";
	public static final String BASICA = "Basic";
	public static final String DATA = "data";
	public static final String STATUS = "status";
	public static final String ERROR = "error";
	public static final String URLKEY = "urlCepsaKey";
	public static final String USER = "usuario";
	public static final String PASSWORD = "clave";
	public static final String TIMEOUTSOCKET = "TimeOutSocket";
	public static final String TIMEOUTREAD = "TimeOutRead";
	public static final String DEFAULT = "urlPorDefecto";
	public static final String EMAILVALIDADO = "emailValidado";
	public static final String NIFVALIDADO = "nifValidado";
	public static final String TIPOUSUARIO = "tipoUsuario";
	public static final String URLATERRIZAJE = "urlDestino";
	public static final String URLATERRIZAJE_DEFAULT = "defaultRedirect";
	public static final String FORBIDEN = "forbiden";
	public static final String ID_URLATERRIZAJE = "idurlaterrizaje";
	
}
