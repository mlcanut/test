package com.ieci.cepsa17.util.perfilado;


import java.io.IOException;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

public class CepsaLdapRequest {

	

	private String UrlRest;
	private ResourceBundle resourceBundle;
    private String securityToken;
    String timeOutRead;
    String timeOutSocket;
	
    private static Logger logger = Logger.getLogger(CepsaLdapRequest.class);
    
	public CepsaLdapRequest() throws IOException,NullPointerException {
		
        logger.debug("inicia peticion");
		resourceBundle = ResourceBundle.getBundle(RestFinalUtils.WS_BUNDLE);
		
		String authUser=resourceBundle.getString(RestFinalUtils.USER);
		String authPassword=resourceBundle.getString(RestFinalUtils.PASSWORD);
		UrlRest=resourceBundle.getString(RestFinalUtils.URLKEY);
		
		timeOutRead=resourceBundle.getString(RestFinalUtils.TIMEOUTREAD);
		logger.debug("timeout:"+timeOutRead);
		timeOutSocket=resourceBundle.getString(RestFinalUtils.TIMEOUTSOCKET);
		logger.debug("timeout2:"+timeOutSocket);
        
		securityToken = RestUtil.setBase64Authorization(authUser,authPassword);
	}
	
	
	
	private RestConnector conexionUsuario(String usuario) {
		String urlUsuario = UrlRest.replace("{user}", usuario);
		
		RestConnector request = new RestConnector(urlUsuario,usuario);
		if(timeOutRead != null && !timeOutRead.isEmpty()) {
			request.setConnReadOut(Integer.parseInt(timeOutRead));
		} else {
			request.setConnReadOut(1000);
		}
		if(timeOutSocket != null && !timeOutSocket.isEmpty()) {
			request.setConnConOut(Integer.parseInt(timeOutSocket));
		} else {
			request.setConnConOut(1000);
		}
		request.setSecurityToken(securityToken);
		logger.debug("creado usuario");
        return request;
	}
	
	
	public LDapUser getLdapUser(String user) {
		
		RestConnector conn = conexionUsuario(user);
		String sourceRequest = conn.getRequest();
		Map<String,Object> infoReq = RestUtil.getParametersFromLAPD(sourceRequest);
		LDapUser usuario;
		if(infoReq.containsKey(RestFinalUtils.ERROR)) {
			usuario = null;
		} else {
			usuario = new LDapUser(user,infoReq);
		}
		return usuario;
	}
	
	public static CepsaLdapRequest getDefaultConnection() throws NullPointerException, IOException {
		return new CepsaLdapRequest();
	}
	
	public static LDapUser consultarUsuario(String usuario) {
		try {
			CepsaLdapRequest c = new CepsaLdapRequest();
			LDapUser p = c.getLdapUser(usuario);
			p.setProperties(c.resourceBundle);
			return p;
		} catch (Exception e){
			logger.debug("Perfilado Error consultarUsuario: "+e.getMessage(),e);
			return new LDapUser(usuario);
		}
	}
	
	public static String getUsuarioCodigoAterizaje(String usuario) {
		try {
		CepsaLdapRequest c = new CepsaLdapRequest();
		LDapUser p = c.getLdapUser(usuario);
		p.setProperties(c.resourceBundle);
		return p.getIdDireccionAterrizaje();
		}
		catch (Exception e){
			logger.debug("Perfilado Error getUsuarioCodigoAterizaje: "+e.getMessage(),e);
			return new LDapUser(usuario).getIdDireccionAterrizaje();
		}
	}
}
