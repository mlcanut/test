package com.ieci.cepsa17.util;

import com.vignette.logging.Category;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Clase de utilidad para el manejo de las fechas
 * @author 66083486
 *
 */
public class DateUtils
{
	/**
	 * Constructor
	 *
	 */
    public DateUtils()
    {
    }

	/**
	 * Indica el dia de comienzo
	 * @param date Date de la fecha
	 * @return Date con el  dia de comienzo
	 */
	public static Date getDayBeginning(Date date) {
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		
		return getDayBeginning(
				calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH));
	}
	
	/**
	 * Indica el dia de finalizacion
	 * @param date Date de la fecha
	 * @return Date con el dia de finalizacion
	 */
	public static Date getDayEnding(Date date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		
		return getDayEnding(
				calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH));		
	}
	
	/**
	 * @param year
	 * @param zeroBasedMonth
	 * @param day
	 * @return
	 */
	public static Date getDayBeginning(int year, int zeroBasedMonth, int day) {

		if (logger.isDebugEnabled())
			logger.debug("| getDayBeginning | TRACE -->"+
					" year: "+year+
					" zeroBasedMonth: "+ zeroBasedMonth+
					" day: "+day);
			
		GregorianCalendar calendar = new GregorianCalendar(year, zeroBasedMonth, day);
		return new Date(calendar.getTimeInMillis());		
	}
	
	/**
	 * @param year
	 * @param zeroBasedMonth
	 * @param day
	 * @return
	 */
	public static Date getDayEnding(int year, int zeroBasedMonth, int day) {

		if (logger.isDebugEnabled())
			logger.debug("| getDayEnding | TRACE -->"+
					" year: "+year+
					" zeroBasedMonth: "+ zeroBasedMonth+
					" day: "+day);
		
		GregorianCalendar calendar = new GregorianCalendar(year, zeroBasedMonth, day);
		calendar.add(Calendar.DATE, 1);
		return new Date(calendar.getTimeInMillis()-1);
	}
	
	/**
	 * Comprueba si una fecha es valida o no 
	 * @param anio String con el año
	 * @param mes String con el mes
	 * @param dia String con el dia
	 * @return boolean indicando si la fecha es valida o no
	 */
    public static boolean isValidDate(String anio, String mes, String dia)
    {
        boolean valido = false;
        String dt = "";
        if(anio == null || mes == null || dia == null)
            return false;
        dt = dt.concat(anio + "/" + mes + "/" + dia);
        logger.debug("DTTTTT" + dt);
        String fdt = "yyyy/MM/dd";
        SimpleDateFormat sdf = new SimpleDateFormat(fdt);
        sdf.setLenient(false);
        try
        {
            Date dt2 = sdf.parse(dt);
            valido = true;
        }
        catch(ParseException e)
        {
            valido = false;
        }
        return valido;
    }

    /**
     * Comprueba si la fecha inicial y la fecha final de un periodo de tiempo son validas o no
     * El formato de la fecha ha de ser dd/MM/yyyy
     * @param fechaInicial String con la fecha inicial, en formato dd/MM/yyyy
     * @param fechaFinal String con la fecha final, en formato dd/MM/yyyy
     * @return boolean que indica si la fecha inicial y la fecha final son validas o no
     */
    public static boolean isValidDate(String fechaInicial, String fechaFinal)
    {
        if(logger.isDebugEnabled())
            logger.debug("| DateUtils.isValidDate | TRACE -> fechaInicial=" + fechaInicial + ", fechaFinal=" + fechaFinal);
		boolean returnValue = false;
        Date f = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String fechaActual = sdf.format(f);
        boolean menor = false;
        boolean mayor = false;
        int diaI = Integer.parseInt(fechaInicial.substring(0, 2));
        int mesI = Integer.parseInt(fechaInicial.substring(3, 5));
        int anioI = Integer.parseInt(fechaInicial.substring(6, 10));
        int diaF = Integer.parseInt(fechaFinal.substring(0, 2));
        int mesF = Integer.parseInt(fechaFinal.substring(3, 5));
        int anioF = Integer.parseInt(fechaFinal.substring(6, 10));
        int diaA = Integer.parseInt(fechaActual.substring(0, 2));
        int mesA = Integer.parseInt(fechaActual.substring(3, 5));
        int anioA = Integer.parseInt(fechaActual.substring(6, 10));
        if(anioI < anioA)
            menor = true;
        if(anioI <= anioA && mesI < mesA)
            menor = true;
        if(anioI <= anioA && mesI <= mesA && diaI < diaA)
            menor = true;
        if(anioA < anioF)
            mayor = true;
        if(anioA <= anioF && mesA < mesF)
            mayor = true;
        if(anioA <= anioF && mesA <= mesF && diaA < diaF)
            mayor = true;
        
        if(menor && mayor)
            returnValue = true;
        else
            returnValue = false;
        if(logger.isDebugEnabled())
            logger.debug("| DateUtils.isValidDate return:" + returnValue);
        return returnValue;
    }

    /**
     * Trasforma una fecha de formato yyyy/MM/dd a formato dd/MM/yyy
     * @param f String con la fecha en formato yyyy/MM/dd
     * @return String con la fecha en formato dd/MM/yyy
     */
    public static String formatFecha(String f)
    {
        if(logger.isDebugEnabled())
            logger.debug("| DateUtils.formatFecha(String f=" + f + ")");
        String anio = f.substring(0, 4);
        String mes = f.substring(5, 7);
        String dia = f.substring(8, 10);
        String fecha = dia + "/" + mes + "/" + anio;
        if(logger.isDebugEnabled())
            logger.debug("| DateUtils.formatFecha:return:" + fecha);
        return fecha;
    }

    /**
     * Obtiene el nombre dia de la semana de <code>fecha</code>
     * @param fecha Long con una fecha en formato dd/MM/yyyy
     * @return String con el dia de la semana (Lunes, Martes Miercoles, ....)
     */
    public static String dateToDayOfWeekString(long fecha)
    {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(fecha);
        int numDia = c.get(7);
        if(--numDia > 6)
            numDia %= 7;
        String diasSemana[] = {
            "Domingo", "Lunes", "Martes", "Mi\351rcoles", "Jueves", "Viernes", "S\341bado"
        };
        return diasSemana[numDia];
    }

    /**
     * Obtiene el nombre del mes de <code>fecha</code>
     * @param fecha Long con una fecha en formato dd/MM/yyyy
     * @return String con el nombre del mes (Enero, Febrero, Marzo, Abril,....)
     */
    public static String getMes(long fecha)
    {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(fecha);
        int numMes = c.get(2);
        String meses[] = {
            "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", 
            "Noviembre", "Diciembre"
        };
        return meses[numMes];
    }

    /**
     * Indica el nombre del dia
     * @param dayOfWeek int con el numero de dia de la semana (1..7)
     * @return String con el dia de la semana (Lunes, Martes Miercoles, ....)
     */
    public static String intToDayofWeekString(int dayOfWeek)
    {
        int numDia = dayOfWeek - 1;
        if(numDia > 6)
            numDia %= 7;
        String diasSemana[] = {
            "Domingo", "Lunes", "Martes", "Mi\351rcoles", "Jueves", "Viernes", "S\341bado"
        };
        return diasSemana[numDia];
    }

    /**
     * Obtiene la fecha actual en milisegundos
     * @return Long con la fecha actual en milisegundos
     */
    public static long getTodayTimeInMillis()
    {
        Calendar choy = Calendar.getInstance();
        return choy.getTimeInMillis() - (long)(choy.get(Calendar.HOUR_OF_DAY) * DateUtils.SEGUNDOS_EN_HORA * DateUtils.SEGUNDOS_A_MILISEGUNDOS + choy.get(Calendar.MINUTE) * DateUtils.SEGUNDOS_EN_MINUTO * DateUtils.SEGUNDOS_A_MILISEGUNDOS + choy.get(Calendar.SECOND) * DateUtils.SEGUNDOS_A_MILISEGUNDOS + choy.get(Calendar.MILLISECOND));
    }

    /**
     * Devuelve la fecha actual
     * @return Date con la fecha actual
     */
    public static Date getToday()
    {
        return new Date(getTodayTimeInMillis());
    }

    /**
     * Numero de segundo que tiene una hora
     */
	public static int SEGUNDOS_EN_HORA = 3600;
	/**
	 * Numero de segundo que tiene un minuto
	 */
	public static int SEGUNDOS_EN_MINUTO = 60;
	/**
	 * Numero de milisegundos que tiene un minutos
	 */
	public static int SEGUNDOS_A_MILISEGUNDOS = 1000;
	
	private static final Category logger = Category
	.getInstance(DateUtils.class);	
}
