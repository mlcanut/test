package com.ieci.cepsa17.util;

/**
 * Interface que contiene constantes con el nombre de las claves que almacenan la locale en los distintos ambitos
 * 
 */
public interface I18nHttpConstants
{
	
    public static final String I18N_LANGREQUESTKEY = "lang_choosen";
    
}


