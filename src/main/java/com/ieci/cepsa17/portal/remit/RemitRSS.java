package com.ieci.cepsa17.portal.remit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.ieci.cepsa17.util.DBUtil;


public class RemitRSS {

	public static final String REMIT_BUNDLE = "com.cepsa.portal.remit.config";
	
	public static final String REMIT_PARAM_TYPE_EXTERNAS = "externas";
	public static final String REMIT_PARAM_TYPE_INTERNAS = "internas";
	
	public static final String REMIT_DATASOURCE = "remit.datasource";
	public static final String REMIT_RSS_QUERY_EXTERNAS ="remit.rss.query.externas";
	public static final String REMIT_RSS_QUERY_EXTERNAS_HISTORICO ="remit.rss.query.externas.historico";
	public static final String REMIT_RSS_QUERY_INTERNAS ="remit.rss.query.internas";
	public static final String REMIT_RSS_QUERY_INTERNAS_HISTORICO ="remit.rss.query.internas.historico";
	
	public static final String REMIT_RSS_CHANNELS_INTERNAS = "remit.rss.channelid.internas";
	public static final String REMIT_RSS_CHANNELS_EXTERNAS = "remit.rss.channelid.externas";
	
	public static final String KEY_ID_MENSAJE = "id_mensaje";
	public static final String KEY_ESTADO ="estado";
	public static final String KEY_TIPO ="tipo"; //planned or unplanned
	public static final String KEY_TIPO_EVENTO="tipo_evento";
	public static final String KEY_ULTIMA_FECHA_PUBLICACION ="ultima_fecha_pub";
	public static final String KEY_FECHA_INICIO = "fecha_inicio";
	public static final String KEY_FECHA_FIN = "fecha_fin";
	public static final String KEY_UNIDAD="unidad";
	public static final String KEY_CAPACIDAD_INDISPONIBLE = "capacidad_indisponible";
	public static final String KEY_CAPACIDAD_DISPONIBLE = "capacidad_disponible";
	public static final String KEY_CAPACIDAD_INSTALADA = "capacidad_instalada";
	public static final String KEY_RAZON_INIDPONIBILIDAD = "razon_indisponibilidad";
	public static final String KEY_COMBUSTIBLE = "combustible";
	public static final String KEY_COMENTARIOS = "comentarios";
	public static final String KEY_ZONA_OFERTA ="zona_oferta";
	public static final String KEY_UNIDAD_AFECTADA = "unidad_afectada";
	public static final String KEY_CODIGO_EIC_UNIDAD =  "codigo_eic_unidad";
	public static final String KEY_PARTICIPANTE_MERCADO  = "participante_mercado";
	public static final String KEY_CODIGO_PARTICIPANTE_MERCADO="codigo_participante_mercado";

	  
	  
	
	
	static Map<String, Integer> mapFieldsES = new HashMap<String, Integer>();
	static Map<String, Integer> mapFieldsEN = new HashMap<String, Integer>();
	static{
		mapFieldsES.put(KEY_ID_MENSAJE, 1);
		mapFieldsES.put(KEY_ESTADO, 2);
		mapFieldsES.put(KEY_TIPO, 3);
		mapFieldsES.put(KEY_TIPO_EVENTO, 4);
		mapFieldsES.put(KEY_ULTIMA_FECHA_PUBLICACION, 5);
		mapFieldsES.put(KEY_FECHA_INICIO, 6);
		mapFieldsES.put(KEY_FECHA_FIN, 7);
		mapFieldsES.put(KEY_UNIDAD, 8);
		mapFieldsES.put(KEY_CAPACIDAD_INDISPONIBLE, 9);
		mapFieldsES.put(KEY_CAPACIDAD_DISPONIBLE, 10);
		mapFieldsES.put(KEY_CAPACIDAD_INSTALADA, 11);
		mapFieldsES.put(KEY_RAZON_INIDPONIBILIDAD, 12);
		mapFieldsES.put(KEY_COMENTARIOS, 13);
		mapFieldsES.put(KEY_COMBUSTIBLE, 14);
		mapFieldsES.put(KEY_ZONA_OFERTA, 15);
		mapFieldsES.put(KEY_UNIDAD_AFECTADA, 16);
		mapFieldsES.put(KEY_CODIGO_EIC_UNIDAD, 17);
		mapFieldsES.put(KEY_PARTICIPANTE_MERCADO, 18);
		mapFieldsES.put(KEY_CODIGO_PARTICIPANTE_MERCADO, 19);
	}
	static{
		mapFieldsEN.put(KEY_ID_MENSAJE, 1);
		mapFieldsEN.put(KEY_ESTADO, 2);
		mapFieldsEN.put(KEY_TIPO, 3);
		mapFieldsEN.put(KEY_TIPO_EVENTO, 4);
		mapFieldsEN.put(KEY_ULTIMA_FECHA_PUBLICACION, 5);
		mapFieldsEN.put(KEY_FECHA_INICIO, 6);
		mapFieldsEN.put(KEY_FECHA_FIN, 7);
		mapFieldsEN.put(KEY_UNIDAD, 8);
		mapFieldsEN.put(KEY_CAPACIDAD_INDISPONIBLE, 9);
		mapFieldsEN.put(KEY_CAPACIDAD_DISPONIBLE, 10);
		mapFieldsEN.put(KEY_CAPACIDAD_INSTALADA, 11);
		mapFieldsEN.put(KEY_RAZON_INIDPONIBILIDAD, 12);
		mapFieldsEN.put(KEY_COMENTARIOS, 13);
		mapFieldsEN.put(KEY_COMBUSTIBLE, 14);
		mapFieldsEN.put(KEY_ZONA_OFERTA, 15);
		mapFieldsEN.put(KEY_UNIDAD_AFECTADA, 16);
		mapFieldsEN.put(KEY_CODIGO_EIC_UNIDAD, 17);
		mapFieldsEN.put(KEY_PARTICIPANTE_MERCADO, 18);
		mapFieldsEN.put(KEY_CODIGO_PARTICIPANTE_MERCADO, 19);
	}
	
	static ResourceBundle resourceBundle;
	
	private static Logger logger = Logger.getLogger(RemitRSS.class);
	
	public static String getJsonRemitData(Locale locale, boolean historico,String tipoQuery){
		
		logger.debug(String.format("RemitRSS getJsonRemitData locale %s historico %b tipoQuery %s ", locale.toString(), historico, tipoQuery));
				
		Map<String, Integer> currentMap = null;
			
		
		if (locale!=null && "es".equalsIgnoreCase(locale.getLanguage())){
			currentMap = mapFieldsES;
			logger.debug("RemitRSS getJsonRemitData mapFieldsES ");
			
		}else if (locale!=null && "en".equalsIgnoreCase(locale.getLanguage())){
			logger.debug("RemitRSS getJsonRemitData mapFieldsES ");
			currentMap = mapFieldsEN;
		}
		//return JSON "{ \"key\":\"zzzzzzzzzz\", \"key\":\"xxxxxxxxxxxx\", ... }";
				
		return getRemitData(locale,historico,currentMap, tipoQuery);
				
		
	
	}
	/**
	 * Retorna el resulset con los resultados de la query adecuada
	 * @param locale
	 * @param vcmChannelId
	 * @param historico
	 * @return
	 */
	static String getRemitData(Locale locale,boolean historico, Map<String, Integer> currentMap,String tipoQuery){
		logger.debug(String.format("RemitRSS getRemitData locale %s historico %b tipoQuery %s", locale.toString(), historico,tipoQuery));
		
		ResultSet rs=null;
		PreparedStatement statement = null;
		StringBuilder sb = new StringBuilder();
		
		resourceBundle= ResourceBundle.getBundle(REMIT_BUNDLE, locale);
		String query = getQueryToExecute(historico,tipoQuery, resourceBundle);
		logger.debug(String.format("RemitRSS getRemitData query: %s", query));
		
		Connection conn = DBUtil.getConnection(resourceBundle.getString(REMIT_DATASOURCE));
		if (conn==null){
			logger.debug("RemitRSS getRemitData return []");
			sb.append("[]");
			return sb.toString();
		}
		logger.debug("RemitRSS getRemitData ha obtenido conn");
		try {
			statement =  conn.prepareStatement(query);
			rs = statement.executeQuery();
			//Iteramos y devolvemos JSON
			
			sb.append("[");
			try {
				while (rs.next()) {
					if (sb.length()!=1){
						sb.append(",");
					}
					sb.append(generateRowJson(rs, currentMap));
									
				}
			} catch (SQLException e) {
				logger.error(e,e);
			} catch (ParseException e) {
				logger.error(e,e);
			}
			sb.append("]");
			
		} catch (SQLException e) {
			logger.error("RemitRSS getRemitData Error ejecutando la query",e);
		}finally {

			try {
				statement.close();
				if (rs!=null){
					logger.debug("RemitRSS getRemitData cerrando rs");
					rs.close();
				}
			} catch (SQLException e) {
				logger.error("RemitRSS getRemitData Error cerrando resultSet y statement",e);

			}
			DBUtil.closeConnection(conn);
			logger.debug("RemitRSS getRemitData cerrando conn");
		}

		logger.debug("RemitRSS getRemitData result: " + sb.toString());
		return sb.toString();
	}



	private static String generateRowJson(ResultSet rs, Map<String, Integer> currentMap) throws ParseException, SQLException {
		SimpleDateFormat fromDatabase = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-ddHH:mm:ssZ");
		SimpleDateFormat myFormatWithOutTime = new SimpleDateFormat("dd/MM/yyyy");
		
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("\""+KEY_ID_MENSAJE+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_ID_MENSAJE)));
		sb.append("\",");
		sb.append("\""+KEY_ESTADO+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_ESTADO)));
		sb.append("\",");
		sb.append("\""+KEY_TIPO+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_TIPO)));
		sb.append("\",");
		sb.append("\""+KEY_TIPO_EVENTO+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_TIPO_EVENTO)));
		sb.append("\",");
		sb.append("\""+KEY_ULTIMA_FECHA_PUBLICACION+"\":\"");
		sb.append(myFormat.format(fromDatabase.parse(rs.getString(currentMap.get(KEY_ULTIMA_FECHA_PUBLICACION)))));
		sb.append("\",");
		sb.append("\""+KEY_FECHA_INICIO+"\":\"");
		sb.append(myFormat.format(fromDatabase.parse(rs.getString(currentMap.get(KEY_FECHA_INICIO)))));
		sb.append("\",");
		sb.append("\""+KEY_FECHA_FIN+"\":\"");
		sb.append(myFormat.format(fromDatabase.parse(rs.getString(currentMap.get(KEY_FECHA_FIN)))));
		sb.append("\",");
		sb.append("\""+KEY_UNIDAD+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_UNIDAD)));
		sb.append("\",");
		sb.append("\""+KEY_CAPACIDAD_INDISPONIBLE+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_CAPACIDAD_INDISPONIBLE)));
		sb.append("\",");
		sb.append("\""+KEY_CAPACIDAD_DISPONIBLE+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_CAPACIDAD_DISPONIBLE)));
		sb.append("\",");
		sb.append("\""+KEY_CAPACIDAD_INSTALADA+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_CAPACIDAD_INSTALADA)));
		sb.append("\",");
		sb.append("\""+KEY_RAZON_INIDPONIBILIDAD+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_RAZON_INIDPONIBILIDAD)));
		sb.append("\",");
		sb.append("\""+KEY_COMBUSTIBLE+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_COMBUSTIBLE)));
		sb.append("\",");
		sb.append("\""+KEY_COMENTARIOS+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_COMENTARIOS)) != null ? rs.getString(currentMap.get(KEY_COMENTARIOS)) : "");
		sb.append("\",");
		sb.append("\""+KEY_ZONA_OFERTA+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_ZONA_OFERTA)));
		sb.append("\",");
		sb.append("\""+KEY_UNIDAD_AFECTADA+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_UNIDAD_AFECTADA)));
		sb.append("\",");
		sb.append("\""+KEY_CODIGO_EIC_UNIDAD+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_CODIGO_EIC_UNIDAD)));
		sb.append("\",");
		sb.append("\""+KEY_PARTICIPANTE_MERCADO+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_PARTICIPANTE_MERCADO)));
		sb.append("\",");
		sb.append("\""+KEY_CODIGO_PARTICIPANTE_MERCADO+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_CODIGO_PARTICIPANTE_MERCADO)));
		sb.append("\"");
		sb.append("}");
		
		logger.debug("RemitRSS generateRowJson row: " + sb.toString());
		return sb.toString();
		
		
	}
	/**
	 * Calcula la query a ejecutar en función del canal y de si es un historico o no
	 * @param locale
	 * @param vcmChannelId
	 * @param historico
	 * @param resourceBundle
	 * @return
	 */
	private static String getQueryToExecute(boolean historico, String tipoQuery, ResourceBundle resourceBundle) {
		
		logger.debug(String.format("RemitRSS getQueryToExecute historico %b tipoQuery ", historico,tipoQuery));
				
		if (REMIT_PARAM_TYPE_EXTERNAS.equalsIgnoreCase(tipoQuery)){
	    	logger.debug(String.format("Remit getQueryToExecute REMIT_PARAM_TYPE_EXTERNAS equals %s",tipoQuery));
	    	return historico?resourceBundle.getString(REMIT_RSS_QUERY_EXTERNAS_HISTORICO):resourceBundle.getString(REMIT_RSS_QUERY_EXTERNAS);
	    	
	    }else if (REMIT_PARAM_TYPE_INTERNAS.equalsIgnoreCase(tipoQuery)){
	    	logger.debug(String.format("Remit getQueryToExecute REMIT_PARAM_TYPE_INTERNAS equals %s",tipoQuery));
	    	return historico?resourceBundle.getString(REMIT_RSS_QUERY_INTERNAS_HISTORICO):resourceBundle.getString(REMIT_RSS_QUERY_INTERNAS);
	    }
		
		logger.debug("RemitRSS getQueryToExecute return ''");
		return "";
	
		
	}
	
}
