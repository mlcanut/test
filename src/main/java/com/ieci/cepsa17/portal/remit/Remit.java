package com.ieci.cepsa17.portal.remit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.ieci.cepsa17.util.DBUtil;

public class Remit {

	public static final String REMIT_BUNDLE = "com.cepsa.portal.remit.config";
	
	public static final String REMIT_PARAM_TYPE_EXTERNAS = "externas";
	public static final String REMIT_PARAM_TYPE_INTERNAS = "internas";
	
	public static final String REMIT_DATASOURCE = "remit.datasource";
	public static final String REMIT_QUERY_EXTERNAS ="remit.query.externas";
	public static final String REMIT_QUERY_EXTERNAS_HISTORICO ="remit.query.externas.historico";
	public static final String REMIT_QUERY_INTERNAS ="remit.query.internas";
	public static final String REMIT_QUERY_INTERNAS_HISTORICO ="remit.query.internas.historico";
	
	public static final String REMIT_CHANNELS_INTERNAS = "remit.channelid.internas";
	public static final String REMIT_CHANNELS_EXTERNAS = "remit.channelid.externas";
	
	public static final String KEY_ULTIMA_FECHA_PUB ="ultima_fecha_pub";
	public static final String KEY_FECHA_INICIO = "fecha_inicio";
	public static final String KEY_FECHA_FIN = "fecha_fin";
	public static final String KEY_INSTALACION ="instalacion";
	public static final String KEY_TIPO_MERCADO="tipo_mercado";
	public static final String KEY_CAPACIDAD_INDISPONIBLE="capacidad_indisponible";
	public static final String KEY_CAPACIDAD_DISPONIBLE="capacidad_disponible";
	public static final String KEY_COMBUSTIBLE="combustible";
	public static final String KEY_RAZON_INDISPONIBILIDAD="razon_indisponibilidad";
	public static final String KEY_VERSION="version";
	public static final String KEY_COMENTARIOS="comentarios";
	public static final String KEY_TIPO="tipo";
	
	static Map<String, Integer> mapFieldsES = new HashMap<String, Integer>();
	static Map<String, Integer> mapFieldsEN = new HashMap<String, Integer>();
	static{
		mapFieldsES.put(KEY_ULTIMA_FECHA_PUB, 1);
		mapFieldsES.put(KEY_FECHA_INICIO, 2);
		mapFieldsES.put(KEY_FECHA_FIN, 3);
		mapFieldsES.put(KEY_INSTALACION, 4);
		mapFieldsES.put(KEY_TIPO_MERCADO, 5);
		mapFieldsES.put(KEY_CAPACIDAD_INDISPONIBLE, 6);
		mapFieldsES.put(KEY_CAPACIDAD_DISPONIBLE, 7);
		mapFieldsES.put(KEY_COMBUSTIBLE, 8);
		mapFieldsES.put(KEY_RAZON_INDISPONIBILIDAD, 9);
		mapFieldsES.put(KEY_VERSION, 10);
		mapFieldsES.put(KEY_COMENTARIOS, 11);
		mapFieldsES.put(KEY_TIPO, 12);
	}
	static{
		mapFieldsEN.put(KEY_ULTIMA_FECHA_PUB, 1);
		mapFieldsEN.put(KEY_FECHA_INICIO, 2);
		mapFieldsEN.put(KEY_FECHA_FIN, 3);
		mapFieldsEN.put(KEY_INSTALACION, 4);
		mapFieldsEN.put(KEY_TIPO_MERCADO, 5);
		mapFieldsEN.put(KEY_CAPACIDAD_INDISPONIBLE, 6);
		mapFieldsEN.put(KEY_CAPACIDAD_DISPONIBLE, 7);
		mapFieldsEN.put(KEY_COMBUSTIBLE, 8);
		mapFieldsEN.put(KEY_RAZON_INDISPONIBILIDAD, 9);
		mapFieldsEN.put(KEY_VERSION, 10);
		mapFieldsEN.put(KEY_COMENTARIOS, 11);
		mapFieldsEN.put(KEY_TIPO, 12);
	}
	
	static ResourceBundle resourceBundle;
	
	private static Logger logger = Logger.getLogger(Remit.class);
	
	public static String getJsonRemitData(Locale locale,boolean historico, String tipoQuery){
		
		logger.debug(String.format("Remit getJsonRemitData locale %s historico %b TipoQuery %s ", locale.toString(), historico,tipoQuery));
				
		Map<String, Integer> currentMap = null;
			
		if (locale!=null && "es".equalsIgnoreCase(locale.getLanguage())){
			currentMap = mapFieldsES;
			logger.debug("Remit getJsonRemitData mapFieldsES ");
			
		}else if (locale!=null && "en".equalsIgnoreCase(locale.getLanguage())){
			logger.debug("Remit getJsonRemitData mapFieldsES ");
			currentMap = mapFieldsEN;
		}
		//return JSON "{ \"key\":\"zzzzzzzzzz\", \"key\":\"xxxxxxxxxxxx\", ... }";
				
		return getRemitData(locale,historico,currentMap, tipoQuery);
				
		
	
	}
	/**
	 * Retorna el resulset con los resultados de la query adecuada
	 * @param locale
	 * @param vcmChannelId
	 * @param historico
	 * @return
	 */
	static String getRemitData(Locale locale,boolean historico, Map<String, Integer> currentMap, String tipoQuery){
		logger.debug(String.format("Remit getRemitData locale %s historico %b tipoQuery %s", locale.toString(), historico, tipoQuery));
		
		ResultSet rs=null;
		PreparedStatement statement = null;
		StringBuilder sb = new StringBuilder();
		
		resourceBundle= ResourceBundle.getBundle(REMIT_BUNDLE, locale);
		String query = getQueryToExecute(historico, tipoQuery, resourceBundle);
		logger.debug(String.format("Remit getRemitData query: %s", query));
		
		Connection conn = DBUtil.getConnection(resourceBundle.getString(REMIT_DATASOURCE));
		if (conn==null){
			logger.debug("Remit getRemitData return []");
			sb.append("[]");
			return sb.toString();
		}
		logger.debug("Remit getRemitData ha obtenido conn");
		try {
			statement =  conn.prepareStatement(query);
			rs = statement.executeQuery();
			//Iteramos y devolvemos JSON
			
			sb.append("[");
			try {
				while (rs.next()) {
					if (sb.length()!=1){
						sb.append(",");
					}
					sb.append(generateRowJson(rs, currentMap));
									
				}
			} catch (SQLException e) {
				logger.error(e,e);
			} catch (ParseException e) {
				logger.error(e,e);
			}
			sb.append("]");
			
		} catch (SQLException e) {
			logger.error("Remit getRemitData Error ejecutando la query",e);
		}finally {

			try {
				statement.close();
				if (rs!=null){
					logger.debug("Remit getRemitData cerrando rs");
					rs.close();
				}
			} catch (SQLException e) {
				logger.error("Remit getRemitData Error cerrando resultSet y statement",e);

			}
			DBUtil.closeConnection(conn);
			logger.debug("Remit getRemitData cerrando conn");
		}

		logger.debug("Remit getRemitData result: " + sb.toString());
		return sb.toString();
	}



	private static String generateRowJson(ResultSet rs, Map<String, Integer> currentMap) throws ParseException, SQLException {
		SimpleDateFormat fromDatabase = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat myFormatWithOutTime = new SimpleDateFormat("dd/MM/yyyy");
		
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("\""+KEY_ULTIMA_FECHA_PUB+"\":\"");
		sb.append(myFormatWithOutTime.format(fromDatabase.parse(rs.getString(currentMap.get(KEY_ULTIMA_FECHA_PUB)))));
		sb.append("\",");
		sb.append("\""+KEY_FECHA_INICIO+"\":\"");
		sb.append(myFormatWithOutTime.format(fromDatabase.parse(rs.getString(currentMap.get(KEY_FECHA_INICIO)))));
		sb.append("\",");
		sb.append("\""+KEY_FECHA_FIN+"\":\"");
		sb.append(myFormatWithOutTime.format(fromDatabase.parse(rs.getString(currentMap.get(KEY_FECHA_FIN)))));
		sb.append("\",");
		sb.append("\""+KEY_INSTALACION+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_INSTALACION)));
		sb.append("\",");
		sb.append("\""+KEY_TIPO_MERCADO+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_TIPO_MERCADO)));
		sb.append("\",");
		sb.append("\""+KEY_CAPACIDAD_INDISPONIBLE+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_CAPACIDAD_INDISPONIBLE)));
		sb.append("\",");
		sb.append("\""+KEY_CAPACIDAD_DISPONIBLE+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_CAPACIDAD_DISPONIBLE)));
		sb.append("\",");
		sb.append("\""+KEY_COMBUSTIBLE+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_COMBUSTIBLE)));
		sb.append("\",");
		sb.append("\""+KEY_RAZON_INDISPONIBILIDAD+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_RAZON_INDISPONIBILIDAD)));
		sb.append("\",");
		sb.append("\""+KEY_VERSION+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_VERSION)));
		sb.append("\",");
		sb.append("\""+KEY_COMENTARIOS+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_COMENTARIOS))!=null?rs.getString(currentMap.get(KEY_COMENTARIOS)):"");
		sb.append("\",");
		sb.append("\""+KEY_TIPO+"\":\"");
		sb.append(rs.getString(currentMap.get(KEY_TIPO)));
		sb.append("\"");
		sb.append("}");
		
		logger.debug("Remit generateRowJson row: " + sb.toString());
		return sb.toString();
		
		
	}
	/**
	 * Calcula la query a ejecutar en función del canal y de si es un historico o no
	 * @param locale
	 * @param vcmChannelId
	 * @param historico
	 * @param resourceBundle
	 * @return
	 */
	private static String getQueryToExecute(boolean historico, String tipoQuery, ResourceBundle resourceBundle) {
		
		logger.debug(String.format("Remit getQueryToExecute historico %b tipoQuery %s ", historico, tipoQuery));
	    if (REMIT_PARAM_TYPE_EXTERNAS.equalsIgnoreCase(tipoQuery)){
	    	logger.debug(String.format("Remit getQueryToExecute REMIT_PARAM_TYPE_EXTERNAS equals %s",tipoQuery));
	    	return historico?resourceBundle.getString(REMIT_QUERY_EXTERNAS_HISTORICO):resourceBundle.getString(REMIT_QUERY_EXTERNAS);
	    	
	    }else if (REMIT_PARAM_TYPE_INTERNAS.equalsIgnoreCase(tipoQuery)){
	    	logger.debug(String.format("Remit getQueryToExecute REMIT_PARAM_TYPE_INTERNAS equals %s",tipoQuery));
	    	return historico?resourceBundle.getString(REMIT_QUERY_INTERNAS_HISTORICO):resourceBundle.getString(REMIT_QUERY_INTERNAS);
	    }
		
		logger.debug("Remit getQueryToExecute return ''");
		return "";
	
		
	}
	
}
