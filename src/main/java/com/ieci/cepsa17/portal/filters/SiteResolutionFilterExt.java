package com.ieci.cepsa17.portal.filters;

import com.epicentric.authorization.PrincipalSet;
import com.epicentric.common.website.CookieUtils;
import com.epicentric.common.website.EntityUtils;
import com.epicentric.common.website.I18nUtils;
import com.epicentric.common.website.Localizer;
import com.epicentric.common.website.RequestUtils;
import com.epicentric.common.website.SessionInfo;
import com.epicentric.common.website.SiteUtils;
import com.epicentric.i18n.locale.LocaleUtils;
import com.epicentric.metastore.MetaStoreFolder;
import com.epicentric.site.Site;
import com.epicentric.site.SiteException;
import com.epicentric.site.SiteManager;
import com.epicentric.user.User;
import com.vignette.portal.hpd.CachingHelper;
import com.vignette.portal.hpd.HPDRedirectHelper;
import com.vignette.portal.hpd.UserClassManager;
import com.vignette.portal.log.LogWrapper;
import com.vignette.portal.util.StringUtils;
import com.vignette.portal.website.enduser.PortalRequest;
import com.vignette.portal.website.enduser.PortalURI;
import com.vignette.portal.website.enduser.internal.ControllerUtils;
import com.vignette.portal.website.enduser.internal.PortalContextImpl;
import java.io.IOException;
import java.util.Collection;
import java.util.Locale;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SiteResolutionFilterExt implements Filter
{
	  private FilterConfig filterConfig = null;
	  public static final String SELECTOR_NAME = "lang_choosen";
	  protected static final LogWrapper LOG = new LogWrapper(SiteResolutionFilterExt.class, "com.ieci.cepsa17.portal.filters");

	  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
	    throws IOException, ServletException
	  {
	
		LOG.debug("SiteResolutionFilterExt - INIT");
		  
	    PortalContextImpl portalContext = (PortalContextImpl)request.getAttribute("portalContext");

	    PrincipalSet ps = portalContext.getCurrentUserPrincipals();

	    HttpServletRequest httpRequest = (HttpServletRequest)request;
	    HttpServletResponse httpResponse = (HttpServletResponse)response;

	    String pathInfo = httpRequest.getPathInfo();
	    String servletPath = httpRequest.getServletPath();
	    String siteDNS = "";
	    Site site = null;
	    SiteManager siteMgr = SiteManager.getInstance();
	    boolean redirect = false;

	    if (servletPath.equals("/site")) {
	      siteDNS = getSiteDNS(pathInfo);
	      if (!StringUtils.isEmpty(siteDNS))
	        try {
	          site = siteMgr.getSiteFromDNSName(siteDNS);
	        }
	        catch (SiteException se)
	        {
	        }
	      if (site == null) {
	        site = SiteUtils.resolveSite((HttpServletRequest)request);
	      }

	    }

	    HttpSession session = httpRequest.getSession();

	    if (site != null) {
	      portalContext.setSite(site);
	    }

	    SessionInfo sessionInfo = (SessionInfo)session.getAttribute("epicentric_session_info");
	    sessionInfo.setSite(site);
	    String pageContent = (String)request.getAttribute("epi-content");
	    User user = sessionInfo.getUser();
	    if ((user.isGuestUser()) && (site != null))
	    {
	    	LOG.debug("SiteResolutionFilterExt - usuario=guest y site!=null");
	    	
	      if (sessionInfo.isSiteChanged()) {
	    	LOG.debug("SiteResolutionFilterExt - isSiteChanged true");
	    	LOG.debug("SiteResolutionFilterExt - removeAttribute(epi-locale)");
	        session.removeAttribute("epi-locale");
	      }
	      
	      String language = RequestUtils.getParameter(httpRequest, "lang_choosen");
	      LOG.debug("SiteResolutionFilterExt get parameter lang_choosen: "+ language);
	      
	      if ((language != null) && (!"CHOOSE_LANGUAGE".equals(pageContent))){
	        LocaleUtils.setGuestPreferredLocale(httpRequest, httpResponse, language, site.getUID());
	      }
	      
	      if ((site != null) && (!"CHOOSE_LANGUAGE".equals(pageContent)) && (!"LOGOUT".equals(pageContent))) {
	        LocaleUtils.setSiteRegisteredLocales(httpRequest, site.getUID());
	      }

	      if (CachingHelper.isHPDEnabled()) {	    	  
	    	LOG.debug("SiteResolutionFilterExt isHPDEnabled true");
	        UserClassManager ucm = UserClassManager.getInstance();
	        if (ucm.setUserPreferredLanguageCookie(portalContext, false)){
	          redirect = true;
	        }
	      }
	    } else if (site != null) {
	    	
	    	LOG.debug("SiteResolutionFilterExt - Usuario LOGADO");
	    	Locale guestSelectedLocale = (Locale)session.getAttribute("epi-locale");
	    	if (guestSelectedLocale!=null){
	    		LOG.debug("SiteResolutionFilterExt attribute epi-locale (guestSelectedLocale): " + guestSelectedLocale.toString());
	    	}else{
	    		LOG.debug("SiteResolutionFilterExt attribute epi-locale (guestSelectedLocale): null");
	    	}
	    	
	    	LOG.debug("SiteResolutionFilterExt I18nUtils.getSiteRegisteredLocales(site.getUID()).contains(guestSelectedLocale)): " + I18nUtils.getSiteRegisteredLocales(site.getUID()).contains(guestSelectedLocale));
	    	if ((guestSelectedLocale != null) && (I18nUtils.getSiteRegisteredLocales(site.getUID()).contains(guestSelectedLocale)) && ((pageContent == null) || (pageContent.equals("PAGE")))) {
		        Locale profileLocale = I18nUtils.getLocalizer(session, httpRequest).getLocale();
		        if (profileLocale!=null){
		    		LOG.debug("SiteResolutionFilterExt profileLocale: " + profileLocale.toString());
		    	}else{
		    		LOG.debug("SiteResolutionFilterExt profileLocale: null");
		    	}
		     	
		        /* FIX UPDATE_PROFILE 11.06.2018 - pt_BR
		        String descripency_str = EntityUtils.getUserSystemFolder(user, true).getProperty("allow_discrepancy");
		        if ((!guestSelectedLocale.equals(profileLocale)) && (!guestSelectedLocale.toString().equals(descripency_str))) {
		        	LOG.debug("SiteResolutionFilterExt !guestSelectedLocale.equals(profileLocale): " + (!guestSelectedLocale.equals(profileLocale)));
		        	LOG.debug("SiteResolutionFilterExt !guestSelectedLocale.toString().equals(descripency_str): " + (!guestSelectedLocale.toString().equals(descripency_str)));
		          PortalURI redirectURI = portalContext.createDisplayURI("UPDATE_PROFILE");
		          ControllerUtils.redirect(portalContext, httpResponse, redirectURI);
		        }
		        */
		        LOG.debug("SiteResolutionFilterExt removeAttribute(epi-locale)");
		        session.removeAttribute("epi-locale");
	      }
	    }
	    if (sessionInfo.isSiteChanged())
	    {
	    	
	     LOG.debug("SiteResolutionFilterExt set attribute epicentric_session_info");	    
	      session.setAttribute("epicentric_session_info", sessionInfo);
	    }

	    if (CachingHelper.isHPDEnabled()) {	    	
	      UserClassManager ucm = UserClassManager.getInstance();
	      boolean cookieSet = ucm.setUserClassCookie(portalContext, portalContext.getCurrentUserPrincipals(), false);
	      Cookie userClassCookie = CookieUtils.getCookie(httpRequest, "USER_CLASS");

	      if ((userClassCookie == null) && (cookieSet)) {
	        redirect = true;
	      }

	    }

	    PortalRequest portalRequest = portalContext.getPortalRequest();
	    String templateFriendlyID = portalRequest.getTemplateFriendlyId();

	    boolean isRAW = "RAW".equals(templateFriendlyID);
	    boolean isPartialPageRequest = portalContext.isPartialPageRequest();

	    if ((request.getAttribute("epi_requested_location") == null) && (!isPartialPageRequest) && (!isRAW))
	    {
	    	
	      StringBuffer requestedURI = new StringBuffer(httpRequest.getRequestURI());
	      if (httpRequest.getQueryString() != null) {
	        requestedURI.append('?').append(httpRequest.getQueryString());
	      }
	      request.setAttribute("epi_requested_location", requestedURI.toString());
	    }

	    boolean isLoginPage = "LOGIN".equals(templateFriendlyID);
	    boolean isSSO = "TRUE".equals(session.getAttribute("epi:single-sign-on"));

	    String cookieName = null;
	    if ((site != null) && ((session.isNew()) || ((isSSO) && (isLoginPage)))) {
	      cookieName = "VignettePortal-NavTreeState-" + site.getDNSName();
	      if (CookieUtils.getCookie(httpRequest, cookieName) != null) {
	        CookieUtils.expireCookie(httpRequest, httpResponse, cookieName, "/");
	      }
	      redirect = true;
	    }

	    if ((CachingHelper.isHPDEnabled()) && (!CachingHelper.isHPDRedirectDisabled()) && (cookiesEnabledValidation(httpRequest, httpResponse))) {
	      return;
	    }

	    if ((CachingHelper.isHPDEnabled()) && (redirect) && (!CachingHelper.isHPDRedirectDisabled())) {
	      HPDRedirectHelper redirectObj = HPDRedirectHelper.forRequest(httpRequest);
	      String redirectPath = redirectObj.getUri();
	      if (redirectObj.getQueryString() != null) {
	        redirectPath = redirectPath + "?" + redirectObj.getQueryString();
	      }
	      httpResponse.sendRedirect(redirectPath);
	      return;
	    }

	    chain.doFilter(request, response);
	  }

	  private boolean cookiesEnabledValidation(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
	    throws ServletException, IOException
	  {
	    boolean headingForErrorPage = httpRequest.getAttribute("error") != null;
	    boolean alreadyTriedRedirect = httpRequest.getParameter("redirected") != null;
	    boolean redirect = httpRequest.getSession().isNew();
	    if ((redirect) && (!headingForErrorPage)) {
	      if (alreadyTriedRedirect)
	      {
	        httpRequest.setAttribute("error", Boolean.TRUE);
	        httpRequest.getRequestDispatcher("/cookies_error_display.jsp").forward(httpRequest, httpResponse);
	        return true;
	      }

	      HPDRedirectHelper redirectObj = HPDRedirectHelper.forRequest(httpRequest);
	      String redirectPath = redirectObj.getUri();
	      redirectPath = redirectPath + "?redirected=true";
	      if (redirectObj.getQueryString() != null) {
	        redirectPath = redirectPath + "&" + redirectObj.getQueryString();
	      }
	      httpResponse.sendRedirect(redirectPath);
	      return true;
	    }
	    if ((alreadyTriedRedirect) && (!redirect) && (!headingForErrorPage))
	    {
	      HPDRedirectHelper redirectObj = HPDRedirectHelper.forRequest(httpRequest);
	      String redirectPath = redirectObj.getUri();
	      String qs = redirectObj.getQueryString();
	      if (qs != null) {
	        qs = qs.replaceAll("redirected=true&?", "");
	        if (qs.trim().length() > 0) {
	          redirectPath = redirectPath + "?" + qs;
	        }
	      }
	      httpResponse.sendRedirect(redirectPath);
	      return true;
	    }
	    return false;
	  }

	  private String getSiteDNS(String pathInfo) {
	    if (StringUtils.isEmpty(pathInfo)) return null;

	    String siteDNS = null;

	    if (pathInfo.startsWith("/")) {
	      pathInfo = pathInfo.substring(1);
	    }

	    int index = pathInfo.indexOf("/");
	    if (index == -1) {
	      siteDNS = pathInfo;
	    }
	    else
	    {
	      siteDNS = pathInfo.substring(0, index);
	    }

	    return siteDNS;
	  }

	  public void destroy() {
	  }

	  public void init(FilterConfig filterConfig) {
	    this.filterConfig = filterConfig;
	  }
	}