package com.ieci.cepsa17.wem.constants;

public class BuscadorFichasSeguridadConstants {

	/**
	 * Inputs Name Form
	 */
	public static final String FORM_NOMBRE_PRODUCTO = "nombreProducto";
	public static final String FORM_NOMBRE_COMERCIAL = "nombreComercial";
	public static final String FORM_COD_CEPSA = "codCEPSA";
	public static final String FORM_COD_PRODUCTO = "codProducto";
	public static final String FORM_CAS = "cas";
	public static final String FORM_COMPANIA = "compania";
	public static final String FORM_IDIOMA = "idioma";
	public static final String FORM_TIPO_FICHA = "tipoFicha";
	public static final String FORM_FECHA_VALIDACION_D = "fechaD";
	public static final String FORM_FECHA_VALIDACION_H = "fechaD_H";

	public static final String COMPANIA_CEPSA = "CEPSA";
	public static final String COMPANIA_CEPSA_COMERCIAL_PETROLEO = "CEPSA COMERCIAL PETROLEO";
	public static final String COMPANIA_CEPSA_QUIMICA = "CEPSA QUIMICA";
	public static final String COMPANIA_CEPSA_UK = "CEPSA UK";
	public static final String COMPANIA_CEPSA_IT = "CEPSA IT";
	public static final String COMPANIA_CGL = "CGL";
	public static final String COMPANIA_CLSA = "CLSA";
	public static final String COMPANIA_PROAS = "PROAS";

	public static final String IDIOMA_KEY_ALEMAN = "de-DE";
	public static final String IDIOMA_VALUE_ALEMAN = "Alemán (DE)";
	public static final String IDIOMA_KEY_ESPANOL = "es-ES";
	public static final String IDIOMA_VALUE_ESPANOL = "Español (ES)";
	public static final String IDIOMA_KEY_FRANCES = "fr-FR,fr-CA";
	public static final String IDIOMA_VALUE_FRANCES = "Francés (FR)";
	public static final String IDIOMA_KEY_INGLES = "en-US,en-GB";
	public static final String IDIOMA_VALUE_INGLES = "Inglés (EN)";
	public static final String IDIOMA_KEY_ITALIANO = "it-IT";
	public static final String IDIOMA_VALUE_ITALIANO = "Italiano (IT)";
	public static final String IDIOMA_KEY_PORTUGUES = "pt-BR,pt-PT";
	public static final String IDIOMA_VALUE_PORTUGUES = "Portugués (PT)";
	public static final String IDIOMA_KEY_RESTO = "zh-CHT,nl-NL,pl-PL,el-GR";
	public static final String IDIOMA_VALUE_RESTO = "Resto";

	public static final String TIPO_FICHA_FDS = "FDS";
	public static final String USES_PROXY = "1";

	public static final String HTTP_PROXY_HOST = "http.proxyHost";
	public static final String HTTP_PROXY_PORT = "http.proxyPort";
	public static final String HTTP_PROXY_USER = "http.proxyUser";
	public static final String HTTP_PROXY_PASS = "http.proxyPassword";

	public static final String END_POINT = "end.point";
	public static final String USER_NAME = "user.name";
	public static final String PASS = "password";
	public static final String PROXY = "proxy";
	public static final String PROXY_HOST = "proxy.host";
	public static final String PROXY_PORT = "proxy.port";
	public static final String PROXY_USER = "proxy.user";
	public static final String PROXY_PASS = "proxy.password";

	public static final String ERROR = "error";

	// 12 h de TTL
	public static final int TTL = 43200000;

	private BuscadorFichasSeguridadConstants() {
		super();
	}

}