package com.ieci.cepsa17.wem.constants;

public class BuscadorHomologacionesConstants {

	public static final String CT_HOMOLOGACION = "CT_HOMOLOGACION";
	public static final String CT_PRODUCTO = "CT_PRODUCTO";
	public static final String CT_CATEGORIA = "CT_CATEGORIA";

	public static final String HOMOLOGACION_ALIAS = "ALIAS";
	public static final String HOMOLOGACION_CODIGO_HOM = "COD_HOM";
	public static final String HOMOLOGACION_PRODUCTO_ID = "PROD";
	public static final String HOMOLOGACION_HOMOLOGADOR = "HOMOLOGADOR";

	public static final String PRODUCTO_CODIGO = "CODIGO_CEPSA";
	public static final String PRODUCTO_NOMBRE = "NOMBRE";

	public static final String CATEGORIA_ALIAS = "ALIAS";

	private BuscadorHomologacionesConstants() {
		super();
	}

}