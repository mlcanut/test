package com.ieci.cepsa17.wem.constants;

public class BuscadorGuiaLubricacionConstants {

	/**
	 * Inputs Name Form
	 */

	// Código ISO3 de país (Español e Inglés) para las llamadas a los métodos de obtener Categorías, Marcas, Modelos y Tipos
	public static String LANGUAGE_ISO3_SPA = "SPA";
	public static String LANGUAGE_ISO3_ENG = "ENG";
	public static String LANGUAGE_ISO3_POR = "POR";

	public static final String USES_PROXY = "1";
	
	public static final String WS_BUNDLE = "com.cepsa.portal.guialubricacion.ws.config";

	public static final String JBOSS_SERVER_CONFIG_DIR = "jboss.server.config.dir";
	public static final String FILE = "file:";
	public static final String BUSCADORES_UTILS_PROPERTIES = "buscadores_utils.properties";

	public static final String HTTP_PROXY_HOST = "http.proxyHost";
	public static final String HTTP_PROXY_PORT = "http.proxyPort";
	public static final String HTTP_PROXY_USER = "http.proxyUser";
	public static final String HTTP_PROXY_PASS = "http.proxyPassword";
	
	// Tipos de búsqueda
	public static final int TIPO_BUSQUEDA_CATEGORIAS 				= 1;
	public static final int TIPO_BUSQUEDA_MARCAS 					= 2;
	public static final int TIPO_BUSQUEDA_MODELOS 					= 3;
	public static final int TIPO_BUSQUEDA_TIPOS 					= 4;
	public static final int TIPO_BUSQUEDA_IMAGEN_MARCA 				= 5;
	public static final int TIPO_BUSQUEDA_IMAGEN_MODELO 			= 6;
	public static final int TIPO_BUSQUEDA_IMAGEN_TIPO 				= 7;
	public static final int TIPO_BUSQUEDA_TIPOS_POR_MARCA_MODELO	= 8;
	public static final int TIPO_BUSQUEDA_TIPOS_POR_MATRICULA		= 9;
	public static final int TIPO_BUSQUEDA_MARCAS_MAS_BUSCADAS		= 10;
	public static final int TIPO_BUSQUEDA_TIPOS_PREDICTIVO			= 11;
	public static final int TIPO_BUSQUEDA_RECOMENDACION				= 12;
	
	public static final String END_POINT 	= "end.point";
	public static final String USER_NAME	= "user.name";
	public static final String PASS 		= "password";
	public static final String PROXY 		= "proxy";
	public static final String PROXY_HOST 	= "proxy.host";
	public static final String PROXY_PORT 	= "proxy.port";
	public static final String PROXY_USER 	= "proxy.user";
	public static final String PROXY_PASS 	= "proxy.password";
	
	public static final String TTL = "ttl";
	
	public static final String TIME_OUT_OLYSLAGER = "time_out_olyslager";
	
	// Valores del fichero config.properties
	public static final String MOCK_MARCAS_POPULARES = "mock.marcas.populares";
	public static final String MAKES_POPULAR = "makes.popular";
	
	// Valor en el fichero properties de la cadena de marcas populares para aquellas categorías que no dispongan de ella
	public static final String NO_MAKES_POPULAR = "0";
	
	// indicador para buscar lista de las marcas más populares sin Categoría (Lista de la página inicial)
	public static final String NO_CATEGORIA = "0";
	
	// Indicador de que hay mock marcas más populares
	public static final String HAY_MOCK_MARCAS_POPULARES = "1";
	
	// Categorías de vehículos que no se tienen en cuenta para la guía de lubricación 
	public static final String CATEGORIA_MARINO 		= "31";
	public static final String CATEGORIA_VEHICULOS_RUS 	= "33";
	public static final String CATEGORIA_VEHICULOS_TUR 	= "34";
	
	// Indicador para limpiar la caché
	public static final String FLUSH_CACHE = "flush.cache";
	public static final String HAY_FLUSH_CACHE = "1";
	
	public static final String CT_PRODUCTO 				= "CT_PRODUCTO";
	public static final String CT_PRODUCTO_R_IMAGEN		= "CT_PRODUCTO_R_IMAGEN";
	public static final String PRODUCTO_CODIGO_CEPSA	= "CODIGO_CEPSA";
	public static final String PRODUCTO_R_IMAGEN_ORDEN	= "ORDEN";
	public static final String PRODUCTO_IDPRODUCTO		= "IDPRODUCTO";	
	
	// Número máximo de caracteres a mostrar para cada categoría (NO se usa)
	public static final int MAX_CARACTERES_CATEGORIA = 35;

//	public static final String PARAMETRIZACION_ANYO_CACHE  	= "parametro.anyo.cache";	
//	public static final String HAY_PARAMETRIZACION_ANYO 	= "1";
//	public static final String PARAMETRO_ANYO  				= "parametro.anyo";	
			
	// 12 h de TTL
	//public static final int TTL = 43200000;
	
	// 100 h
	//public static final int TTL2 = 360000000;

	private BuscadorGuiaLubricacionConstants() {
		super();
	}

}