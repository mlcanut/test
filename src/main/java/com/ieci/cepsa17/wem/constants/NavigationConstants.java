package com.ieci.cepsa17.wem.constants;

public class NavigationConstants {

	public static final short PREDECESSOR_LEVEL_TO_SHOW = 3;
	public static final short PREDECESSOR_MINIMUM_LEVELS_UP = 0;

	public static final short SET_CHANNEL_INFO_LEVEL_TO_SHOW = 2;
	public static final short SET_CHANNEL_INFO_MINIMUM_LEVELS_UP = 0;

	public static final int PAGE_SCOPE = 1;
	public static final int REQUEST_SCOPE = 2;
	public static final int SESSION_SCOPE = 3;

	public static final String SEGMENT_ID = "segmentId";
	public static final String SEGMENT = "segment";
	public static final String SEGMENT_I18N = "segmentI18n";
	public static final String SEGMENT_FURL = "segmentFurl";

	public static final String LANGUAGE_ES = "es";
	public static final String COUNTRY_ES = "ES";

	public static final String LOCALE = "locale";

	public static final String VCMID_SEGMENTO_PARTICULAR = "98a6363ffdb1d510VgnVCM1000005ba5e40aRCRD";
	public static final String VCMID_SEGMENTO_EMPRESAS = "56c6363ffdb1d510VgnVCM1000005ba5e40aRCRD";
	public static final String VCMID_SEGMENTO_TRANSPORTISTA = "04e6363ffdb1d510VgnVCM1000005ba5e40aRCRD";
	public static final String VCMID_SEGMENTO_DISTRIBUIDOR = "a107363ffdb1d510VgnVCM1000005ba5e40aRCRD";

	public static final String SEGMENTO_PARTICULAR = "particular";

	private NavigationConstants() {
		super();
	}

}