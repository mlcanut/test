package com.ieci.cepsa17.wem.comun.util.buscador.comparador;

import java.util.Comparator;

import com.ieci.cepsa17.wem.comun.util.buscador.bean.Certificado;

public class ComparadorTipoCertificadoCertificado implements Comparator<Certificado> {

	@Override
	public int compare(Certificado o1, Certificado o2) {

		return o1.getTipoCertificado().compareTo(o2.getTipoCertificado());

	}

}