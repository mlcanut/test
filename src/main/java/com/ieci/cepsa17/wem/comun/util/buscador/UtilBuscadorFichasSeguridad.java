package com.ieci.cepsa17.wem.comun.util.buscador;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.tempuri.EFicha;

import com.ieci.cepsa17.comercial.wem.fds.service.FichasSeguridadServices;
import com.ieci.cepsa17.comercial.wem.fds.view.bean.FormBean;
import com.ieci.cepsa17.wem.constants.BuscadorFichasSeguridadConstants;

public class UtilBuscadorFichasSeguridad {

	protected static final String WS_BUNDLE = "com.cepsa.portal.fds.ws.config";

	protected static Logger logger = Logger.getLogger(UtilBuscadorFichasSeguridad.class);

	private UtilBuscadorFichasSeguridad() {
		super();
	}

	public static List<String> getCompanias() {

		logger.debug("UtilBuscadorFichasSeguridad -- getCompanias");

		LinkedList<String> resultsList = new LinkedList<>();
		resultsList.add(BuscadorFichasSeguridadConstants.COMPANIA_CEPSA);
		resultsList.add(BuscadorFichasSeguridadConstants.COMPANIA_CEPSA_COMERCIAL_PETROLEO);
		resultsList.add(BuscadorFichasSeguridadConstants.COMPANIA_CEPSA_QUIMICA);
		resultsList.add(BuscadorFichasSeguridadConstants.COMPANIA_CEPSA_UK);
		resultsList.add(BuscadorFichasSeguridadConstants.COMPANIA_CEPSA_IT);
		resultsList.add(BuscadorFichasSeguridadConstants.COMPANIA_CGL);
		resultsList.add(BuscadorFichasSeguridadConstants.COMPANIA_CLSA);
		resultsList.add(BuscadorFichasSeguridadConstants.COMPANIA_PROAS);

		return resultsList;

	}

	@SuppressWarnings("rawtypes")
	public static Map getIdiomas() {

		logger.debug("UtilBuscadorFichasSeguridad -- getIdiomas");

		LinkedHashMap<String, String> resultsList = new LinkedHashMap<>();
		resultsList.put(BuscadorFichasSeguridadConstants.IDIOMA_KEY_ALEMAN,
				BuscadorFichasSeguridadConstants.IDIOMA_VALUE_ALEMAN);
		resultsList.put(BuscadorFichasSeguridadConstants.IDIOMA_KEY_ESPANOL,
				BuscadorFichasSeguridadConstants.IDIOMA_VALUE_ESPANOL);
		resultsList.put(BuscadorFichasSeguridadConstants.IDIOMA_KEY_FRANCES,
				BuscadorFichasSeguridadConstants.IDIOMA_VALUE_FRANCES);
		resultsList.put(BuscadorFichasSeguridadConstants.IDIOMA_KEY_INGLES,
				BuscadorFichasSeguridadConstants.IDIOMA_VALUE_INGLES);
		resultsList.put(BuscadorFichasSeguridadConstants.IDIOMA_KEY_ITALIANO,
				BuscadorFichasSeguridadConstants.IDIOMA_VALUE_ITALIANO);
		resultsList.put(BuscadorFichasSeguridadConstants.IDIOMA_KEY_PORTUGUES,
				BuscadorFichasSeguridadConstants.IDIOMA_VALUE_PORTUGUES);
		resultsList.put(BuscadorFichasSeguridadConstants.IDIOMA_KEY_RESTO,
				BuscadorFichasSeguridadConstants.IDIOMA_VALUE_RESTO);

		return resultsList;

	}

	public static String getFichasDeSeguridad(String nombreProducto, String codigoCepsa, String cas, String compania,
			String idioma) {

		logger.debug("UtilBuscadorFichasSeguridad ---- getFichasDeSeguridad");

		String fichas = "";

		try {

			FormBean busqueda = FormBean.createInstanceFDS(nombreProducto, codigoCepsa, cas, compania, idioma);

			logger.debug("UtilBuscadorFichasSeguridad -- Busqueda: " + busqueda.getNombreProducto() + " - "
					+ busqueda.getCodCepsa() + " - " + busqueda.getCas() + " - " + busqueda.getCompania() + " - "
					+ busqueda.getIdioma());

			FichasSeguridadServices fichasSeguridadServices = getConnection();
			List<EFicha> resultado = fichasSeguridadServices.obtenerFichas(busqueda);

			JSONArray jsonArray = new JSONArray();
			for (EFicha eFicha : resultado) {
				JSONObject fichaJson = new JSONObject();
				fichaJson.put("idficha", safeFormatJsonValue(eFicha.getIdFicha(), ""));
				fichaJson.put("proddescnombre",
						safeFormatJsonValue(eFicha.getProducto().getDescripcion().getNombre(), ""));
				fichaJson.put("prodnombrecomercial",
						safeFormatJsonValue(eFicha.getProducto().getDescripcion().getNombreComercial(), ""));

				String strFechaVal = "";
				XMLGregorianCalendar fechaVal = eFicha.getHistoria().getFechaValidacion();
				if (fechaVal != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					strFechaVal = formatter.format(fechaVal.toGregorianCalendar().getTime());
				}
				fichaJson.put("fechavalidacion", safeFormatJsonValue(strFechaVal, ""));

				fichaJson.put("prodcodsap", safeFormatJsonValue(eFicha.getProducto().getCodSAP(), ""));
				fichaJson.put("prodcodprod", safeFormatJsonValue(eFicha.getProducto().getCodProd(), ""));
				fichaJson.put("cas", safeFormatJsonValue(eFicha.getProducto().getCAS(), ""));
				fichaJson.put("companiaorigen", safeFormatJsonValue(eFicha.getCompaniaOrigen().getNombre(), ""));
				fichaJson.put("idioma", safeFormatJsonValue(eFicha.getIdioma().getIdioma(), ""));

				jsonArray.put(fichaJson);
			}

			if (resultado != null) {
				logger.debug("UtilBuscadorFichasSeguridad -- " + resultado.size() + " fichas de seguridad encontradas");
			} else {
				logger.debug("UtilBuscadorFichasSeguridad -- No se han encontrado fichas de seguridad");
			}

			fichas = jsonArray.toString();

		} catch (Exception e) {
			logger.error(
					"UtilBuscadorFichasSeguridad -- Se ha producido un error en la busqueda de fichas de seguridad: "
							+ e.getMessage(),
					e);
			fichas = BuscadorFichasSeguridadConstants.ERROR;
		}

		return fichas;

	}

	public static String getContenidoFicha(String id) {

		logger.debug("UtilBuscadorFichasSeguridad ---- getContenidoFicha");

		String base64Encoded = "";

		try {

			FichasSeguridadServices fichasSeguridadServices = getConnection();
			byte[] resultados = fichasSeguridadServices.obtContenidoFichaPorID(id);
			Base64 base64 = new Base64();
			base64Encoded = base64.encodeAsString(resultados);

			logger.debug("UtilBuscadorFichasSeguridad -- Contenido de la ficha: " + id);

		} catch (Exception e) {
			logger.error(
					"UtilBuscadorFichasSeguridad -- Se ha producido un error en la busqueda del contenido de la ficha "
							+ id + ": " + e.getMessage(),
					e);
		}

		return base64Encoded;

	}

	protected static String safeFormatJsonValue(String data, String defaultValue) {

		return data == null || "".equals(data) ? defaultValue : data;

	}

	private static FichasSeguridadServices getConnection() throws Exception {

		String endpoint = "";
		String usuario = "";
		String pass = "";

		try {
			ResourceBundle resourceBundle = ResourceBundle.getBundle(WS_BUNDLE);

			endpoint = resourceBundle.getString(BuscadorFichasSeguridadConstants.END_POINT);
			usuario = resourceBundle.getString(BuscadorFichasSeguridadConstants.USER_NAME);
			pass = resourceBundle.getString(BuscadorFichasSeguridadConstants.PASS);

			String usesProxy = resourceBundle.getString(BuscadorFichasSeguridadConstants.PROXY);

			if (BuscadorFichasSeguridadConstants.USES_PROXY.equals(usesProxy)) {

				System.setProperty(BuscadorFichasSeguridadConstants.HTTP_PROXY_HOST,
						resourceBundle.getString(BuscadorFichasSeguridadConstants.PROXY_HOST));
				System.setProperty(BuscadorFichasSeguridadConstants.HTTP_PROXY_PORT,
						resourceBundle.getString(BuscadorFichasSeguridadConstants.PROXY_PORT));
				System.setProperty(BuscadorFichasSeguridadConstants.HTTP_PROXY_USER,
						resourceBundle.getString(BuscadorFichasSeguridadConstants.PROXY_USER));
				System.setProperty(BuscadorFichasSeguridadConstants.HTTP_PROXY_PASS,
						resourceBundle.getString(BuscadorFichasSeguridadConstants.PROXY_PASS));

			}

		} catch (final Exception e) {
			throw new Exception("Se ha producido un error obteniendo la conexion " + WS_BUNDLE, e);
		}

		return FichasSeguridadServices.getInstance(endpoint, usuario, pass);

	}

}