package com.ieci.cepsa17.wem.comun.util.
buscador;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.ieci.cepsa17.wem.comun.util.CIUtil;
import com.ieci.cepsa17.wem.comun.util.buscador.bean.Certificado;
import com.ieci.cepsa17.wem.comun.util.buscador.comparador.ComparadorEmpresaCertificado;
import com.ieci.cepsa17.wem.comun.util.buscador.comparador.ComparadorFechaCaducidadCertificado;
import com.ieci.cepsa17.wem.comun.util.buscador.comparador.ComparadorFechaCertificacionCertificado;
import com.ieci.cepsa17.wem.comun.util.buscador.comparador.ComparadorNormaCertificado;
import com.ieci.cepsa17.wem.comun.util.buscador.comparador.ComparadorOrganoCertificadorCertificado;
import com.ieci.cepsa17.wem.comun.util.buscador.comparador.ComparadorTipoCertificadoCertificado;
import com.ieci.cepsa17.wem.comun.util.cms.CMSUtils;
import com.ieci.cepsa17.wem.comun.util.cms.RelatorUtils;
import com.ieci.cepsa17.wem.comun.util.comparators.EmpresaContentInstanceComparatorByName;
import com.vignette.as.client.api.bean.ManagedObjectBean;
import com.vignette.as.client.common.ContentInstanceWhereClause;
import com.vignette.as.client.common.ref.AsLocaleRef;
import com.vignette.as.client.common.ref.ChannelRef;
import com.vignette.as.client.common.ref.ManagedObjectVCMRef;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.exception.ValidationException;
import com.vignette.as.client.javabean.Channel;
import com.vignette.as.client.javabean.ContentInstance;
import com.vignette.as.client.javabean.StaticFile;
import com.vignette.util.ObjectQueryOp;


public class UtilBuscadorCertificados {

	protected static Logger logger = Logger.getLogger(UtilBuscadorCertificados.class);
	
	/**
	 * DEvuelve un json con los tipos de certificado existentes en el sistema
	 * @param locale
	 * @return
	 * @throws ApplicationException
	 * @throws ValidationException
	 */
	public static String getJSonTiposCertificado(Locale locale) throws ApplicationException, ValidationException {
		
		List<ManagedObjectBean> centrosNegocios = getTiposCertificado(locale);
		//TODO return JSON
		//"{ \"name\":\"zzzzzzzzzz\", \"vcmid\":\"xxxxxxxxxxxx\" }";
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		Iterator<ManagedObjectBean> it = centrosNegocios.iterator();
		while (it.hasNext()){
			if (sb.length()!=1){
				sb.append(",");
			}
			sb.append("{");
			ManagedObjectBean current = it.next();
			sb.append("\"vcmid\":\"");
			sb.append(current.getSystem().getId());
			sb.append("\",");
			sb.append("\"name\":\"");
			sb.append(current.getSystem().getName());
			sb.append("\"");
			sb.append("}");
		}
		sb.append("]");
		logger.debug("getJSonTiposCertificado result: " + sb.toString());
		
		return sb.toString();
		
		
				
	}
	
	/**
	 * DEvuelve los tipos de certificado exustentes en el sistema
	 * @param locale
	 * @return
	 * @throws ApplicationException
	 * @throws ValidationException
	 */
	public static List<ManagedObjectBean> getTiposCertificado(Locale locale) throws ApplicationException, ValidationException{
	
		logger.debug("getTiposCertificado locale: " + locale.toString());
		
		//1.- Buscamos Certificados existentes
		ArrayList<ContentInstanceWhereClause> query = new ArrayList<ContentInstanceWhereClause>();
		
		ContentInstanceWhereClause clause =  new ContentInstanceWhereClause();
		clause.checkAttribute("TIPO_CAT", ObjectQueryOp.EQUAL, "tCert");
		query.add(clause);
		
		ContentInstanceWhereClause clauseLocale =  new ContentInstanceWhereClause();
		clauseLocale.checkLocale(ObjectQueryOp.EQUAL, new AsLocaleRef(locale));
		query.add(clauseLocale);
		

		// Ejecuta la query
		ContentInstance[] resultado = CMSUtils.getContentInstancesByQuery ("CT_CATEGORIA", query, "ALIAS");
		logger.debug("getTiposCertificado categorias recuperadas "+resultado.length);
		
		List<ManagedObjectBean> results = new ArrayList<ManagedObjectBean>();
		
		for (ContentInstance entry : resultado)
		{
			logger.debug("Iterate id: " + entry.getName());
			results.add(CIUtil.makeBean((ContentInstance)entry));
		    
		}
		logger.debug("getTiposCertificado FIN return size: " + results.size());
		return results;
		
		
		
	}
	
	/**
	 * 
	 * @param tipoCertificado
	 * @param locale
	 * @return
	 * @throws ApplicationException
	 * @throws ValidationException 
	 */
	public static String getJSonCentrosNegociosByTipoCertificado(String tipoCertificado, Locale locale) throws ApplicationException, ValidationException{
		
		logger.debug("getJSonCentrosNegociosByTipoCertificado tipoCertificado: " + tipoCertificado + " locale: " + locale.toString());
		
		List<ManagedObjectBean> centrosNegocios = getCentrosNegociosByTipoCertificado(tipoCertificado, locale);
		//TODO return JSON
		//"{ \"name\":\"zzzzzzzzzz\", \"vcmid\":\"xxxxxxxxxxxx\" }";
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		Iterator<ManagedObjectBean> it = centrosNegocios.iterator();
		while (it.hasNext()){
			if (sb.length()!=1){
				sb.append(",");
			}
			sb.append("{");
			ManagedObjectBean current = it.next();
			sb.append("\"vcmid\":\"");
			sb.append(current.getSystem().getId());
			sb.append("\",");
			sb.append("\"name\":\"");
			sb.append(current.getSystem().getName());
			sb.append("\"");
			sb.append("}");
		}
		sb.append("]");
		logger.debug("getJSonCentrosNegociosByTipoCertificado result: " + sb.toString());
		
		return sb.toString();

	}
	
	/**
	 * 
	 * @param tipoCertificado
	 * @return
	 * @throws ApplicationException
	 * @throws ValidationException 
	 */
	public static List<ManagedObjectBean> getCentrosNegociosByTipoCertificado(String tipoCertificado, Locale locale) throws ApplicationException, ValidationException{
		
		logger.debug("getCentrosNegociosByTipoCertificado tipoCertificado: " + tipoCertificado);
		
		//1.- Buscamos Certificados del tipo que se ha seleccionado
		ArrayList<ContentInstanceWhereClause> query = new ArrayList<ContentInstanceWhereClause>();
		
		//ContentInstanceWhereClause clauseChannel =  new ContentInstanceWhereClause();
		//clauseChannel.checkChannel(ObjectQueryOp.EQUAL, new ChannelRef (currentChannel));
		//query.add(clauseChannel);
		ContentInstanceWhereClause clauseTipoCertificado =  new ContentInstanceWhereClause();
		clauseTipoCertificado.checkAttribute("IDCATEG", ObjectQueryOp.EQUAL, tipoCertificado);
		query.add(clauseTipoCertificado);
		
		ContentInstanceWhereClause clauseLocale =  new ContentInstanceWhereClause();
		clauseLocale.checkLocale(ObjectQueryOp.EQUAL, new AsLocaleRef(locale));
		query.add(clauseLocale);
		
		ContentInstance[] certificados = CMSUtils.getContentInstancesByQuery ("CT_CERTIFICADO", query, "ID");
		logger.debug("getCentrosNegociosByTipoCertificado busqueda inicial por tipo de certificado se han obtenido: " + certificados.length);
		
		//2.- Buscamos para los certificados sus centros negocios en su relación
		/**
		 * Metodo al que se le pasa una instancia de contenido, el nombre de un relator y el atributo del relator del que se quiere recuperar
		 * sus contenidos. Devuelve un array de ContentInstance con todos los contenidos relacionados
		 *	 
		 * @param ci ContentInstance  a partir de la cual obtendremos los atributos relacionados.
		 * @param relationName String, nombre del Relator 
		 * @param attributeRelation String, nombre del atributo del Relator que se desea recuperar
		 * @return Retorna un array de ContentInstance con todos las instancias de contenido relacionados que tenga ese ContentInstance
		 * */
		//public static ContentInstance[] getRelations(ContentInstance ci,String relationName, String attributeRelation)
		Map<String, ContentInstance> result = new HashMap<String, ContentInstance>();
		for(ContentInstance currentCi: certificados){
			logger.debug("getCentrosNegociosByTipoCertificado centros-negocios para el certificado: " + currentCi.getName());
			ContentInstance[] centrosNegocios = CMSUtils.getAllRelationsVCM(currentCi, "CT-CERTIFICADO-R-CENTRO", "CT_CERTIFICADO_R_CENTRO_CENTRO_NEGOCIO");
			logger.debug("getCentrosNegociosByTipoCertificado centros-negocios para el certificado encontrados: " + centrosNegocios.length);
			for (ContentInstance currentCentroNegocio:centrosNegocios){
				logger.debug("getCentrosNegociosByTipoCertificado centro-negocio: "+currentCentroNegocio.getName() +" ID: " + (String)currentCentroNegocio.getAttributeValue("ID"));
				if (!result.containsKey((String)currentCentroNegocio.getAttributeValue("ID"))){
					result.put((String)currentCentroNegocio.getAttributeValue("ID"), currentCentroNegocio);
					logger.debug("getCentrosNegociosByTipoCertificado inserto centro-negocio: " + currentCentroNegocio.getName());
										
				}
			}
		}
		List<ManagedObjectBean> results = new ArrayList<ManagedObjectBean>(result.size());
		for (Map.Entry<String, ContentInstance> entry : result.entrySet())
		{
			logger.debug("Iterate id: " + entry.getKey());
			results.add(CIUtil.makeBean((ContentInstance)entry.getValue()));
		    
		}
		logger.debug("getCentrosNegociosByTipoCertificado FIN return size: " + results.size());
		return results;
	}
	/**
	 * Devuelve todas las empresas que tienen algun tipo de certificado
	 * @return List<ManagedObjectBean>
	 */
	public static List<ManagedObjectBean> getEmpresasCertificadas(){
		if (logger.isDebugEnabled()) logger.debug("> getEmpresasCertificadas()");
		ArrayList<String> resultsList = new ArrayList<String>();
		ArrayList<ContentInstanceWhereClause> query = new ArrayList<ContentInstanceWhereClause>();
		/*
		ContentInstanceWhereClause clause =  new ContentInstanceWhereClause();
		clause.checkChannel(ObjectQueryOp.EQUAL, new ChannelRef ("9bc59af405444410VgnVCM100000fc071eac____"));
		query.add(clause);
		*/
		ContentInstance[] certificados = CMSUtils.getContentInstancesByQuery ("CERTIFICADO", query, "ID");
		if (logger.isDebugEnabled()) logger.debug("Recuperadas "+certificados.length + " empresas con certificado.");
		String temp = "";
		for (int i = 0; i < certificados.length; i++) {
			try {
				temp = (String) certificados[i].getAttributeValue("EMPRESA");
				if (temp != null && !resultsList.contains(temp)) {
					if (logger.isDebugEnabled()) logger.debug("Nombre de la empresa :: " +temp);
					resultsList.add(temp);
				}
			} catch (ApplicationException e) {
				logger.error("Error recuperando atributo EMPRESA en: " +  certificados[i], e);
			}
		}
	
		if (logger.isDebugEnabled()) logger.debug("Convirtiendo los ID en Empresas. Total Empresas: " +resultsList.size());
		ContentInstance[] result = new ContentInstance[resultsList.size()];
		for (int i = 0; i < resultsList.size(); i++) {
			try {			
			result[i] = RelatorUtils.getRelatedCIByVCMID(resultsList.get(i));
			if (logger.isDebugEnabled()) logger.debug("Elemento :: " + result[i].getAttributeValue("ALIAS"));
			
			} catch (ApplicationException e) {
				if (logger.isDebugEnabled()) logger.debug("Error al Convertir los ID en CI EMPRESA");
			}
		}
		
		// se ordenan por nombre
		if (result.length > 1)
			Arrays.sort(result, new EmpresaContentInstanceComparatorByName());
				
		if (logger.isDebugEnabled()) logger.debug("< getEmpresasCertificadas()");
	    
		return CIUtil.makeBeans(result);
	}
	
	
	/**
	 * Devuelve todos los tipos de certificado
	 * @return List<ManagedObjectBean>
	 */
	public static List<ManagedObjectBean> getTiposCertificados(){
		if (logger.isDebugEnabled()) logger.debug("> getTiposCertificados()");

		// Prepara la query
		//ArrayList<String> resultsList = new ArrayList<String>();
		ArrayList<ContentInstanceWhereClause> query = new ArrayList<ContentInstanceWhereClause>();
		ContentInstanceWhereClause clause =  new ContentInstanceWhereClause();
		clause.checkAttribute("TIPO_CAT", ObjectQueryOp.EQUAL, "tcert");
		query.add(clause);

		// Ejecuta la query
		ContentInstance[] resultado = CMSUtils.getContentInstancesByQuery ("CATEGORIA", query, "ALIAS");
		if (logger.isDebugEnabled()) logger.debug("Recuperados "+resultado.length + " Resultados.");
		
		if (logger.isDebugEnabled()) logger.debug("< getTiposCertificados()");

	    
		return CIUtil.makeBeans(resultado);
	}
	
	
	/**
	 * Devuelve todos los tipos de certificado
	 * @return List<ContentInstance>
	 */
	public static List<ContentInstance> getTiposCertificadosComoCI() {
		if (logger.isDebugEnabled()) logger.debug("> getTiposCertificados()");

		// Prepara la query
		//ArrayList<String> resultsList = new ArrayList<String>();
		ArrayList<ContentInstanceWhereClause> query = new ArrayList<ContentInstanceWhereClause>();
		ContentInstanceWhereClause clause =  new ContentInstanceWhereClause();
		clause.checkAttribute("TIPO_CAT", ObjectQueryOp.EQUAL, "tcert");
		query.add(clause);

		// Ejecuta la query
		ContentInstance[] resultado = CMSUtils.getContentInstancesByQuery ("CATEGORIA", query, "ALIAS");
		if (logger.isDebugEnabled()) logger.debug("Recuperados "+resultado.length + " Resultados.");
		
		if (logger.isDebugEnabled()) logger.debug("< getTiposCertificados()");

	    
		return Arrays.asList(resultado);
	}
	
	/**
	 * 
	 * @param currentChannel
	 * @param tipoCertificado
	 * @param centroNegocio
	 * @return
	 * @throws ApplicationException
	 * @throws ValidationException
	 */
	public static String searchCertificados (Channel currentChannel, String tipoCertificado, String centroNegocio,
			Locale locale, Integer paginaDestino, Integer delta, String sentidoOrden, Integer posicionOrden) throws ApplicationException, ValidationException {
		
		logger.debug("searchCertificados tipoCertificado: "+ tipoCertificado + " centroNegocio: " + centroNegocio + " currentChannel: " + currentChannel.getName()+" locale: " + locale.toString());
		
		//1.- buscamos los certificados cuyo tipo de certificado sea el que se busca
		
		ArrayList<ContentInstanceWhereClause> query = new ArrayList<ContentInstanceWhereClause>();
		
		ContentInstanceWhereClause clauseChannel =  new ContentInstanceWhereClause();
		clauseChannel.checkChannel(ObjectQueryOp.EQUAL, new ChannelRef (currentChannel));
		query.add(clauseChannel);
		
		if (!"".equals(tipoCertificado)) {
			ContentInstanceWhereClause clauseTipoCertificado =  new ContentInstanceWhereClause();
			clauseTipoCertificado.checkAttribute("IDCATEG", ObjectQueryOp.EQUAL, tipoCertificado);
			query.add(clauseTipoCertificado);
		}
		
		ContentInstanceWhereClause clauseLocale =  new ContentInstanceWhereClause();
		clauseLocale.checkLocale(ObjectQueryOp.EQUAL, new AsLocaleRef(locale));
		query.add(clauseLocale);
		
		ContentInstance[] certificados = CMSUtils.getContentInstancesByQuery("CT_CERTIFICADO", query, "ID");
		logger.debug("searchCertificados busqueda inicial por tipo de certificado se han obtenido: " + certificados.length);
		
		//2.- filtramos estos CI por su relacion de CENTROS/NEGOCIOS buscando el valor adecuado.
		
		List<Certificado> results = new ArrayList<>();
		
		for(ContentInstance currentCI : certificados) {
			logger.debug("searchCertificados busqueda centro-negocio para CI: " + currentCI.getName());
			
			String returnValue = "all";
			if (!"".equals(centroNegocio)) {
				returnValue = CMSUtils.getAtt1FromRelWhereAtt2EqualsVal(currentCI, "CT-CERTIFICADO-R-CENTRO", "CT_CERTIFICADO_R_CENTRO_ID", "CT_CERTIFICADO_R_CENTRO_CENTRO_NEGOCIO", centroNegocio);
			}
			
			if (!"".equals(returnValue)) {
				//Lo hemos encontrado o no se filtra por centro de negocio
				results.add(getCurrentCertificado(currentCI));
				
				logger.debug("searchCertificados encontrado centro negocio para CI centro-negocio: " + returnValue);
			}
		}
		
		//3.- Ordenamos la lista por la columna correspondiente
		
		Comparator<Certificado> comparator = null;
		
		switch (posicionOrden) {
		case 0:
			comparator = new ComparadorEmpresaCertificado();
			break;
		case 1:
			comparator = new ComparadorTipoCertificadoCertificado();
			break;
		case 2:
			comparator = new ComparadorOrganoCertificadorCertificado();
			break;
		case 3:
			comparator = new ComparadorNormaCertificado();
			break;
		case 4:
			comparator = new ComparadorFechaCertificacionCertificado();
			break;
		case 5:
			comparator = new ComparadorFechaCaducidadCertificado();
		}
			
		
		if (comparator != null) {
			if ("desc".equals(sentidoOrden)) {
				comparator = Collections.reverseOrder(comparator);
			}
			
			Collections.sort(results, comparator);
		}
		
		//4.- Componemos el json con los resultados
		
		StringBuilder strB = new StringBuilder();
		strB.append("{");
		
		Integer numresultados = results.size();
		strB.append("\"numresultados\":");
		strB.append(numresultados);
		strB.append(",");
		
		strB.append("\"numpaginas\":");
		Integer numpaginas = numresultados / delta;
		if (numresultados % delta != 0) {
			numpaginas++;
		}
		strB.append(numpaginas);
		strB.append(",");
		
		strB.append("\"tipocertificado\":\"");
		strB.append(tipoCertificado);
		strB.append("\",");
		
		strB.append("\"centronegocio\":\"");
		strB.append(centroNegocio);
		strB.append("\",");
		
		strB.append("\"datos\":");
		Integer primero = delta * (paginaDestino - 1);
		Integer ultimo = delta * paginaDestino;
		if (ultimo > numresultados) {
			ultimo = numresultados;
		}
		strB.append(getJsonResultadosCertificados(results, primero, ultimo));
		
		strB.append("}");
		
		return strB.toString();
		
	}
	
	private static Certificado getCurrentCertificado(ContentInstance currentCI) throws ApplicationException {
		
		String empresa = "";
		try {
			ContentInstance ciEmpresa = (ContentInstance) (new ManagedObjectVCMRef((String) currentCI.getAttributeValue("EMPRESA"))).retrieveManagedObject();
			empresa = (String) ciEmpresa.getAttributeValue("ALIAS");
		} catch (RemoteException e) {
			logger.error("searchCertificados error al obtener el alias para la empresa " + (String) currentCI.getAttributeValue("EMPRESA"), e);
		}
		
		String tipocert = "";
		try {
			ContentInstance ciCategoria = (ContentInstance) (new ManagedObjectVCMRef((String) currentCI.getAttributeValue("IDCATEG"))).retrieveManagedObject();
			tipocert = (String) ciCategoria.getAttributeValue("ALIAS");
		} catch (RemoteException e) {
			logger.error("searchCertificados error al obtener el alias para la categoria " + (String) currentCI.getAttributeValue("IDCATEG"), e);
		}
		
		String organoCertificador = (String) currentCI.getAttributeValue("CORG_CERT");
		
		String fichero = "";
		try {
			ContentInstance ciFichero = (ContentInstance) (new ManagedObjectVCMRef((String) currentCI.getAttributeValue("FICHERO"))).retrieveManagedObject();
			StaticFile sfArchivo = (StaticFile) (new ManagedObjectVCMRef((String) ciFichero.getAttributeValue("STATICFILE"))).retrieveManagedObject();
			fichero = sfArchivo.getPlacementPath();
		} catch (RemoteException e) {
			logger.error("searchCertificados error al obtener la ruta para el fichero " + (String) currentCI.getAttributeValue("FICHERO"), e);
		}
		
		String norma = (String) currentCI.getAttributeValue("NORMA");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String fechaCert  = dateFormat.format((Date) currentCI.getAttributeValue("FECHA_CERT"));
		
		String fechaCad = dateFormat.format((Date) currentCI.getAttributeValue("FECHA_CADUCIDAD"));
		
		String descripcion = currentCI.getAttributeValue("DESCRIPCION") != null ? (String) currentCI.getAttributeValue("DESCRIPCION") : "";
		
		return new Certificado(empresa, tipocert, organoCertificador, fichero, norma, fechaCert, fechaCad, descripcion);
		
	}
	
	private static String getJsonResultadosCertificados(List<Certificado> results, Integer primero, Integer ultimo) throws ApplicationException {
		
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		
		for (int i = primero; i < ultimo; i++) {
			
			Certificado result = results.get(i);
			
			if (sb.length() != 1) {
				sb.append(",");
			}
			
			sb.append("{");
			sb.append("\"empresa\":\"");
			sb.append(result.getEmpresa());
			sb.append("\",");
			sb.append("\"tipoCertificado\":\"");
			sb.append(result.getTipoCertificado());
			sb.append("\",");
			sb.append("\"certificado\":\"");
			sb.append(result.getOrganoCertificador());
			sb.append("\",");
			sb.append("\"fichero\":\"");
			sb.append(result.getFicheroNorma());
			sb.append("\",");
			sb.append("\"norma\":\"");
			sb.append(result.getNombreNorma());
			sb.append("\",");
			sb.append("\"certificacion\":\"");
			sb.append(result.getFechaCertificacion());
			sb.append("\",");
			sb.append("\"caducidad\":\"");
			sb.append(result.getFechaCaducidad());
			sb.append("\",");
			sb.append("\"descripcion\":\"");
			sb.append(StringUtils.remove(result.getDescripcion(), "\n"));
			sb.append("\"");
			sb.append("}");
			
		}
		
		sb.append("]");
		
		return sb.toString();
		
	}
	
}