package com.ieci.cepsa17.wem.comun.util.cms.cdaimpl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ieci.cepsa17.util.StringUtil;
import com.ieci.cepsa17.wem.comun.util.cms.IChannelOps;
import com.vignette.as.client.common.ChannelBaseData;
import com.vignette.as.client.common.ContentInstanceDBQuery;
import com.vignette.as.client.common.ContentInstanceWhereClause;
import com.vignette.as.client.common.ManagedObjectDBQuery;
import com.vignette.as.client.common.ManagedObjectWhereClause;
import com.vignette.as.client.common.WhereClause;
import com.vignette.as.client.common.ref.ChannelRef;
import com.vignette.as.client.common.ref.ContentTypeRef;
import com.vignette.as.client.common.ref.ManagedObjectRef;
import com.vignette.as.client.common.ref.ManagedObjectVCMRef;
import com.vignette.as.client.common.ref.ObjectTypeRef;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.exception.ValidationException;
import com.vignette.as.client.javabean.Channel;
import com.vignette.as.client.javabean.ContentType;
import com.vignette.as.client.javabean.IPagingList;
import com.vignette.as.client.javabean.ManagedObject;
import com.vignette.as.client.javabean.ObjectType;
import com.vignette.as.client.javabean.QueryManager;
import com.vignette.ext.templating.util.ContentUtil;
import com.vignette.util.ObjectQueryOp;



/**
 * Implementacion de las operaciones sobre canales basada en el API de CDS.
 * 
 * @author Fernando Salazar
 * 
 */
public class ChannelOpsCdaImpl extends ManagedObjectOpsCdaImpl implements
		IChannelOps {

	/** Log */
	private static final Log logger = LogFactory.getLog(ChannelOpsCdaImpl.class);


	/**
	 * Constructor
	 *
	 */
	public ChannelOpsCdaImpl() {
		
	}
	
	/**
	 * Localiza las instancias de contenido del tipo indicado en
	 * <code>strType</code> asociadas al canal indicado en
	 * <code>channelRef</code>. Devuelve una lista de ManagedObject cuyas
	 * instancias almacenan los campos indicados en <code>attrList</code>
	 * siendo el valor de esos campos el que ten&iacute;an las instancias
	 * localizadas.
	 * 
	 * @param channelRef
	 *            referencia al canal sobre el que se busca
	 * @see com.vignette.as.client.common.ref.ChannelRef
	 * @param strType
	 *            nombre XML (<code>String</code> del tipo de datos sobre el
	 *            que se busca
	 * @param attrList
	 *            lista de atributos a pedir a las instancias localizadas (<code>String</code>
	 *            de valores separados por coma)
	 * @return lista (<code>java.util.List</code> de instancias
	 *         <code>ManagedObject</code>
	 */
	public List findContentInstancesByChannel(ChannelRef channelRef,
			String strType, String attrList) {

		if (logger.isDebugEnabled())
			logger
					.debug("| ChannelOpsCdaImpl.findContentInstancesByChannel | TRACE --> channelRef: "
							+ channelRef.getId()
							+ " strType: "
							+ strType
							+ " attrList: " + attrList);

		// TODO verificar cache
		ContentInstanceWhereClause channelClause = new ContentInstanceWhereClause();

		try {
			channelClause.checkChannel(ObjectQueryOp.EQUAL, channelRef);

			ContentType ciType = (ContentType) ContentType.findByName(strType);

			ContentInstanceDBQuery query = new ContentInstanceDBQuery(
					new ContentTypeRef(ciType.getId()));
			String[] attrs = attrList.split(FIELD_SEPARATOR);
			for (int i = 0; i < attrs.length; i++) {

				if (attrs[i].equals(CONTENTMANAGEMENTID_ATTR_NAME))
					query.addReturnsContentManagementId();
				else
					query.addReturnsAttribute(attrs[i]);
			}
			query.setWhereClause(channelClause);
			IPagingList results = QueryManager.execute(query);

			return results.asList();
		} catch (ApplicationException e) {
			logger
					.error("| ChannelOpsCdaImpl.findContentInstancesByChannel | APPLICATION_EXCEPTION --> "
							+ e.getMessage());
			e.printStackTrace();
		} catch (ValidationException e) {
			logger
					.error("| ChannelOpsCdaImpl.findContentInstancesByChannel | VALIDATION_EXCEPTION --> "
							+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.ieci.micinn.common.util.cms.channel.IChannelOps#findContentInstancesByChannel(com.vignette.as.client.common.ref.ChannelRef,
	 *      java.lang.String)
	 */
	public List findContentInstancesByChannel(ChannelRef channelRef,
			String strType) {

		if (logger.isDebugEnabled())
			logger
					.debug("| ChannelOpsCdaImpl.findContentInstancesByChannel | TRACE --> channelRef: "
							+ channelRef.getId() + " strType: " + strType);

		ContentInstanceWhereClause channelClause = new ContentInstanceWhereClause();

		try {
			channelClause.checkChannel(ObjectQueryOp.EQUAL, channelRef);

			ContentType ciType = (ContentType) ContentType.findByName(strType);

			ContentInstanceDBQuery query = new ContentInstanceDBQuery(
					new ContentTypeRef(ciType.getId()));

			query.setWhereClause(channelClause);
			IPagingList results = QueryManager.execute(query);

			return results.asList();
		} catch (ApplicationException e) {
			logger
					.error("| ChannelOpsCdaImpl.findContentInstancesByChannel | APPLICATION_EXCEPTION --> "
							+ e.getMessage());
			e.printStackTrace();
		} catch (ValidationException e) {
			logger
					.error("| ChannelOpsCdaImpl.findContentInstancesByChannel | VALIDATION_EXCEPTION --> "
							+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param channelRef
	 * @return
	 */
	public String getChannelName(ChannelRef channelRef) {

		Channel channel = null;
		String returnValue = null;
		
		try {
			channel = (Channel) ContentUtil.getManagedObject(channelRef);
			if (channel != null)
			returnValue =  getChannelName(channel);
		} catch (ApplicationException e) {
			logger.error("| ChannelOpsCdaImpl.getChannelName | APPLICATION_EXCEPTION --> msg: "
					+ e.getMessage());
			e.printStackTrace();
			return returnValue;
		}
		return returnValue;
	}

	/**
	 * @param channel
	 * @return
	 */
	public String getChannelName(Channel channel) {

		if (logger.isDebugEnabled())
			logger.debug("| ChannelOpsCdaImpl.getChannelName | ASSERT --> channel!=null: "
					+ (channel != null ? "ASSERT_OK" : "ASSERT_FAILED"));

		return ((ChannelBaseData) channel.getBaseData()).getName();
	}

	/**
	 * @param moVcmRef
	 * @return
	 */
	public Channel getChannelFromManagedObjectVcmRef(
			ManagedObjectVCMRef moVcmRef) {

		try {
			return (Channel) moVcmRef.retrieveManagedObject();
		} catch (ApplicationException e) {
			logger
					.error("| ChannelOpsCdaImpl.getChannelFromManagedObjectVcmRef | APPLICATION_EXCEPTION --> msg: "
							+ e.getMessage());
			e.printStackTrace();
		} catch (RemoteException e) {
			logger
					.error("| ChannelOpsCdaImpl.getChannelFromManagedObjectVcmRef | REMOTE_EXCEPTION --> msg: "
							+ e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * @param channelRef
	 * @return
	 */
	public Channel getChannelFromChannelRef(ChannelRef channelRef) {
		try {
			return channelRef.getChannel();
		} catch (ApplicationException e) {
			logger
					.error("| ChannelOpsCdaImpl.getChannelFromChannelRef | APPLICATION_EXCEPTION --> msg: "
							+ e.getMessage());
			e.printStackTrace();
		} catch (ValidationException e) {
			logger
					.error("| ChannelOpsCdaImpl.getChannelFromChannelRef | VALIDATION_EXCEPTION --> msg: "
							+ e.getMessage());
			e.printStackTrace();
		} catch (RemoteException e) {
			logger
					.error("| ChannelOpsCdaImpl.getChannelFromChannelRef | REMOTE_EXCEPTION --> msg: "
							+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param vcmId
	 * @return
	 */
	public Channel getChannelFromVcmId(String vcmId) {

		if (logger.isDebugEnabled())
			logger.debug("| ChannelOpsCdaImpl.getChannelFromVcmId | TRACE --> vcmId: " + vcmId);

		ManagedObjectVCMRef moVcmRef = new ManagedObjectVCMRef(vcmId);
		return getChannelFromManagedObjectVcmRef(moVcmRef);
	}

	/**
	 * @param stringId
	 * @return
	 */
	public Channel getChannelFromStringId(String stringId) {

		if (logger.isDebugEnabled())
			logger.debug("| ChannelOpsCdaImpl.getChannelFromStringId | TRACE --> stringId: "
					+ stringId);

		ChannelRef channelRef = new ChannelRef(stringId);
		return getChannelFromChannelRef(channelRef);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.ieci.micinn.common.util.cms.IChannelOps#getChannelRefFromStringId(java.lang.String)
	 */
	public ChannelRef getChannelRefFromStringId(String stringId) {

		if (logger.isDebugEnabled())
			logger.debug("| ChannelOpsCdaImpl.getChannelRefFromStringId | TRACE --> stringId: "
					+ stringId);

		return new ChannelRef(stringId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.ieci.micinn.common.util.cms.IChannelOps#getChannelRefFromVcmId(java.lang.String)
	 */
	public ChannelRef getChannelRefFromVcmId(String vcmId) {

		if (logger.isDebugEnabled())
			logger
					.debug("| ChannelOpsCdaImpl.getChannelRefFromVcmId | TRACE --> vcmId: "
							+ vcmId);

		Channel channel = getChannelFromVcmId(vcmId);
		return new ChannelRef(channel.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.ieci.micinn.common.util.cms.IChannelOps#getChannelRefFromChannel(com.vignette.as.client.javabean.Channel)
	 */
	public ChannelRef getChannelRefFromChannel(Channel channel) {
		return new ChannelRef(channel.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.ieci.micinn.common.util.cms.IChannelOps#countChannelContentInstances(com.vignette.as.client.common.ref.ChannelRef,
	 *      java.lang.String)
	 */
	public int countChannelContentInstances(ChannelRef channelRef,
			String strType) {

		ManagedObjectWhereClause channelClause = new ManagedObjectWhereClause();
		channelClause.checkChannel(ObjectQueryOp.EQUAL, channelRef);

		ManagedObjectWhereClause typeClause = new ManagedObjectWhereClause();
		try {
			ObjectType objectType = getObjectTypeByName(strType);
			typeClause.checkObjectTypeId(ObjectQueryOp.EQUAL,
					new ObjectTypeRef(objectType.getId()));
			ArrayList moWhereClauses = new ArrayList();
			moWhereClauses.add(channelClause);
			moWhereClauses.add(typeClause);
			return executeLightQuery(moWhereClauses).size();
		} catch (ApplicationException e) {
			logger
					.error("| ChannelOpsCdaImpl.countChannelContentInstances | APPLICATION_EXCEPTION --> msg: "
							+ e.getMessage());
			e.printStackTrace();
		} catch (ValidationException e) {
			logger
					.error("| ChannelOpsCdaImpl.countChannelContentInstances | VALIDATION_EXCEPTION --> msg: "
							+ e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}

	/**
	 * @param moWhereClauseList
	 * @return
	 * @throws ApplicationException
	 * @throws ValidationException
	 */
	protected IPagingList executeLightQuery(List moWhereClauseList)
			throws ApplicationException, ValidationException {

		ManagedObjectDBQuery moDBQuery = new ManagedObjectDBQuery();
		moDBQuery.addReturnsContentManagementId();

		if (moWhereClauseList != null || moWhereClauseList.size() > 0) {

			Iterator it = moWhereClauseList.iterator();
			ManagedObjectWhereClause moWhereClause = (ManagedObjectWhereClause) it
					.next();

			while (it.hasNext())
				moWhereClause.addWhereClause((WhereClause) it.next());

			moDBQuery.addWhereClause(moWhereClause);
		}
		return QueryManager.execute(moDBQuery);
	}

	/**
	 * @param typeName
	 * @return
	 * @throws ValidationException
	 * @throws ApplicationException
	 */
	protected ObjectType getObjectTypeByName(String typeName)
			throws ApplicationException, ValidationException {

		if (logger.isDebugEnabled())
			logger.debug("| ChannelOpsCdaImpl.getObjectTypeByName | TRACE --> typeName: "
					+ typeName);

		return ObjectType.findByName(typeName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.ieci.micinn.common.util.cms.IChannelOps#countChannelContentInstances(com.vignette.as.client.common.ref.ChannelRef)
	 */
	public int countChannelContentInstances(ChannelRef channelRef) {

		if (logger.isDebugEnabled())
			logger
					.debug("| ChannelOpsCdaImpl.countChannelContentInstances | ASSERT --> channelRef!=null: "
							+ (channelRef != null ? "ASSERT_TRUE"
									: "ASSERT_FAILED"));

		int result = -1;
		try {
			result = Channel.getContentInstanceCount(channelRef);
		} catch (ApplicationException e) {
			logger
					.error("| ChannelOpsCdaImpl.countChannelContentInstances | APPLICATION_EXCEPTION --> msg: "
							+ e.getMessage());
			e.printStackTrace();
		} catch (ValidationException e) {
			logger
					.error("| ChannelOpsCdaImpl.countChannelContentInstances | VALIDATION_EXCEPTION --> msg: "
							+ e.getMessage());
			e.printStackTrace();
		}

		if (logger.isDebugEnabled())
			logger
					.debug("| ChannelOpsCdaImpl.countChannelContentInstances | TRACE --> return_value: "
							+ result);

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.ieci.micinn.common.util.cms.IChannelOps#findContentInstancesByChannel(com.vignette.as.client.common.ref.ChannelRef)
	 */
	public List findContentInstancesByChannel(ChannelRef channelRef) {

		if (logger.isDebugEnabled())
			logger
					.debug("| ChannelOpsCdaImpl.findContentInstancesByChannel | ASSERT --> channelRef!=null: "
							+ (channelRef != null ? "ASSERT_TRUE"
									: "ASSERT_FAILED"));

		Channel channel = getChannelFromChannelRef(channelRef);

		try {
			IPagingList results = channel.getContentInstances(null);

			if (logger.isDebugEnabled())
				logger
						.debug("| ChannelOpsCdaImpl.findContentInstancesByChannel | TRACE --> results.size(): "
								+ results.size());

			return results.asList();

		} catch (ValidationException e) {
			logger
					.error("| ChannelOpsCdaImpl.findContentInstancesByChannel | VALIDATION_EXCEPTION --> msg: "
							+ e.getMessage());
			e.printStackTrace();
		} catch (ApplicationException e) {
			logger
					.error("| ChannelOpsCdaImpl.findContentInstancesByChannel | APPLICATION_EXCEPTION --> msg: "
							+ e.getMessage());
			e.printStackTrace();
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see es.ieci.micinn.common.util.cms.IChannelOps#isChannel(com.vignette.as.client.common.ref.ManagedObjectVCMRef)
	 */
	public boolean isChannel(ManagedObjectVCMRef moVcmRef) {

		ManagedObject mo;
		try {
			mo = moVcmRef.retrieveManagedObject();
			return mo.getObjectTypeRef().getId().equals(
					Channel.getTypeObjectTypeRef().getId());
		} catch (Exception e) {
			if (logger.isDebugEnabled())
				logger.debug("| ChannelOpsCdaImpl.isChannel | EXCEPTION --> msg: "
						+ e.getMessage());
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.ieci.micinn.common.util.cms.IChannelOps#isChannel(com.vignette.as.client.common.ref.ManagedObjectRef)
	 */
	public boolean isChannel(ManagedObjectRef moRef) {

		ManagedObject mo;
		try {
			mo = moRef.retrieveManagedObject();
			return mo.getObjectTypeRef().getId().equals(
					Channel.getTypeObjectTypeRef().getId());
		} catch (Exception e) {
			if (logger.isDebugEnabled())
				logger.debug("| ChannelOpsCdaImpl.isChannel | EXCEPTION --> msg: "
						+ e.getMessage());
		}
		return false;
	}

	/**
	 * @param request
	 * @param paramKey
	 * @return Channel
	 */
	private Channel getChannelFromRequestParam(ServletRequest request,
			String paramKey) {

		String paramValue = request.getParameter(paramKey);
		if (StringUtil.isEmptyIgnoreWhitespace(paramValue)) {
			if (logger.isDebugEnabled())
				logger
						.debug("| ChannelOpsCdaImpl.getChannelFromRequestParam | ASSERT --> request["
								+ paramKey + "]!=null: ASSERT_FAILED");
			return null;
		}

		if (logger.isDebugEnabled())
			logger.debug("| ChannelOpsCdaImpl.getChannelFromRequestParam | TRACE --> probando "
					+ paramKey + " valor: " + paramValue);

		int idType = guessIdType(paramKey);

		switch (idType) {

		case MANAGEDOBJECTREF:
			if (logger.isDebugEnabled())
				logger
						.debug("| ChannelOpsCdaImpl.getChannelFromRequestParam | TRACE --> tipo estimado: MANAGEDOBJECT_REF");
			ChannelRef channelRef = new ChannelRef(paramValue);
			if (isChannel(channelRef))
				return getChannelFromChannelRef(channelRef);

		case MANAGEDOBJECTVCMREF:
			if (logger.isDebugEnabled())
				logger
						.debug("| ChannelOpsCdaImpl.getChannelFromRequestParam | TRACE --> tipo estimado: MANAGEDOBJECT_VCMREF");

			ManagedObjectVCMRef moVcmRef = new ManagedObjectVCMRef(paramValue);
			if (isChannel(moVcmRef))
				return getChannelFromManagedObjectVcmRef(moVcmRef);

		default:
			return null;

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.ieci.micinn.common.util.cms.IChannelOps#guessChannelFromRequest(javax.servlet.ServletRequest)
	 */
	public Channel guessChannelFromRequest(ServletRequest request) {

		Channel returnValue = null;

		returnValue = getChannelFromRequestParam(request, VGNEXTOID_PARAM);

		if (returnValue == null)
			returnValue = getChannelFromRequestParam(request,
					VGNEXTCHANNEL_PARAM);

		if (logger.isDebugEnabled()) {
			if (returnValue == null)
				logger
						.debug("| ChannelOpsCdaImpl.guessChannelFromRequest | ERROR --> canal no encontrado");
			else
				logger
						.debug("| ChannelOpsCdaImpl.guessChannelFromRequest | TRACE --> returnValue: "
								+ returnValue.getContentManagementId().getId());
		}

		return returnValue;
	}
	
	
}
