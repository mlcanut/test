package com.ieci.cepsa17.wem.comun.util;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.vignette.as.client.api.bean.BeanFactory;
import com.vignette.as.client.api.bean.BeanFactoryFactory;
import com.vignette.as.client.api.bean.ContentItemBean;
import com.vignette.as.client.api.bean.ManagedObjectBean;
import com.vignette.as.client.common.ref.ChannelRef;
import com.vignette.as.client.common.ref.ManagedObjectVCMRef;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.exception.ValidationException;
import com.vignette.as.client.javabean.Channel;
import com.vignette.as.client.javabean.ContentInstance;
import com.vignette.as.client.javabean.ManagedObject;
import com.vignette.as.client.javabean.Site;
import com.vignette.ext.templating.util.ContentUtil;
import com.vignette.ext.templating.util.RequestContext;

import es.ieci.cepsa.utilities.as.client.i18nbean.I18nContentInstance;

import com.ieci.cepsa17.util.I18nContentInstanceUtils;
import com.ieci.cepsa17.wem.comun.util.cms.CMSUtils;
import com.ieci.cepsa17.wem.comun.util.cms.RelatorUtils;

public class CIUtil  {
    private static Logger logger = Logger.getLogger(CIUtil.class);
	
	final static String SITE_ATTR_LITERALES = "LITERALES";
	final static BeanFactory beanFactory = BeanFactoryFactory.getFactory();

	
	public static String getLiteral(Site site, String literal, String defecto, String idioma) {
		// Inicializamos el valor por defecto
		if (defecto == null) defecto = "";
		String result = defecto;
		
		I18nContentInstance i18n = null;
		try {
			String ciGUID = (String)site.getAttributeValue(SITE_ATTR_LITERALES);
			
			if (ciGUID != null) {
				// Si hay literales recuperamos el objeto y de él la traducción del literal
				//    Si no devolveremos el valor por defecto
				i18n = (I18nContentInstance)ContentUtil.getManagedObject(new ManagedObjectVCMRef(ciGUID));
				
				result = i18n.getPropertyValueFromLocale(literal, idioma, defecto);
			}			
		} catch (ApplicationException e) {
			logger.debug(e.getMessage());
		}
		return result;
	}

	public static String getLiteral(RequestContext rc, String literal, String defecto, String idioma) {
		Site site = null;
		try {
			// Recuperamos el site de la RequestContext y el locale sólo si viene vacio
			site = rc.getCurrentSite();
			if (idioma == null) {
				Locale locale = rc.getLocale();
				idioma=locale.getLanguage();
				String country=locale.getCountry();
				if(country!=null && !"".equals(country)) {
					idioma+="_"+country;
				}
			}
		} catch (ApplicationException e) {
			logger.debug(e.getMessage());
		}
		return getLiteral(site, literal, defecto, idioma);
	}
	
	public static String getLiteral(RequestContext rc, String literal, String defecto) {
		return getLiteral(rc, literal, defecto, null);
	}
	
	public static String getLiteral(RequestContext rc, String literal) {
		return getLiteral(rc, literal, null, null);
	}
	
	/**
	 * Retorna el bean correspondiente al objeto
	 * @param mo  El objeto a convertir
	 * @return Bean
	 */
	public static ManagedObjectBean makeBean(ManagedObject mo) {
		return beanFactory.createBean(mo);
	}
	
	/**
	 * Convierte una lista de objetos a sus beans correspondientes
	 * @param lmo  Lista de objetos
	 * @return Lista de beans
	 */
	public static List<ManagedObjectBean> makeBeans(List<ManagedObject> lmo) {
		List<ManagedObjectBean> queryResultsJavaBeanList = new ArrayList<ManagedObjectBean>();
	    for (ManagedObject mo : lmo)
	    {
	      ManagedObjectBean bean = beanFactory.createBean(mo);
	      queryResultsJavaBeanList.add(bean);
	    }
	    
		return queryResultsJavaBeanList;
	}
	
	/**
	 * Convierte un array de objetos a sus beans correspondientes
	 * @param lmo  Array de objetos
	 * @return Lista de beans
	 */
	public static List<ManagedObjectBean> makeBeans(ManagedObject[] lmo) {
		List<ManagedObjectBean> queryResultsJavaBeanList = new ArrayList<ManagedObjectBean>();
	    for (ManagedObject mo : lmo)
	    {
	      ManagedObjectBean bean = beanFactory.createBean(mo);
	      queryResultsJavaBeanList.add(bean);
	    }
	    
		return queryResultsJavaBeanList;
	}
	
	public static List<ManagedObjectBean> getSubChannelBeans(Channel selectedParent) throws ApplicationException, ValidationException, RemoteException {
		List<ManagedObjectBean> channelBeans = new ArrayList<>();

		try {
			List<ChannelRef> selChannelChildren = selectedParent.getActiveSubChannelRefs();
			if (selChannelChildren.size() > 0) {
				for (ChannelRef selChannelChild : selChannelChildren) {
					channelBeans.add(CIUtil.makeBean(selChannelChild.getChannel()));
				}
			}
		} catch (ApplicationException | ValidationException e) {
			logger.error("Error obteniendo los subcanales del canal " + selectedParent.getContentManagementId().getId() +
					": " + e.getMessage(), e);
		}

		return channelBeans;
	}
	
	public static String getFirstParentId(RequestContext rc) {
		
		String firstParentId = "";
		
		try {
			int depth = CMSUtils.getCurrentChannelDepth(rc);
			firstParentId = CMSUtils.getCurrentChannelID(rc);
			Channel firstParentChannel = null;
			for (int i = 0; i < depth - 2; i++) {
				firstParentChannel = (Channel) (new ManagedObjectVCMRef(firstParentId)).retrieveManagedObject();
				firstParentId = firstParentChannel.getParentChannelRef().getChannel().getContentManagementId().getId();
			}
		} catch (ApplicationException | RemoteException | ValidationException e) {
			try {
				logger.error("Error obteniendo el id del primer padre del canal " + rc.getCurrentLink());
			} catch (ApplicationException e1) {
				logger.error("Error obteniendo current link");
			}
		}
		
		return firstParentId;
		
	}
	
	/**
	 * @param rc	RequestContext 
	 * @param ci	ContentItem
	 * @param literal	I18n attribute to retrieve
	 * @param def	Valor por defecto
	 * @return Value of the the internationalized attribute, if it does not exist an empty string
	 */
	public static String getOldPropertyI18n(RequestContext rc, ContentInstance ci, String attr, String def) {
		String result = null;
		try {
			result = I18nContentInstanceUtils.getPropertyI18n(rc, ci, attr, def);
		} catch (Exception e) {
			StringBuffer msg = new StringBuffer();
			msg.append("Excepción recuperando traducciones:\n");
			msg.append("GUID:" + ci.getContentManagementId().toString() + "\n");
			msg.append("ATTR:" + attr + "\n");
			logger.warn(msg, e);
		}
		if (result != null)
			return result;
		else
			return def;
	}
	
	public static String getOldPropertyI18n(RequestContext rc, String guid, String attr, String def) {
		ContentInstance ci = null;
		if (guid != null && !"".equalsIgnoreCase(guid))
			ci = RelatorUtils.getRelatedCIByVCMID(guid);
		if (ci == null) {
			if (logger.isInfoEnabled()) logger.info("Error recuperando traducciones de: " + guid + " para ATTR:" + attr);
			return def;
		}
		return getOldPropertyI18n(rc, ci, attr, def);
	}
	
	public static String getOldPropertyI18n(RequestContext rc, ContentItemBean cib, String attr, String def) {
		String guid = null;
		if (cib != null)
			guid = cib.getSystem().getId();
		else {
			if (logger.isInfoEnabled()) logger.info("Error recuperando traducciones de un objeto null para ATTR:" + attr);
			return def;
		}
		return getOldPropertyI18n(rc, guid, attr, def);
	}
	
	public static String getOldPropertyI18nFromPageAttr(RequestContext rc, String pageAttr, String attr, String def) {
		String guid = (String)rc.getPageContext().getAttribute(pageAttr);
		if (logger.isDebugEnabled()) logger.debug("getOldPropertyI18nFromPageAttr - GUID: "+ guid);
		return getOldPropertyI18n(rc, guid, attr, def);
	}
	
	public static void setOldPropertyI18nFromPageAttr(RequestContext rc, String pageAttrTo, String pageAttrFrom, String attr, String def) {
		String guid = (String)rc.getPageContext().getAttribute(pageAttrFrom);
		if (logger.isDebugEnabled()) logger.debug("setOldPropertyI18nFromPageAttr - GUID: "+ guid);
		rc.getPageContext().setAttribute(pageAttrTo, getOldPropertyI18n(rc, guid, attr, def));
	}
}
