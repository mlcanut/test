package com.ieci.cepsa17.wem.comun.util.cms.cdaimpl;

import com.ieci.cepsa17.util.StringUtil;
import com.ieci.cepsa17.wem.comun.util.cms.IManagedObjectOps;

/**
 * Clase de utilidad
 * @author 66083486
 *
 */
public class ManagedObjectOpsCdaImpl implements IManagedObjectOps {

	/**
	 * Constructor
	 *
	 */
	public ManagedObjectOpsCdaImpl() {}
	/**
	 * Adivina que parametro representa el canal en la request.
	 * @param id String con el identeficado
	 * @return int con el resultado
	 */
	public int guessIdType(String id) {
		
		if (StringUtil.isEmptyIgnoreWhitespace(id))
			return WRONG_TYPE;
		
		if (id.endsWith("____"))
			return MANAGEDOBJECTREF;
		else if (id.length()== ID_LENGTH && id.endsWith("RCRD"))
			return MANAGEDOBJECTVCMREF;
		else if (id.length()== ID_LENGTH && id.endsWith("STFL"))
			return STATICFILE;
		else
			return STRINGID;
	}
	private static final int ID_LENGTH = 40;
}
