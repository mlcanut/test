package com.ieci.cepsa17.wem.comun.util.buscador;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import com.ieci.cepsa17.comercial.wem.guialubricacion.service.GuiaLubricantesServices;
import com.ieci.cepsa17.comercial.wem.guialubricacion.view.bean.FormBean;
import com.ieci.cepsa17.wem.comun.util.CIUtil;
import com.ieci.cepsa17.wem.constants.BuscadorGuiaLubricacionConstants;
import com.vignette.as.client.api.bean.ManagedObjectBean;
import com.vignette.as.client.common.ContentInstanceWhereClause;
import com.vignette.as.client.common.ref.AsLocaleRef;
import com.vignette.as.client.common.ref.ManagedObjectVCMRef;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.javabean.AttributedObject;
import com.vignette.as.client.javabean.ContentInstance;
import com.vignette.util.ObjectQueryOp;
import com.vignette.util.StringQueryOp;

import es.cepsa.ws.olyslagerapi.beans.ArrayCategory;
import es.cepsa.ws.olyslagerapi.beans.ArrayMake;
import es.cepsa.ws.olyslagerapi.beans.ArrayModel;
import es.cepsa.ws.olyslagerapi.beans.ArrayType;
import es.cepsa.ws.olyslagerapi.beans.Component;
import es.cepsa.ws.olyslagerapi.beans.Model;
import es.cepsa.ws.olyslagerapi.beans.Product;
import es.cepsa.ws.olyslagerapi.beans.Recommendation;
import es.cepsa.ws.olyslagerapi.beans.Type;
import es.cepsa.ws.olyslagerapi.beans.Use;
import es.ieci.cepsa.utilities.common.cms.CMSUtils;
import es.cepsa.ws.olyslagerapi.beans.Image;
import es.cepsa.ws.olyslagerapi.beans.Make;

public class UtilBuscadorGuiaLubricacion {

	protected static Logger logger = Logger.getLogger(UtilBuscadorGuiaLubricacion.class);

	private UtilBuscadorGuiaLubricacion() {
		super();
	}

	public static ArrayCategory getListaCategorias(String idioma) {
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::getListaCategorias -- Parámetros de entrada:: idioma --> " + idioma);
		}
		
		try {
			FormBean request = FormBean.createInstanceOlyslager();
			request.setIdioma(idioma);
			//logger.debug("UtilBuscadorGuiaLubricacion lsCategorias --> después de crear FormBean");
					
			GuiaLubricantesServices guiaLubricantesServices = getConnection();
			//logger.debug("UtilBuscadorGuiaLubricacion lsCategorias --> después de getConnection");
			
			ArrayCategory resultado = guiaLubricantesServices.obtenerCategorias(request);
			//logger.debug("UtilBuscadorGuiaLubricacion lsCategorias resultado --> " + resultado);
			
			if (resultado != null && resultado.getCategories() != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getListaCategorias número de resultados --> " + resultado.getCategories().length + " categorías encontradas");
				}
			} 
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getListaCategorias --> No se han encontrado categorías");
				}
			}

			return resultado;
			
		}
		catch (Exception e) {
			logger.error("UtilBuscadorGuiaLubricacion::getListaCategorias --> Se ha producido un error en la busqueda de categorías: " + e.getMessage(), e);
		}

		return new ArrayCategory();
	}
	
	public static HashMap<String, ArrayList<Type>> getTiposPorMarcaModelo(String idCategoria, String texto, String idioma, boolean recuperarImagen) {
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModelo -- Parámetros de entrada:: idCategoria --> " + idCategoria + " - Texto --> " + texto + " - Idioma --> " + idioma + " Recuperar imagenes --> " + String.valueOf(recuperarImagen));
		}
		
		try {
			HashMap<String, ArrayList<Type>> map = new HashMap<String, ArrayList<Type>>();
			
			FormBean request = FormBean.createInstanceOlyslager(BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_TIPOS_POR_MARCA_MODELO, idCategoria, texto);
			request.setIdioma(idioma);
			
			if (logger.isDebugEnabled()) {		
				logger.debug("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModelo --> Búsqueda:: Categoria --> " + request.getCategoria() + " -- Texto --> " + request.getTexto() + " -- Idioma --> " + request.getIdioma());
			}
					
			GuiaLubricantesServices guiaLubricantesServices = getConnection();
			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModelo  --> después de getConnection");
			}
			
			ArrayType resultado = guiaLubricantesServices.obtenerTiposPorMarcaModelo(request);
			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModelo --  resultado --> " + resultado);
			}	
			
			if (null != resultado) {
				Type[] arrayTypesIter = resultado.getTypes();
				int lonArrayTypes = arrayTypesIter.length;
				
				// Recorremos el array de tipos para crear los nuevos tipos que se añadirán al nuevo ArrayType
				for (int i=0; i<lonArrayTypes; i++){
					Type typeIter = arrayTypesIter[i];
					// Añadimos a los datos de Olysalger los datos descompuestos del nombre del modelo
					Type type = getTiposDatos(typeIter);
					
					request.setTipo(typeIter.getId());
					
					// Si se requiere que los tipos se acompañen con las imágenes
					if (recuperarImagen) {	    	
				    	//logger.debug("UtilBuscadorGuiaLubricacion lsMarcaModelo --> antes de buscar imagen ID --> " + typeIter.getId());
				    	Image image = guiaLubricantesServices.obtenerImagenTipo(request);
				    	//logger.debug("UtilBuscadorGuiaLubricacion lsMarcaModelo --> después de buscar imagen image --> " + image);
				    	type.setImg(image.getImg());
					}
					
					// La clave para identificar un modelo es la suma de marca y modelos (para tener accesible la marca para pintar la jsp)
					String make = type.getMake();
					String model = type.getModel();
					String key = make + " " + model;
					ArrayList<Type> arrayTypes = map.get(key);
					
					if (null == arrayTypes) {
						if (logger.isDebugEnabled()) {
							logger.debug("No existe en el MAP la clave --> " + key);
						}
						
						arrayTypes = new ArrayList<Type>();
					}	
					
					arrayTypes.add(type);
					map.put(key, arrayTypes);
					
					if (logger.isDebugEnabled()) {
						logger.debug("Añadido al MAP -- Key --> " + key + " Type --> " + typeIter);
					}
				}
				
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModelo -- Número de resultados --> " + resultado.getTypes().length + " tipos encontradas para la búsqueda");
				}
			}
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModelo --> No se han encontrado tipos para la búsqueda");
				}
			}

			return map;
			
		}
		catch (Exception e) {
			logger.error("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModelo --> Se ha producido un error en la busqueda de Tipos por Marca/Modelo: " + e.getMessage(), e);
		}

		return new HashMap<String, ArrayList<Type>>();
	}
	
	public static ArrayMake getListaMarcas(String idCategoria, String idioma) {
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::getListaMarcas -- Parámetros de entrada::  idCategoria --> " + idCategoria + " idioma --> " + idioma);
		}
		
		try {
			FormBean request = FormBean.createInstanceOlyslager(BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_MARCAS, idCategoria, null);
			request.setIdioma(idioma);
			
			if (logger.isDebugEnabled()) {		
				logger.debug("UtilBuscadorGuiaLubricacion::getListaMarcas --> Busqueda Lista Marcas:: Categoria --> " + request.getCategoria() + " -- Idioma --> " + request.getIdioma());
			}
					
			GuiaLubricantesServices guiaLubricantesServices = getConnection();
			//logger.debug("UtilBuscadorGuiaLubricacion lsMarcas --> después de getConnection");
			
			ArrayMake resultado = guiaLubricantesServices.obtenerListaMarcas(request);
			//logger.debug("UtilBuscadorGuiaLubricacion lsMarcas resultado --> " + resultado);
			
			if (resultado != null && resultado.getMakes() != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getListaMarcas --> " + resultado.getMakes().length + " marcas encontradas para la búsqueda");
				}
			} 
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getListaMarcas --> No se han encontrado marcas para la búsqueda");
				}
			}

			return resultado;		
		}
		catch (Exception e) {
			logger.error("UtilBuscadorGuiaLubricacion::getListaMarcas --> Se ha producido un error en la busqueda de Marcas: " + e.getMessage(), e);
		}

		return new ArrayMake();
	}
	
	public static HashMap<String, ArrayList<Model>> getListaModelos(String idMarca, String idioma, boolean recuperarImagen) {
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::getListaModelos -- Parámetros de entrada:: idMarca --> " + idMarca + " - Idioma --> " + idioma + " Recuperar imagenes --> " + String.valueOf(recuperarImagen));
		}
		
		try {
			HashMap<String, ArrayList<Model>> map = new HashMap<String, ArrayList<Model>>();
			
			FormBean request = FormBean.createInstanceOlyslager(BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_MODELOS, idMarca, null);
			request.setIdioma(idioma);
			
			if (logger.isDebugEnabled()) {		
				logger.debug("UtilBuscadorGuiaLubricacion::getListaModelos -- Busqueda Lista Modelos:: Marca --> " + request.getMarca() + " -- Idioma --> " + request.getIdioma());
			}
					
			GuiaLubricantesServices guiaLubricantesServices = getConnection();
			logger.debug("UtilBuscadorGuiaLubricacion::getListaModelos --> después de getConnection");
			
			ArrayModel resultado = guiaLubricantesServices.obtenerListaModelos(request);
			//logger.debug("UtilBuscadorGuiaLubricacion lsModelos resultado --> " + resultado);
			
			Model[] arrayModelsIter = null;
			
			if (null != resultado) {
				arrayModelsIter = resultado.getModels();
				int lonArrayModels = arrayModelsIter.length;
				//Model[] arrayModels = new Model[lonArrayModels];
				
				// Recorremos el array de modelos para crear los nuevos modelos que se añadirán al nuevo ArrayModel
				for (int i=0; i<lonArrayModels; i++){
					Model modelIter = arrayModelsIter[i];	
					
					// Añadimos a los datos de Olysalger los datos descompuestos del nombre del modelo
					Model model = getModelosDatos(modelIter);
				
					request.setModelo(modelIter.getId());
					
					// Si se requiere que los tipos se acompañen con las imágenes
					if (recuperarImagen) {	    	
						//logger.debug("UtilBuscadorGuiaLubricacion lsModelos --> antes de buscar imagen ID --> " + model.getId());
				    	Image image = guiaLubricantesServices.obtenerImagenModelo(request);
				    	//logger.debug("UtilBuscadorGuiaLubricacion lsModelos --> después de buscar imagen image --> " + image);
				    	model.setImg(image.getImg());
					}	
					
					String modelName = model.getModelName();
					
					ArrayList<Model> arrayModels = map.get(modelName);
					if (null == arrayModels) {
						if (logger.isDebugEnabled()) {
							logger.debug("No existe en el MAP la clave --> " + modelName);
						}
						
						arrayModels = new ArrayList<Model>();
					}	
					
					arrayModels.add(model);
					map.put(modelName, arrayModels);		
					
					if (logger.isDebugEnabled()) {
						logger.debug("Añadido al MAP -- Key --> " + modelName + " Type --> " + modelIter);
					}
				}	
			}
			
			if (resultado != null && resultado.getModels() != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getListaModelos -- Número de resultados--> " + resultado.getModels().length + " modelos encontrados para la búsqueda");
				}
			} 
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getListaModelos --> No se han encontrado modelos para la búsqueda");
				}
			}
			
			return map;
			
		}
		catch (Exception e) {
			logger.error("UtilBuscadorGuiaLubricacion::getListaModelos --> Se ha producido un error en la busqueda de Modelos: " + e.getMessage(), e);
		}
	
		return new HashMap<String, ArrayList<Model>>();
	}
			
	public static HashMap<String, ArrayList<Type>> getListaTipos(String idModelo, String idioma) {
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::getListaTipos Parámetros de entrada:: Modelo --> " + idModelo + " - Idioma --> " + idioma);
		}
		
		try {
			HashMap<String, ArrayList<Type>> map = new HashMap<String, ArrayList<Type>>();
			
			FormBean request = FormBean.createInstanceOlyslager(BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_TIPOS, idModelo, null);
			request.setIdioma(idioma);
			
			if (logger.isDebugEnabled()) {		
				logger.debug("UtilBuscadorGuiaLubricacion ::getListaTipos -- Busqueda Lista Tipos:: Tipo --> " + request.getModelo() + " -- Idioma --> " + idioma);
			}
					
			GuiaLubricantesServices guiaLubricantesServices = getConnection();
			//logger.debug("UtilBuscadorGuiaLubricacion lsTipos --> después de getConnection");
			
			ArrayType resultado = guiaLubricantesServices.obtenerListaTipos(request);
			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorGuiaLubricacion lsTipos::getListaTipos -- resultado --> " + resultado);
			}
			
			if (null != resultado) {
				Type[] arrayTypesIter = resultado.getTypes();
				int lonArrayTypes = arrayTypesIter.length;
				
				// Recorremos el array de tipos para crear los nuevos tipos que se añadirán al nuevo ArrayType
				for (int i=0; i<lonArrayTypes; i++){
					Type typeIter = arrayTypesIter[i];
					String nameIter = typeIter.getName();
			    	String typeYearsIter = "";
	
			    	int index = nameIter.lastIndexOf('(');
					if (index != -1) {
						int index2 = nameIter.lastIndexOf(')');
						if (index2 != -1)
							typeYearsIter = nameIter.substring(index+1, index2).trim();
						
						ArrayList<Type> arrayTypes = map.get(typeYearsIter);
						if (null == arrayTypes) {
							if (logger.isDebugEnabled()) {
								logger.debug("No existe en el MAP la clave --> " + typeYearsIter);
							}
							
							arrayTypes = new ArrayList<Type>();
						}	
						
						arrayTypes.add(typeIter);
						map.put(typeYearsIter, arrayTypes);		
						
						if (logger.isDebugEnabled()) {
							logger.debug("Añadido al MAP -- Key --> " + typeYearsIter + " Type --> " + typeIter);
						}
					}	
				}
				
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getListaTipos -- Número de resultados --> " + resultado.getTypes().length + " tipos encontradas para la búsqueda");
				}
			}
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getListaTipos --> No se han encontrado tipos para la búsqueda");
				}
			}

			return map;
			
		}
		catch (Exception e) {
			logger.error("UtilBuscadorGuiaLubricacion lsTipos::getListaTipos --> Se ha producido un error en la busqueda de Tipos: " + e.getMessage(), e);
		}
	
		return new HashMap<String, ArrayList<Type>>();
	}
	
	public static ArrayType getTiposPorMarcaModeloPredictivo(String idCategoria, String texto, String idioma) {
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModeloPredictivo -- Parámetros de entrada:: idCategoria --> " + idCategoria + " - Texto --> " + texto + " - Idioma --> " + idioma);
		}
		
		try {
			FormBean request = FormBean.createInstanceOlyslager(BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_TIPOS_PREDICTIVO, idCategoria, texto);
			request.setIdioma(idioma);
			
			if (logger.isDebugEnabled()) {		
				logger.debug("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModeloPredictivo --> Busqueda:: Categoria --> " + request.getCategoria() + " -- Texto --> " + request.getTexto() + " -- Idioma --> " + idioma);
			}
					
			GuiaLubricantesServices guiaLubricantesServices = getConnection();
			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModeloPredictivo  --> después de getConnection");
			}
			
			ArrayType resultado = guiaLubricantesServices.obtenerTiposPorMarcaModeloPredictivo(request);
			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModeloPredictivo -- Resultado --> " + resultado);
			}	
						
			if (resultado != null && resultado.getTypes()!= null) {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModeloPredictivo -- Número de resultados --> " + resultado.getTypes().length + " tipos encontradas para la búsqueda");
				}
			} 
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModeloPredictivo --> No se han encontrado tipos para la búsqueda");
				}
			}

			return resultado;
			
		}
		catch (Exception e) {
			logger.error("UtilBuscadorGuiaLubricacion::getTiposPorMarcaModeloPredictivo --> Se ha producido un error en la busqueda de Tipos por Marca/Modelo Predictivo: " + e.getMessage(), e);
		}

		return new ArrayType();
	}
	
	public static Image getImagenMarca(String idMarca) {
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::getImagenMarca -- idMarca --> " + idMarca);
		}
		
		try {
			FormBean request = FormBean.createInstanceOlyslager(BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_IMAGEN_MARCA, idMarca, null);
			if (logger.isDebugEnabled()) {		
				logger.debug("UtilBuscadorGuiaLubricacion::getImagenMarca --> Búsqueda Imagen Marca: " + request.getMarca());
			}
					
			GuiaLubricantesServices guiaLubricantesServices = getConnection();
				
			Image resultado = guiaLubricantesServices.obtenerImagenMarca(request);
			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorGuiaLubricacion::getImagenMarca -- Resultado --> " + resultado);
			}
			
			if (resultado != null && resultado.getImg() != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getImagenMarca -- Se ha encontrado la imagen asociada a la marca buscada");
				}
			} 
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getImagenMarca -- NO se ha encontrado la imagen asociada a la marca buscada");
				}
			}

			return resultado;			
		}
		catch (Exception e) {
			logger.error("UtilBuscadorGuiaLubricacion::getImagenMarca --> Se ha producido un error en la busqueda de la imagen asociada a la Marca: " + e.getMessage(), e);
		}

		return new Image();
	}
	
	public static Image getImagenModelo(String idModelo) {
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::getImagenModelo -- idModelo --> " + idModelo);
		}
		
		try {
			FormBean request = FormBean.createInstanceOlyslager(BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_IMAGEN_MODELO, idModelo, null);
			if (logger.isDebugEnabled()) {		
				logger.debug("UtilBuscadorGuiaLubricacion::getImagenModelo --> Búsqueda Imagen Modelo: " + request.getModelo());
			}
					
			GuiaLubricantesServices guiaLubricantesServices = getConnection();
				
			Image resultado = guiaLubricantesServices.obtenerImagenModelo(request);
			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorGuiaLubricacion::getImagenModelo -- Resultado --> " + resultado);
			}
			
			if (resultado != null && resultado.getImg() != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getImagenModelo -- Se ha encontrado la imagen asociada al Modelo buscado");
				}
			} 
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getImagenModelo -- NO se ha encontrado la imagen asociada al Modelo buscado");
				}
			}

			return resultado;			
		}
		catch (Exception e) {
			logger.error("UtilBuscadorGuiaLubricacion::getImagenModelo --> Se ha producido un error en la busqueda de la imagen asociada al Modelo: " + e.getMessage(), e);
		}

		return new Image();
	}
	
	public static Image getImagenTipo(String idTipo) {
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::getImagenTipo idTipo --> " + idTipo);
		}
		
		try {
			FormBean request = FormBean.createInstanceOlyslager(BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_IMAGEN_TIPO, idTipo, null);
			if (logger.isDebugEnabled()) {		
				logger.debug("UtilBuscadorGuiaLubricacion::getImagenTipo --> Búsqueda Imagen Tipo: " + request.getTipo());
			}
					
			GuiaLubricantesServices guiaLubricantesServices = getConnection();
				
			Image resultado = guiaLubricantesServices.obtenerImagenTipo(request);
			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorGuiaLubricacion::getImagenTipo -- Resultado --> " + resultado);
			}
			
			if (resultado != null && resultado.getImg() != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getImagenTipo -- Se ha encontrado la imagen asociada al tipo buscado");
				}
			} 
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion::getImagenTipo -- NO se ha encontrado la imagen asociada al tipo buscado");
				}
			}

			return resultado;			
		}
		catch (Exception e) {
			logger.error("UtilBuscadorGuiaLubricacion::getImagenTipo --> Se ha producido un error en la busqueda de la imagen asociada al Tipo: " + e.getMessage(), e);
		}

		return new Image();
	}
	
	public static Recommendation getRecomendacion(String idTipo, String idioma) {
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::getRecomendacion -- Parámetros de entrada:: Tipo --> " + idTipo + " -- Idioma --> " + idioma);
		}
		
		try {
			FormBean request = FormBean.createInstanceOlyslager(BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_RECOMENDACION, idTipo, null);
			request.setIdioma(idioma);
			
			if (logger.isDebugEnabled()) {		
				logger.debug("UtilBuscadorGuiaLubricacion:: getRecomendacion -- Busqueda Recomendacion:: Tipo: " + request.getTipo() + " -- Idioma --> " + request.getIdioma());
			}
					
			GuiaLubricantesServices guiaLubricantesServices = getConnection();
				
			Recommendation resultado = guiaLubricantesServices.obtenerRecomendacion(request);
			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorGuiaLubricacion:: getRecomendacion -- Resultado --> " + resultado);
			}
			
			if (resultado != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion:: getRecomendacion -- Se ha encontrado la recomendación asociada al tipo buscado");
				}
				
				// Añadimos los datos del producto de la tabla CT_PRODUCTO
				String locale = "es";
				if (BuscadorGuiaLubricacionConstants.LANGUAGE_ISO3_ENG.equals(idioma))
					locale = "en";
				else if (BuscadorGuiaLubricacionConstants.LANGUAGE_ISO3_POR.equals(idioma))
					locale = "pt";
				
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion -- Idioma del portal --> " + locale);
				}
				
				resultado = getRecommendationDB(resultado, locale);
				logger.debug("UtilBuscadorGuiaLubricacion:: getRecomendacion -- Se han añadido los datos de la tabla CT_PRODUCTO a la recomendación asociada al tipo buscado");
			} 
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion:: getRecomendacion -- NO se ha encontrado la recomendación asociada al tipo buscado");
				}
			}

			return resultado;			
		}
		catch (Exception e) {
			logger.error("UtilBuscadorGuiaLubricacion:: getRecomendacion --> Se ha producido un error en la recomendación asociada al Tipo: " + e.getMessage(), e);
		}

		return new Recommendation();
	}
		
	public static ArrayMake getListaMarcasPopulares(String idCategoria, String idioma, boolean recuperarImagen) {
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::getListaMarcasPopulares -- idCategoria --> " + idCategoria + " -- Idioma -->  " + idioma);
		}
		
		try {
			ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
			
			FormBean request = FormBean.createInstanceOlyslager(BuscadorGuiaLubricacionConstants.TIPO_BUSQUEDA_MARCAS_MAS_BUSCADAS, idCategoria, null);
			request.setIdioma(idioma);
			
			if (logger.isDebugEnabled()) {		
				logger.debug("UtilBuscadorGuiaLubricacion::getListaMarcasPopulares --> Busqueda Lista Marcas::Categoria--> " + request.getCategoria() + " " + request.getIdioma());
			}
					
			GuiaLubricantesServices guiaLubricantesServices = getConnection();
			ArrayMake resultado = new ArrayMake();
			
			String mock = resourceBundle.getString(BuscadorGuiaLubricacionConstants.MOCK_MARCAS_POPULARES);
			if (logger.isDebugEnabled()) {		
				logger.debug("UtilBuscadorGuiaLubricacion:: getListaMarcasPopulares -- Mock --> " + mock);
				logger.debug("UtilBuscadorGuiaLubricacion:: getListaMarcasPopulares -- Indicador mock --> " + BuscadorGuiaLubricacionConstants.HAY_MOCK_MARCAS_POPULARES);
			}
			
			if (BuscadorGuiaLubricacionConstants.HAY_MOCK_MARCAS_POPULARES.equals(mock)) {
				if (logger.isDebugEnabled()) {		
					logger.debug("UtilBuscadorGuiaLubricacion:: getListaMarcasPopulares --> Hay Mock de las marcas más populares");
				}
				
				resultado = leerMarcasPopulares(idCategoria, idioma);
			}
			else {
				if (logger.isDebugEnabled()) {		
					logger.debug("UtilBuscadorGuiaLubricacion:: getListaMarcasPopulares --> NO hay Mock de las marcas más populares");
				}
				
				resultado = guiaLubricantesServices.obtenerListaMarcasPopulares(request);
			}	
			
			Make[] arrayMakesIter = null;
			
			if (null != resultado) {
				arrayMakesIter = resultado.getMakes();
				int lonArrayMakes = arrayMakesIter.length;
				Make[] arrayMakes = new Make[lonArrayMakes];
				
				// Si se requiere que los tipos se acompañen con las imágenes
				if (recuperarImagen) {	    	
					// Recorremos el array de marcas para crear las nuevas marcas que se añadirán al nuevo ArrayMake
					for (int i=0; i<arrayMakesIter.length; i++) {
						Make make = arrayMakesIter[i];
						logger.debug("UtilBuscadorGuiaLubricacion:: getListaMarcasPopulares --> Busqueda Lista Marcas Populares ID: " + make.getId());
	
						request.setMarca(make.getId());
						Image image = guiaLubricantesServices.obtenerImagenMarca(request);
						if (null != image)
							make.setImg(image.getImg());
						
						arrayMakes[i] = make;
					}
					
					resultado.setMakes(arrayMakes);
				}	
			}	
			
			//logger.debug("UtilBuscadorGuiaLubricacion lsMarcasPopulares resultado --> " + resultado);
			
			if (resultado != null && resultado.getMakes() != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion:: getListaMarcasPopulares --> " + resultado.getMakes().length + " marcas populares encontradas para la búsqueda");
				}
			} 
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion:: getListaMarcasPopulares --> No se han encontrado marcas populares para la búsqueda");
				}
			}

			return resultado;		
		}
		catch (Exception e) {
			logger.error("UtilBuscadorGuiaLubricacion lsMarcasPopulares --> Se ha producido un error en la busqueda de Marcas populares: " + e.getMessage(), e);
		}

		return new ArrayMake();
	}

	private static GuiaLubricantesServices getConnection() throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::getConnection");
		}
		
		String endpoint = "";
		String usuario = "";
		String pass = "";
		String usesProxy = ""; 
		String usuarioProxy = "";
		String passProxy = "";
		String proxyHost = "";
		String puertoProxy = "";
		boolean proxy = false;
		String timeOut = "";
		
		try {
			ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorGuiaLubricacion resourceBundle --> " + resourceBundle);
			}

			endpoint = resourceBundle.getString(BuscadorGuiaLubricacionConstants.END_POINT);
			usuario = resourceBundle.getString(BuscadorGuiaLubricacionConstants.USER_NAME);
			pass = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PASS);
			usesProxy = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PROXY);
	
			if (BuscadorGuiaLubricacionConstants.USES_PROXY.equals(usesProxy)) {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion PROXY --> SI");
				}
				
				proxy = true;
				usuarioProxy = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PROXY_USER);
				passProxy = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PROXY_PASS);
				proxyHost = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PROXY_HOST);
				puertoProxy = resourceBundle.getString(BuscadorGuiaLubricacionConstants.PROXY_PORT);	
			}
			else {
				if (logger.isDebugEnabled()) {
					logger.debug("UtilBuscadorGuiaLubricacion PROXY --> NO");
				}
			}
			
			//timeOut = resourceBundle.getString(BuscadorGuiaLubricacionConstants.TIME_OUT_OLYSLAGER);
			
			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorGuiaLubricacion endpoint --> " + endpoint);
				logger.debug("UtilBuscadorGuiaLubricacion usuario --> " + usuario);
				logger.debug("UtilBuscadorGuiaLubricacion pass --> " + pass);
				logger.debug("UtilBuscadorGuiaLubricacion usesProxy --> " + usesProxy);
				logger.debug("UtilBuscadorGuiaLubricacion usuarioProxy --> " + usuarioProxy);
				logger.debug("UtilBuscadorGuiaLubricacion passProxy --> " + passProxy);
				logger.debug("UtilBuscadorGuiaLubricacion proxyHost --> " + proxyHost);
				logger.debug("UtilBuscadorGuiaLubricacion puertoProxy --> " + puertoProxy);
				logger.debug("UtilBuscadorGuiaLubricacion time out Olyslager --> "  + timeOut);
			}
		} 
		catch (final Exception e) {
			throw new Exception("Se ha producido un error obteniendo la conexion " + BuscadorGuiaLubricacionConstants.WS_BUNDLE, e);
		}
		
		return GuiaLubricantesServices.getInstance(endpoint, true, usuario, pass, proxy, proxyHost, puertoProxy, usuarioProxy, passProxy);	// , timeOut
	}
	
//	/**
//	 * Método para recuperar un array de marcas más populares proporcionado por CEPSA mientras no se disponga del servicio de Olyslager
//	 * @param Se leen las marcas más populares del fichero config.properties
//	 * @return ArrayMake 
//	 */
//	private static ArrayMake leerMarcasPopulares() {
//		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
//		ArrayMake makes = new ArrayMake();
//		
//		String marcas = resourceBundle.getString(BuscadorGuiaLubricacionConstants.MAKES_POPULAR);
//		if (logger.isDebugEnabled()) {
//			logger.debug("UtilBuscadorGuiaLubricacion::leerMarcasPopulares - cadena --> " + marcas + " cadena leída del fichero de configuración");
//		}
//		
//		if (null != marcas && !BuscadorGuiaLubricacionConstants.NO_MAKES_POPULAR.equals(marcas)) {
//			try {		
//				String separador = Pattern.quote("@");
//				String[] parts = marcas.split(separador);
//				
//				if (null != parts) {
//					if (logger.isDebugEnabled()) {
//						logger.debug("UtilBuscadorGuiaLubricacion::leerMarcasPopulares - cantidad --> " + parts.length + " marcas leídas del fichero de configuración");
//					}
//					
//					int lon = parts.length;
//					Make[] arrMake = new Make[lon];
//					
//					for (int i=0; i< lon; i++) {
//						String marca = parts[i];
//						if (null != marca) {
//							String[] stringArr = marca.split(";");
//							
//							Make make = new Make();
//							make.setId(stringArr[0]);		
//							make.setName(stringArr[1]);
//							make.setPosition(stringArr[2]);
//							arrMake[i] = make;
//						}	
//						else
//							logger.error("ERROR GRAVE RECUPERANDO MARCAS MÁS POPULARES. NO SE HA ENCONTRADO SEPARADOR ; ENTRE CAMPOS DE MARCAS EN FICHERO PROPERTIES");
//					}
//					
//					makes.setMakes(arrMake);
//					
//					if (logger.isDebugEnabled()) {
//						Make[] arrayMakesIter = makes.getMakes();
//						for (int i=0; i<1; i++) {
//							Make makeIter = arrayMakesIter[i];
//							logger.debug("UtilBuscadorGuiaLubricacion::leerMarcasPopulares - marca almacenada --> " +  makeIter.getId() + "-" + makeIter.getName() + "-" + makeIter.getPosition());
//						}
//					}	
//				}
//				else
//					logger.error("ERROR GRAVE RECUPERANDO MARCAS MÁS POPULARES. NO SE HA ENCONTRADO SEPARADOR @ ENTRE MARCAS EN FICHERO PROPERTIES");
//			}
//			catch (final Exception e) {
//				logger.error("ERROR GRAVE RECUPERANDO MARCAS MÁS POPULARES DESDE FICHERO PROPERTIES", e);
//			}
//		}
//		else
//			logger.error("ERROR GRAVE RECUPERANDO MARCAS MÁS POPULARES. NO HAY CADENA DE MARCAS MÁS POPULARES EN FICHERO PROPERTIES");
//		
//		return makes;
//	}
	
	/**
	 * Método para recuperar un array de marcas más populares proporcionado por CEPSA mientras no se disponga del servicio de Olyslager
	 * @param NO_CATEGORIA: lista de la página inicial en la que pueden estar mezcaldas distintas categorías, ID categoría a buscar
	 * @return ArrayMake 
	 */
	private static ArrayMake leerMarcasPopulares(String idCategoria, String idioma) {
		ResourceBundle resourceBundle = ResourceBundle.getBundle(BuscadorGuiaLubricacionConstants.WS_BUNDLE);
		ArrayMake makes = new ArrayMake();
		
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::leerMarcasPopulares - idCategoria --> " + idCategoria);
		}
		
		String marcas = "";
		
		if (BuscadorGuiaLubricacionConstants.NO_CATEGORIA.equals(idCategoria))
			marcas = resourceBundle.getString(BuscadorGuiaLubricacionConstants.MAKES_POPULAR);
		else
			marcas = resourceBundle.getString(BuscadorGuiaLubricacionConstants.MAKES_POPULAR + "." + idCategoria);
		
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion::leerMarcasPopulares - cadena --> " + marcas + " cadena leída del fichero de configuración");
		}
		
		// Comprobamos si se recupera una cadena con resultados (distinta de NO_MAKES_POPULAR)
		if (null != marcas && !BuscadorGuiaLubricacionConstants.NO_MAKES_POPULAR.equals(marcas)) {
			try {		
				String separador = Pattern.quote("@");
				String[] parts = marcas.split(separador);
				
				if (null != parts) {
					if (logger.isDebugEnabled()) {
						logger.debug("UtilBuscadorGuiaLubricacion::leerMarcasPopulares - cantidad --> " + parts.length + " marcas leídas del fichero de configuración");
					}
					
					int lon = parts.length;
					Make[] arrMake = new Make[lon];
					
					for (int i=0; i< lon; i++) {
						String marca = parts[i];
						if (logger.isDebugEnabled()) {
							logger.debug("UtilBuscadorGuiaLubricacion::leerMarcasPopulares - marca --> " + marca);
						}
						
						if (null != marca) {
							String[] stringArr = marca.split(":");
							
							Make make = new Make();
							make.setId(stringArr[0]);		
							make.setName(stringArr[1]);
							make.setPosition(stringArr[2]);
							
							// Solo para la lista de las marcas más populares (de la página inicial) se busca la categoría asociada
							if (BuscadorGuiaLubricacionConstants.NO_CATEGORIA.equals(idCategoria)) {
								// Recuperamos la categoría del properties
								String idCategoriaMarca = stringArr[3];
								
								if (logger.isDebugEnabled()) {
									logger.debug("UtilBuscadorGuiaLubricacion::getListaCategorias -- idCategoriaMarca --> " + idCategoriaMarca);
								}
								
								make.setIdCategory(idCategoriaMarca);
							}
							
							arrMake[i] = make;
						}	
						else
							logger.error("ERROR GRAVE RECUPERANDO MARCAS MÁS POPULARES. NO SE HA ENCONTRADO SEPARADOR : ENTRE CAMPOS DE MARCAS EN FICHERO PROPERTIES");
					}
					
					makes.setMakes(arrMake);
					
					if (logger.isDebugEnabled()) {
						Make[] arrayMakesIter = makes.getMakes();
						for (int i=0; i<1; i++) {
							Make makeIter = arrayMakesIter[i];
							logger.debug("UtilBuscadorGuiaLubricacion::leerMarcasPopulares - marca almacenada --> " +  makeIter.getId() + "-" + makeIter.getName() + "-" + makeIter.getPosition());
						}
					}	
				}
				else
					logger.error("ERROR GRAVE RECUPERANDO MARCAS MÁS POPULARES. NO SE HA ENCONTRADO SEPARADOR @ ENTRE MARCAS EN FICHERO PROPERTIES");
			}
			catch (final Exception e) {
				logger.error("ERROR GRAVE RECUPERANDO MARCAS MÁS POPULARES DESDE FICHERO PROPERTIES", e);
			}
		}
		else
			logger.error("NO HAY CADENA DE MARCAS MÁS POPULARES EN FICHERO PROPERTIES");
		
		return makes;
	}
	
	/**
	 * Este método modifica un texto acortando el número de palabras que contiene, es decir nos trunca el texto pasado.
	 * 
	 * @param texto el texto del cual queremos truncar
	 * @param numMax el número máximo de caracteres que contedrá el texto devuelto
	 * @param CortarArribaAbajo true --> Si el texto queda cortado a la mitad de una palabra, nos la devuelve entera.
	 * 		  false -> Es ingnorada esta última palabra.
	 * @param coletilla Se añade al final del texto devuelto una coletilla. pe. "...", "ver más", "etc".
	 * @return devuelve el texto truncado y con la coletilla correspondiente.
	 */
	public static String RemplazaPalabra(String texto, int numMax, boolean CortarArribaAbajo, String coletilla) {	
		int i = 0;
		boolean blancoEncontrado = false;
		char[] aux = null;
		StringBuffer aux2 = null ;
		aux2 = new StringBuffer();
		aux = texto.toCharArray();
		
		if (texto.length()>numMax) {
			while(i < aux.length && !blancoEncontrado){
				if(i < numMax){
					aux2.insert(i,aux[i]);
					
				}
				else if (CortarArribaAbajo && aux[i]!=' ') {
					aux2.insert(i,aux[i]);
				}
				else {
					blancoEncontrado = true;
				}
				
				i++;
			}
			
			i = numMax-1;
			if (!CortarArribaAbajo){
				while( i > 0 && aux[i]!=' ') {
					aux2.deleteCharAt(i);
					i--;
				}
				aux2.deleteCharAt(i);
			}
		}
		else 
			return texto;
		
		return aux2.toString() + coletilla;		
	}
	
	/**
	 * Método para  añadir al array de modelos proporcionado por Olyslager los datos desglosados del nombre del modelo (nombre, código y años de fabricación)
	 * @param Model de Olyslager
	 * @return Model 
	 */
	private static Model getModelosDatos(Model modelIter) {
		// Modelo creado con los datos del WS de Olysalger más los datos descompuestos del modelo
		Model model = new Model() ;
		
		String id = modelIter.getId();
		String name = modelIter.getName();
		model.setId(id);
		model.setName(name);
		if (logger.isDebugEnabled()) {
			logger.debug("name --> " + name);
		}
		
		String modelName = "";
		String modelCode = "";
		String modelYears = "";
		String modelAux = "";
		
		// Los datos del modelo pueden venir de 2 formas:
		// Active Tourer serie 2, F45 (2014- ) Modelname, Modelcode Modelyears (separado por coma)
		// City / Jazz (1984-1988) Modelname y Modelcode son lo mismo 
		int index = name.indexOf(',');
		
		// "Altea XL, 5P5 - Altea XL 1.6 TDI (77 kW) Ecomotive (2010-2016)" 
		// Los modelos puden tener varios paréntesis. Por ello, cogemos los últimos para seleccionar los años de fabricación
		int index2 = -1;
		if (index != -1) {
			modelName = name.substring(0, index).trim();
			modelAux = name.substring(index + 1, name.length()).trim();
			index = modelAux.lastIndexOf('(');
			if (index != -1)
				modelCode = modelAux.substring(0, index).trim();	
			
			index2 = modelAux.lastIndexOf(')');
			if (index2 != -1)
				modelYears = modelAux.substring(index, index2 + 1).trim();
		}
		else {
			index = name.lastIndexOf('(');
			if (index != -1) {
				modelName = name.substring(0, index).trim();
				modelCode = modelName;
			
				index2 = name.lastIndexOf(')');
				if (index != -1)
					modelYears = name.substring(index, index2 + 1).trim();
			}	
		}	
		
		model.setModelCode(modelCode);
		model.setModelName(modelName);
		model.setModelYears(modelYears);
		
//		if (logger.isDebugEnabled()) {
//			logger.debug("getModelosDatos::modelCode --> " + modelCode);
//			logger.debug("getModelosDatos::modelName --> " + modelName);
//			logger.debug("getModelosDatos::modelYears --> " + modelYears);
//		}

		return model;
	}
		
	/**
	 * Método para  añadir al array de tipos proporcionado por Olyslager los datos desglosados del nombre del tipo (marca, modelo y tipo)
	 * Sólo se puede utilizar para formatear los resultados de la búsqueda por texto introducido por el usuario (NO en la búsqueda de tipos a partir de un código de modelo)
	 * @param Type de Olyslager
	 * @return Type
	 */
	private static Type getTiposDatos(Type typeIter) {
		// Tipo creado con los datos del WS de Olysalger más los datos descompuestos del tipo
		Type type = new Type();
		
		String id = typeIter.getId();
		String name = typeIter.getName();
		type.setId(id);
		type.setName(name);
		
		String[] stringArr = name.split(" - ");
		type.setMake(stringArr[0]);		
		String modelo = stringArr[1];
		type.setModel(modelo);
		type.setType(stringArr[2]);
		
//		if (logger.isDebugEnabled()) {
//			logger.debug("getTiposDatos::make --> " + stringArr[0]);
//			logger.debug("getTiposDatos::modelo --> " + modelo);
//			logger.debug("getTiposDatos::type --> " + stringArr[2]);
//		}
			
		return type;
	}
	
	/**
	 * Método para  añadir a la recomendación proporcionada por Olyslager los datos del producto obtenidos de la tabla CT_PRODUCTO
	 * @param Type de Olyslager
	 * @return Type
	 */
	private static Recommendation getRecommendationDB(Recommendation recommendation, String locale) {	
		ArrayList<Component> lsComponents = recommendation.getComponents();
		ArrayList<Component> lsComponentsDB = new ArrayList<Component>();
		ArrayList<Use> lsUses = new ArrayList<Use>();
		//ArrayList<Capacity> lsCapacities = new ArrayList<Capacity>();
		ArrayList<Product> lsProducts = new ArrayList<Product>();

		if (null != lsComponents) {
			logger.debug("getRecommendationDB::lsComponents.size() --> " + lsComponents.size());
			
			for (int i1 = 0; i1 < lsComponents.size(); i1++) {
				Component component = lsComponents.get(i1);
				//logger.debug("getRecommendationDB::component.getName() --> " + component.getName());
				if (null != component) {
					lsUses = component.getUses();
					//lsCapacities = component.getCapacities();
					
					//logger.debug("getRecommendationDB::lsUses.size() --> " + lsUses.size());
					if (null != lsUses) {
						ArrayList<Use> lsUsesDB = new ArrayList<Use>();
						
						for (int i2 = 0; i2 < lsUses.size(); i2++) {
							Use use = lsUses.get(i2);
							//logger.debug("getRecommendationDB::use.getName() --> " + use.getName());
							if (null != use) {
								lsProducts = use.getProducts();
								
								//logger.debug("getRecommendationDB::lsProducts.size() --> " + lsProducts.size());
								ArrayList<Product> lsProductsDB = new ArrayList<Product>();
								
								for (int i3 = 0; i3 < lsProducts.size(); i3++) {
									Product product = lsProducts.get(i3);
									String productCode = product.getProductcode();
									
									//logger.debug("getRecommendationDB::idProduct --> " + productCode);
									if (null != productCode) {
										Product productoDB = getDatosProductoDB(product, locale);
										lsProductsDB.add(productoDB);
									}
								}	
								
								use.setProducts(lsProductsDB);
								lsUsesDB.add(use);
							}
						}
						
						component.setUses(lsUsesDB);
						//component.setCapacities(lsCapacities);
					}

					lsComponentsDB.add(component);
				}
			}
			
			recommendation.setComponents(lsComponentsDB);
		}		
	
		return recommendation;
	}
	
	private static Product getDatosProductoDB(Product product, String locale) {
		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorGuiaLubricacion -- getDatosProductoDB con Idioma --> " + locale);
		}

		// Buscamos los datos del producto en la tabla CT_PRODUCTO por el productcode devuelto por Olyslager
		String reference = product.getProductcode();
		logger.debug("UtilBuscadorGuiaLubricacion: getDatosProductoDB --> " + reference);
		
		Product productDB = new Product();
		try {
			productDB.setApporder(product.getApporder());
			productDB.setId(product.getId());
			productDB.setName(product.getName());
			productDB.setNumber(product.getNumber());
			productDB.setProductcode(product.getProductcode());
			
			String idDB = "";
			String nombre = "";
			String descripcion = "";
			String imagen = ""; 
			
			ArrayList<ContentInstanceWhereClause> query = new ArrayList<>();
			ContentInstanceWhereClause clause = new ContentInstanceWhereClause();
			clause.setMatchAny(false);
			
			Locale localeR = new Locale(locale); //  new Locale(NavigationConstants.LANGUAGE_ES, NavigationConstants.COUNTRY_ES);
			AsLocaleRef aslocale = new AsLocaleRef(localeR);
			clause.checkLocale(ObjectQueryOp.EQUAL, aslocale);
			clause.checkAttribute(BuscadorGuiaLubricacionConstants.PRODUCTO_CODIGO_CEPSA, StringQueryOp.EQUAL, reference);
				
			query.add(clause);
			
			/* Ejecuta la query */
			ContentInstance[] resultado = CMSUtils.getContentInstancesByQuery(BuscadorGuiaLubricacionConstants.CT_PRODUCTO, query, BuscadorGuiaLubricacionConstants.PRODUCTO_CODIGO_CEPSA);
			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorGuiaLubricacion -- Con el código " + reference + ", recuperados " + resultado.length + " resultados.");
			}
						
			for (int i=0; i<resultado.length; i++) {
				try {			
					String localeRef = resultado[i].getLocale().getJavaLocale().getLanguage();
					if (logger.isDebugEnabled()) {
						logger.debug("UtilBuscadorGuiaLubricacion -- Idioma registro recuperado --> " + localeRef);
					}

					//if (localeRef.equals(locale)) { 	
						if (logger.isDebugEnabled()) {
							logger.debug("UtilBuscadorGuiaLubricacion -- Los Idiomas del registro recuperado y del portal son iguales --> " + localeRef + " -- " +  locale);
						}
						
						//Guardamos una instancia del ManagedObjectBean para en la jsp poder crear la url que enlace con el producto
						ManagedObjectBean mob = CIUtil.makeBean(resultado[i]); 
						productDB.setManagedObjectBean(mob);
						
						idDB = (String) resultado[i].getAttributeValue("ID");
						nombre = (String) resultado[i].getAttributeValue("NOMBRE");
						descripcion = (String) resultado[i].getAttributeValue("DESCRIPCION_CORTA");
						//imagen = (String) resultado[i].getAttributeValue("IMAGEN");
						
						if (logger.isDebugEnabled()) {
							logger.debug("ID del producto :: " + idDB);
							logger.debug("Nombre del producto :: " + nombre);
							logger.debug("Descripcion del producto :: " + descripcion);
						}
						
						productDB.setIdDB(idDB);
						productDB.setNombre(nombre);
						productDB.setDescripcionCorta(descripcion);
						
						// Buscamos las imágenes relacionadas con el producto (sólo necesitamos la imagen principal la 1ª)
						AttributedObject[] relacionados = resultado[i].getRelations("VCM-MANVIG-CT-PRODUCTO-R-IMAGEN");	
						if (logger.isDebugEnabled()) {
							logger.debug("UtilBuscadorGuiaLubricacion VCM-MANVIG-CT-PRODUCTO-R-IMAGEN relacionados --> " + relacionados);
						}
						
						if (null != relacionados) {
							if (logger.isDebugEnabled()) {
								logger.debug("UtilBuscadorGuiaLubricacion VCM-MANVIG-CT-PRODUCTO-R-IMAGEN se han encontrado " + relacionados.length + " imágenes relacionadas");
							}
							
							if (relacionados != null && relacionados.length > 0) {
								try {
									for (int j=0; j<relacionados.length; j++) {
										String idCi = (String)relacionados[j].getAttribute("IMAGEN_RELACIONADA").getValue();	
										if (logger.isDebugEnabled()) 
											logger.debug("UtilBuscadorGuiaLubricacion VCM-MANVIG-CT-PRODUCTO-R-IMAGEN idCi " + idCi);
											
										Integer orden = (Integer)relacionados[j].getAttribute("ORDEN_IMAGEN").getValue();	
										if (logger.isDebugEnabled()) 
											logger.debug("UtilBuscadorGuiaLubricacion VCM-MANVIG-CT-PRODUCTO-R-IMAGEN orden " + orden);
											
										int intOrden = orden.intValue();
										if (logger.isDebugEnabled()) 
											logger.debug("UtilBuscadorGuiaLubricacion VCM-MANVIG-CT-PRODUCTO-R-IMAGEN intOrden " + intOrden);
		
										// Sólo queremos la primera imagen que es la que identifica el producto
										if (intOrden == 1)  {
											ContentInstance contenido = (ContentInstance) (new ManagedObjectVCMRef(idCi)).retrieveManagedObject();	
											
											if (logger.isDebugEnabled()) {
												logger.debug("UtilBuscadorGuiaLubricacion -- getDatosProductoDB contenido " + contenido);
											}
											
											if (contenido.getObjectType().getName().equals("Image")) {
												if (logger.isDebugEnabled()) {
													logger.debug("UtilBuscadorGuiaLubricacion -- getDatosProductoDB contenido.getAttributeValue " + contenido.getAttributeValue("SOURCEPATH"));
												}
												
												imagen = contenido.getAttributeValue("SOURCEPATH").toString(); 
												productDB.setImagen(imagen);
												
												if (logger.isDebugEnabled()) {
													logger.debug("Imagen del producto :: " + imagen);
												}	
											}
											
											break;
										}	
									}	
								}	
								catch (Exception e) {
									logger.error("Error recuperando atributos relacionados de CT_PRODUCTO ", e);
								}
							}		
						//}
					}
					else
						logger.debug("UtilBuscadorGuiaLubricacion -- getDatosProductoDB - NO hay contenidos relacionados");
					
					//logger.debug("getDatosProductoDB:: Producto final " + productDB.toString());
				} 
				catch (ApplicationException e) {
					logger.error("Error recuperando atributos CT_PRODUCTO en: " +  resultado[i], e);
				}
			}
			
			return productDB;
		
			//return CIUtil.makeBeans(resultado);

		} 
		catch (Exception e) {
			logger.error("UtilBuscadorGuiaLubricacion -- Se ha producido un error en la búsqueda de Productos por el código " + reference + ": " + e.getMessage(), e);
		}

		return new Product();
	}
	
}