package com.ieci.cepsa17.wem.comun.util.buscador.bean;

import java.io.Serializable;

public class Certificado implements Serializable {

	private static final long serialVersionUID = -1966993390164234219L;

	private String empresa;
	private String tipoCertificado;
	private String organoCertificador;
	private String ficheroNorma;
	private String nombreNorma;
	private String fechaCertificacion;
	private String fechaCaducidad;
	private String descripcion;

	public Certificado() {
		super();
	}

	public Certificado(String empresa, String tipoCertificado, String organoCertificador, String ficheroNorma,
			String nombreNorma, String fechaCertificacion, String fechaCaducidad, String descripcion) {
		super();
		this.empresa = empresa;
		this.tipoCertificado = tipoCertificado;
		this.organoCertificador = organoCertificador;
		this.ficheroNorma = ficheroNorma;
		this.nombreNorma = nombreNorma;
		this.fechaCertificacion = fechaCertificacion;
		this.fechaCaducidad = fechaCaducidad;
		this.descripcion = descripcion;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getTipoCertificado() {
		return tipoCertificado;
	}

	public void setTipoCertificado(String tipoCertificado) {
		this.tipoCertificado = tipoCertificado;
	}

	public String getOrganoCertificador() {
		return organoCertificador;
	}

	public void setOrganoCertificador(String organoCertificador) {
		this.organoCertificador = organoCertificador;
	}

	public String getFicheroNorma() {
		return ficheroNorma;
	}

	public void setFicheroNorma(String ficheroNorma) {
		this.ficheroNorma = ficheroNorma;
	}

	public String getNombreNorma() {
		return nombreNorma;
	}

	public void setNombreNorma(String nombreNorma) {
		this.nombreNorma = nombreNorma;
	}

	public String getFechaCertificacion() {
		return fechaCertificacion;
	}

	public void setFechaCertificacion(String fechaCertificacion) {
		this.fechaCertificacion = fechaCertificacion;
	}

	public String getFechaCaducidad() {
		return fechaCaducidad;
	}

	public void setFechaCaducidad(String fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}