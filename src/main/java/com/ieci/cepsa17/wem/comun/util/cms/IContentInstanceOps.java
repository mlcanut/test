package com.ieci.cepsa17.wem.comun.util.cms;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.vignette.as.client.common.ref.ManagedObjectRef;
import com.vignette.as.client.common.ref.ManagedObjectVCMRef;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.javabean.ManagedObject;

/**
 * API de acceso a VCM para operaciones relacionadas con instancias de contenido.
 * 
 * @author Fernando Salazar
 *
 */
public interface IContentInstanceOps extends IManagedObjectOps {
	
	public static final GregorianCalendar THE_FIRST_DAY = new GregorianCalendar(1970, 0, 1);
	
	/**
	 * Recupera un java.util.List con las instancias de contenido del tipo indicado (nombre XML) que hayan sido
	 * modificadas entre las fechas startDate y endDate.
	 * <p>
	 * NOTA IMPORTANTE: esta funcion utiliza el API de SearchQuery en lugar del API de ManagedObjectDBQuery
	 * 
	 * @param strType	nombre XML (<code>String</code> del tipo de datos sobre el que se busca
	 * @param startDate fecha inicial. Si startDate==null startDate se establece a 1 de enero de 1970
	 * @param endDate fecha final. Si endDate==null endDate se establece a NOW
	 * @return	List con los content instance correspondientes
	 */
	public List searchContentInstancesByModTime(String strType, Date startDate, Date endDate, boolean fullDays);
	
	/**
	 * Recupera un java.util.List con las instancias de contenido del tipo indicado (nombre XML) que hayan sido
	 * modificadas entre las fechas startDate y endDate.
	 * <p>
	 * NOTA IMPORTANTE: solo funciona en el stage de Management
	 * 
	 * @param strType		nombre XML del tipo de contenido
	 * @param startDate		fecha inicial. Si startDate==null startDate se establece a 1 de enero de 1970
	 * @param endDate		fecha final. Si endDate==null endDate se establece a NOW
	 * @param fullDays		si true la fecha final se ajusta a las 23:59 y la inicial a las 0:00
	 * @return				List con los content instance correspondientes
	 */
	public List findContentInstancesByModTime(String strType, Date startDate, Date endDate, boolean fullDays);
	
	/**
	 * Indica el ManagedObject
	 * @param moRef ManagedObjectRef
	 * @return ManagedObject 
	 * @throws ApplicationException
	 */
	public ManagedObject getManagedObject(ManagedObjectRef moRef) throws ApplicationException;
	
    /**
     * Indica el ManagedObject
     * @param vcmRef ManagedObjectVCMRef
     * @return ManagedObject 
     * @throws ApplicationException
     */
    public ManagedObject getManagedObject(ManagedObjectVCMRef vcmRef) throws ApplicationException;	
}

