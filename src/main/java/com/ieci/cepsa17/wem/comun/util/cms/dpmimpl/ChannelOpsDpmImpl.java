package com.ieci.cepsa17.wem.comun.util.cms.dpmimpl;

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ieci.cepsa17.wem.comun.util.cms.cdaimpl.ChannelOpsCdaImpl;
import com.vignette.as.client.common.ManagedObjectDBQuery;
import com.vignette.as.client.common.ManagedObjectWhereClause;
import com.vignette.as.client.common.ref.ChannelRef;
import com.vignette.as.client.common.ref.ManagedObjectRef;
import com.vignette.as.client.common.ref.ManagedObjectVCMRef;
import com.vignette.as.client.common.ref.ObjectTypeRef;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.exception.ValidationException;
import com.vignette.as.client.javabean.Channel;
import com.vignette.as.client.javabean.ManagedObject;
import com.vignette.as.client.javabean.ObjectType;
import com.vignette.ext.templating.util.ContentUtil;
import com.vignette.util.ObjectQueryOp;




/**
 * Implementacion de las operaciones sobre canales basada en el API CDS.
 * <p>
 * Utiliza los servicios de Dynamic Portal, fundamentalmente la ObjectCache.
 * 
 * @author Fernando Salazar
 *
 */
public class ChannelOpsDpmImpl extends ChannelOpsCdaImpl {

	/** Log */
	private static final Log logger = LogFactory.getLog(ChannelOpsDpmImpl.class);
	
	/**
	 * Constructor
	 *
	 */
	public ChannelOpsDpmImpl() {}
	/**
	 * Localiza las instancias de contenido del tipo indicado en
	 * <code>strType</code> asociadas al canal indicado en
	 * <code>channelRef</code>. Devuelve una lista de ManagedObject cuyas
	 * instancias almacenan los campos indicados en <code>attrList</code>
	 * siendo el valor de esos campos el que ten&iacute;an las instancias
	 * localizadas.
	 * 
	 * @param channelRef
	 *            referencia al canal sobre el que se busca
	 * @see com.vignette.as.client.common.ref.ChannelRef
	 * @param strType
	 *            nombre XML (<code>String</code> del tipo de datos sobre el
	 *            que se busca
	 * @param attrList
	 *            lista de atributos a pedir a las instancias localizadas (<code>String</code>
	 *            de valores separados por coma)
	 * @return lista (<code>java.util.List</code> de instancias
	 *         <code>ManagedObject</code>
	 */
	public List findContentInstancesByChannel(ChannelRef channelRef, String strType) {
		
		if (logger.isDebugEnabled())
			logger.debug("| ChannelOpsDpmImpl.findContentInstancesByChannel | TRACE --> channelRef: "+channelRef.getId()+" strType: "+strType);		
		
		ManagedObjectWhereClause channelClause = new ManagedObjectWhereClause();
		ManagedObjectWhereClause typeClause = new ManagedObjectWhereClause();
		
		try {
			channelClause.checkChannel(ObjectQueryOp.EQUAL,  channelRef);
			
			ObjectType objType = ContentUtil.getObjectTypeByName(strType);
			if (logger.isDebugEnabled())
				logger.debug("| ChannelOpsDpmImpl.findContentInstancesByChannel | TRACE --> objTypeId: "+objType.getId().getId());
						
			ObjectTypeRef objTypeRef = new ObjectTypeRef(objType.getId());
			typeClause.checkObjectTypeId(ObjectQueryOp.EQUAL, objTypeRef);			
			channelClause.addWhereClause(typeClause);
			
			ManagedObjectDBQuery moDBQuery = new ManagedObjectDBQuery();					
			moDBQuery.setWhereClause(channelClause);						
						
			return DpmQueryManager.getInstance().execute(moDBQuery).asList();
			
		} catch (ApplicationException e) {
			logger.error("| ChannelOpsDpmImpl.findContentInstancesByChannel | APPLICATION_EXCEPTION --> "+e.getMessage());
			e.printStackTrace();
		} catch (ValidationException e) {
			logger.error("| ChannelOpsDpmImpl.findContentInstancesByChannel | VALIDATION_EXCEPTION --> "+e.getMessage());
			e.printStackTrace();
		} 
		return null;					
	}

	/**
	 * Indica el nombre del canal
	 * @param channelRef ChannelRef del canal
	 * @return String con el nombre del canal
	 */
	public String getChannelName(ChannelRef channelRef) {

		Channel channel = null;
		String returnValue = null;
		try {
			channel = (Channel) ContentUtil.getManagedObject(channelRef);
			if (channel != null)
				returnValue = getChannelName(channel);
			
		} catch (ApplicationException e) {
			logger.error("| ChannelOpsDpmImpl.getChannelName | APPLICATION_EXCEPTION --> msg: "+e.getMessage());
			e.printStackTrace();
			return returnValue;
		}
		return returnValue;
	}
	
	/**
	 * Indica el canal
	 * @param moVcmRef ManagedObjectVCMRef
	 * @return Channel con el resultado
	 */
	public Channel getChannelFromManagedObjectVcmRef(ManagedObjectVCMRef moVcmRef) {
		
		try {
			return (Channel) ContentUtil.getManagedObject(moVcmRef);
		} catch (ApplicationException e) {
			logger.error("| ChannelOpsDpmImpl.getChannelFromManagedObjectVcmRef | APPLICATION_EXCEPTION --> msg: "+e.getMessage());
			e.printStackTrace();
		} 
		return null;		
	}
	
	/**
	 * Indica el canal
	 * @param channelRef ChannelRef al canal
	 * @return Channel con el resultado
	 */
	public Channel getChannelFromChannelRef(ChannelRef channelRef) {
		try {
			return (Channel) ContentUtil.getManagedObject(channelRef);
		} catch (ApplicationException e) {
			logger.error("| ChannelOpsDpmImpl.getChannelFromChannelRef | APPLICATION_EXCEPTION --> msg: "+e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see es.ieci.micinn.common.util.cms.cdaimpl.ChannelOpsCdaImpl#getObjectTypeByName(java.lang.String)
	 */
	protected ObjectType getObjectTypeByName(String typeName) throws ApplicationException, ValidationException {
		
		if (logger.isDebugEnabled())
			logger.debug("| ChannelOpsDpmImpl.getObjectTypeByName | TRACE --> typeName: "+typeName);
		return ContentUtil.getObjectTypeByName(typeName);
	}

	/* (non-Javadoc)
	 * @see es.ieci.micinn.common.util.cms.IChannelOps#isChannel(com.vignette.as.client.common.ref.ManagedObjectRef)
	 */
	public boolean isChannel(ManagedObjectRef moRef) {
		
		try {
			ManagedObject mo = ContentUtil.getManagedObject(moRef);
			if (mo == null) {
				logger.error("| ChannelOpsDpmImpl.isChannel | ERROR --> mo==null ASSERT_FAILED");
				return false;
			} else 
				return mo.getObjectTypeRef().getId().equals(Channel.getTypeObjectTypeRef().getId());
			
		} catch (ApplicationException e) {
			if (logger.isDebugEnabled())
				logger.debug("| ChannelOpsDpmImpl.isChannel | APPLICATION_EXCEPTION --> msg: "+e.getMessage());
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see es.ieci.micinn.common.util.cms.IChannelOps#isChannel(com.vignette.as.client.common.ref.ManagedObjectVCMRef)
	 */
	public boolean isChannel(ManagedObjectVCMRef moVcmRef) {

	
		try {	
			ManagedObject mo = ContentUtil.getManagedObject(moVcmRef);
			if (mo == null) {
				logger.error("| ChannelOpsDpmImpl.isChannel | ERROR --> mo==null ASSERT_FAILED moVcmRef: "+moVcmRef.getId());
				return false;
			} else			
				return mo.getObjectTypeRef().getId().equals(Channel.getTypeObjectTypeRef().getId());
		
		} catch (ApplicationException e) {
			if (logger.isDebugEnabled())
				logger.debug("| ChannelOpsDpmImpl.isChannel | APPLICATION_EXCEPTION --> msg: "+e.getMessage());
		}
		
		return false;
	}
	
	
}
