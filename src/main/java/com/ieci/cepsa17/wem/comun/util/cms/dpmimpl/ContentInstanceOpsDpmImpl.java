package com.ieci.cepsa17.wem.comun.util.cms.dpmimpl;

import java.util.Date;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ieci.cepsa17.wem.comun.util.cms.IContentInstanceOps;
import com.ieci.cepsa17.wem.comun.util.cms.cdaimpl.ContentInstanceOpsCdaImpl;
import com.vignette.as.client.common.ContentInstanceDBQuery;
import com.vignette.as.client.common.ContentInstanceWhereClause;
import com.vignette.as.client.common.ref.ContentTypeRef;
import com.vignette.as.client.common.ref.ManagedObjectRef;
import com.vignette.as.client.common.ref.ManagedObjectVCMRef;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.exception.ValidationException;
import com.vignette.as.client.javabean.ContentType;
import com.vignette.as.client.javabean.IPagingList;
import com.vignette.as.client.javabean.ManagedObject;
import com.vignette.ext.templating.util.ContentUtil;
import com.vignette.util.DateQueryOp;




/**
 * Implementacion de las operaciones sobre instancias de contenidos basada en el API CDS.
 * <p>
 * Utiliza los servicios de Dynamic Portal, fundamentalmente la ObjectCache.
 * 
 * @author Fernando Salazar
 *
 */
public class ContentInstanceOpsDpmImpl extends ContentInstanceOpsCdaImpl {
	
	/** Log */
	private static final Log logger = LogFactory.getLog(ContentInstanceOpsDpmImpl.class);
	
	/**
	 * Recupera un java.util.List con las instancias de contenido del tipo indicado (nombre XML) que hayan sido
	 * modificadas entre las fechas startDate y endDate.
	 * <p>
	 * NOTA IMPORTANTE: solo funciona en el stage de Management
	 * 
	 * @param strType		nombre XML del tipo de contenido
	 * @param startDate		fecha inicial. Si startDate==null startDate se establece a 1 de enero de 1970
	 * @param endDate		fecha final. Si endDate==null endDate se establece a NOW
	 * @return				List con los content instance correspondientes
	 */
	public List findContentInstancesByModTime(String strType, Date startDate, Date endDate) {
		
		if (startDate == null) {
			startDate = IContentInstanceOps.THE_FIRST_DAY.getTime();
			if (logger.isDebugEnabled())
				logger.debug("| ContentInstanceOpsDpmImpl.findContentInstancesByModTime | TRACE --> startDate IS_NULL. startDate: "+startDate);
		}
		
		if (endDate == null) {
			endDate = new Date();
			logger.debug("| ContentInstanceOpsDpmImpl.findContentInstancesByModTime | TRACE --> endDate IS_NULL. endDate: "+endDate);			
		}
		
		
		if (logger.isDebugEnabled())
			logger.debug("| ContentInstanceOpsDpmImpl.findContentInstancesByModTime | TRACE --> METHOD_BEGIN: strType: "+strType+ 
					", startDate: "+startDate+", endDate: "+ endDate);
		
		try {
			ContentInstanceWhereClause startDateClause = new ContentInstanceWhereClause();
			startDateClause.checkLastModTime(DateQueryOp.EQUAL_OR_AFTER, startDate);
			ContentInstanceWhereClause endDateClause = new ContentInstanceWhereClause();
			endDateClause.checkLastModTime(DateQueryOp.EQUAL_OR_BEFORE, endDate);
			startDateClause.addWhereClause(endDateClause);
			ContentType ciType = (ContentType)ContentType.findByName(strType);
			
			ContentInstanceDBQuery query = new ContentInstanceDBQuery(new ContentTypeRef(ciType.getId()));					
			query.setWhereClause(startDateClause);	
			
			IPagingList results = DpmQueryManager.getInstance().execute(query);
				
			
			if (logger.isDebugEnabled()) {
				logger.debug("| ContentInstanceOpsDpmImpl.findContentInstancesByModTime | ASSERT --> results!=null: "+(results!= null ? "ASSERT_TRUE": "ASSERT_FAILED"));
				if (results != null)
					logger.debug("| ContentInstanceOpsDpmImpl.findContentInstancesByModTime | TRACE --> results.size(): "+results.size());
			}
			
			return results.asList();				
		} catch (ApplicationException e) {
			logger.error("| ContentInstanceOpsDpmImpl.findContentInstancesByModTime | APPLICATION_EXCEPTION --> "+e.getMessage());
			e.printStackTrace();
		} catch (ValidationException e) {
			logger.error("| findContentInstancesByModTime | VALIDATION_EXCEPTION --> "+e.getMessage());
			e.printStackTrace();
		} 
		return null;			
	}

	/* (non-Javadoc)
	 * @see es.ieci.micinn.common.util.cms.IContentInstanceOps#getManagedObject(com.vignette.as.client.common.ref.ManagedObjectRef)
	 */
	public ManagedObject getManagedObject(ManagedObjectRef moRef) throws ApplicationException {
		return ContentUtil.getManagedObject(moRef);
	}

	/* (non-Javadoc)
	 * @see es.ieci.micinn.common.util.cms.IContentInstanceOps#getManagedObject(com.vignette.as.client.common.ref.ManagedObjectVCMRef)
	 */
	public ManagedObject getManagedObject(ManagedObjectVCMRef vcmRef) throws ApplicationException {
		return ContentUtil.getManagedObject(vcmRef);
	}

}
