package com.ieci.cepsa17.wem.comun.util.cms;

import com.ieci.cepsa17.wem.comun.util.cms.dpmimpl.ChannelOpsDpmImpl;
import com.ieci.cepsa17.wem.comun.util.cms.dpmimpl.ContentInstanceOpsDpmImpl;

/**
 * Factoria de clases para el acceso al API
 * @author Fernando Salazar
 *
 */
public class CmsOpsFactory {

	private static CmsOpsFactory instance = null;
	private IChannelOps channelOps = null;
	private IContentInstanceOps contentInstanceOps = null;
	
	/**
	 * Constructor
	 * 
	 */
	private CmsOpsFactory() {
		//TODO Estos enlaces deber�a gestionarlos un contenedor Ioc
		channelOps = new ChannelOpsDpmImpl();
		contentInstanceOps = new ContentInstanceOpsDpmImpl();
		//channelOps = new ChannelOpsCdaImpl();
		//contentInstanceOps = new ContentInstanceOpsCdaImpl();
	}

	/**
	 * Obtenemos una instancia de la factoria
	 * @return CmsOpsFactory instancia
	 */
	public synchronized static CmsOpsFactory getInstance() {
		
		if (instance == null)
			instance = new CmsOpsFactory();
		return instance;		
	}
	
	
	/**
	 * Crea factoria para el acceso a VCM , para realizar operaciones sobre canales.
	 * @return IChannel con los canales
	 */
	public IChannelOps createChannelOps() {
		return channelOps;
	}
	
	/**
	 * Crea factoria para el acceso a VCM , para realizar operaciones con las instancias de los
	 * contenidos
	 * @return IContentInstanceOps con las instancias de los contenidos
	 */
	public IContentInstanceOps createContentInstanceOps() {
		return contentInstanceOps;
	}
	
}
