package com.ieci.cepsa17.wem.comun.util.cms;

import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletRequest;

import com.vignette.as.client.common.ref.ChannelRef;
import com.vignette.as.client.common.ref.ManagedObjectRef;
import com.vignette.as.client.common.ref.ManagedObjectVCMRef;
import com.vignette.as.client.javabean.Channel;

/**
 * API de acceso a VCM para realizar operaciones sobre canales.
 * 
 * @author Fernando Salazar
 *
 */
public interface IChannelOps extends IManagedObjectOps {
	
	public static final String FIELD_SEPARATOR = ",";
	public static final String CONTENTMANAGEMENTID_ATTR_NAME = "VCM_CONTENTMANAGEMENTID";
	public static String VGNEXTOID_PARAM = "vgnextoid";
	public static String VGNEXTCHANNEL_PARAM = "vgnextchannel";		
	public static final GregorianCalendar THE_FIRST_DAY = new GregorianCalendar(1970, 0, 1);

	
	/**
	 * Cuenta el numero de instancias (tipo <code>ContentInstance</code>) de el canal
	 * indicado como parametro.
	 * 
	 * @param channelRef ChannelRef con el canal
	 * @return int con el n�mero de intancias 
	 */
	public int countChannelContentInstances(ChannelRef channelRef);
	
	/**
	 * Cuenta el numero de instancias de tipo strType del canal indicado como parametro.
	 * 
	 * @param channelRef ChannelRef con el canal
	 * @param strType String con el nombre del tipo
	 * @return int con el n�mero de intancias 
	 */
	public int countChannelContentInstances(ChannelRef channelRef, String strType);	
	
	/**
	 * Recupera las <code>ContentInstance</code> del canal indicado como parametro.
	 * 
	 * @param channelRef ChannelRef con el canal
	 * @return List con los resultados
	 */
	public List findContentInstancesByChannel(ChannelRef channelRef);
	
	/**
	 * Localiza las instancias de contenido del tipo indicado en <code>strType</code> asociadas
	 * al canal indicado en <code>channelRef</code>. Devuelve una lista de ManagedObject 
	 * cuyas instancias almacenan los campos indicados en <code>attrList</code> siendo el
	 * valor de esos campos el que ten&iacute;an las instancias localizadas.
	 * 
	 * @param channelRef	referencia al canal sobre el que se busca @see com.vignette.as.client.common.ref.ChannelRef
	 * @param strType		nombre XML (<code>String</code> del tipo de datos sobre el que se busca 
	 * @param attrList		lista de atributos a pedir a las instancias localizadas (<code>String</code> de valores separados por coma)
	 * @return				lista (<code>java.util.List</code> de instancias <code>ManagedObject</code> 
	 */
	public List findContentInstancesByChannel(ChannelRef channelRef, String strType, String attrList);	
	
	/**
	 * Localiza las instancias de contenido del tipo indicado en <code>strType</code> asociadas
	 * al canal indicado en <code>channelRef</code>. Devuelve una lista de ManagedObject 
	 * que se corresponden con los ContentInstance.
	 * 
	 * @param channelRef	referencia al canal sobre el que se busca @see com.vignette.as.client.common.ref.ChannelRef
	 * @param strType		nombre XML (<code>String</code> del tipo de datos sobre el que se busca 
	 * @param attrList		lista de atributos a pedir a las instancias localizadas (<code>String</code> de valores separados por coma)
	 * @return				lista (<code>java.util.List</code> de instancias <code>ManagedObject</code> 
	 */	
	public List findContentInstancesByChannel(ChannelRef channelRef, String strType);	

	/**
	 * Indica el nombre del canal
	 * @param channelRef ChannelRef del canal
	 * @return String con el nombre del canal
	 */
	public String getChannelName(ChannelRef channelRef);
	
	/**
	 * Indica el nombre del canal
	 * @param channel Channel del canal
	 * @return String con el nombre del canal
	 */
	public String getChannelName(Channel channel);
	
	/**
	 * Indica el canal de un <code>ManagedObject</code>
	 * @param moVcmRef ManagedObjectVCMRef 
	 * @return Channel del <code>ManagedObject</code>
	 */
	public Channel getChannelFromManagedObjectVcmRef(ManagedObjectVCMRef moVcmRef);
	
	/**
	 * Indica el canal
	 * @param channelRef ChannelRef
	 * @return Channel al que pertenece
	 */ 
	public Channel getChannelFromChannelRef(ChannelRef channelRef);
	
	/** Indica el canal al que pertenece un <code>vcmId</code>
	 * @param vcmId String con el identificador
	 * @return Channel al que pertenece
	 */
	public Channel getChannelFromVcmId(String vcmId);
	
	/**
	 * Indica el canal al que pertence un Id
	 * @param stringId String con el identificador
	 * @return Channel al que pertenece
	 */
	public Channel getChannelFromStringId(String stringId);
	
	/**
	 * Indica el canal al que pertence un Id
	 * @param stringId String con el identificador
	 * @return ChannelRef al que pertenece
	 */
	public ChannelRef getChannelRefFromStringId(String stringId);
	
	/**
	 * Indica el canal al que pertenece un <code>vcmId</code>
	 * @param vcmId String con el identificador
	 * @return ChannelRef al que pertenece
	 */
	public ChannelRef getChannelRefFromVcmId(String vcmId);
	
	/**
	 * Indica la referencia de un canal
	 * @param channel Channel con el canal
	 * @return ChannelRef al que pertenece
	 */
	public ChannelRef getChannelRefFromChannel(Channel channel);
	
	
	/**
	 * Indica si es o no un canal
	 * @param moVcmRef ManagedObjectVCMRef de la instancia de contenido
	 * @return boolean indicando si es � no un canal
	 */
	public boolean isChannel(ManagedObjectVCMRef moVcmRef);
	
	/**
	 * Indica si es una canal
	 * @param moRef ManagedObjectRef de la instancia de contenido
	 * @return boolean que indica si es un canal
	 */
	public boolean isChannel(ManagedObjectRef moRef);
	
	/**
	 * Adivina que parametro representa el canal en la request.
	 * <p>
	 * Como su nombre indica deberia usarse como ultima opcion. 
	 * 
	 * @param request	ServletRequest
	 * @return			Channel
	 */
	public Channel guessChannelFromRequest(ServletRequest request);
}

