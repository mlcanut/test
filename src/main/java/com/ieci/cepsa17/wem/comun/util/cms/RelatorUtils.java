package com.ieci.cepsa17.wem.comun.util.cms;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vignette.as.client.common.ContentInstanceDBQuery;
import com.vignette.as.client.common.ContentInstanceWhereClause;
import com.vignette.as.client.common.ref.ContentTypeRef;
import com.vignette.as.client.common.ref.ManagedObjectVCMRef;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.exception.ValidationException;
import com.vignette.as.client.javabean.ContentInstance;
import com.vignette.as.client.javabean.ContentType;
import com.vignette.as.client.javabean.IPagingList;
import com.vignette.as.client.javabean.ManagedObject;
import com.vignette.as.client.javabean.QueryManager;
//import com.vignette.logging.Category;
import com.vignette.util.NumberQueryOp;

//import es.ieci.micinn.vgnt.utilities.common.i18n.I18nUtils;


/**
 * Clase de utilidad
 * @author Fer, Daniel
 * @version 1.5
 */
public class RelatorUtils extends CMSUtils {

	private static final String SEARCH_FIELD = "ID";
	//private static Category logger = Category.getInstance(RelatorUtils.class);
	private static final Log logger = LogFactory.getLog(RelatorUtils.class);
	
	private static boolean isEmpty(String param) {
		return param == null || param.length() == 0;
	}
	
	/**
	 * Obtiene el ContentInstance relacionado a partir de su clave primaria en base de datos.
	 * y el nombre del tipo de datos.
	 * @param dbId clave primaria de la base de datos
	 * @param ctypeName cadena con el nombre del tipo 
	 * @return el ContentInstance.
	 */	
	public static ContentInstance getRelatedCIByGUID(String id, String ctypeName) {
		
		if (logger.isDebugEnabled()) 
			logger.debug("| RelatorUtils.getRelatedCIByGUID | TRACE --> id="+id+", ctypeName="+ctypeName);
		if (id != null)
			return getRelatedCIByGUIDNoCached (id, ctypeName);
		
		return null;
	
	}
	
	/** Este m�todo recupera un ContentInstance dado su identificador.
	 * 
	 * @param vcmId identificador VCM del ContentInstance del que se quiere sacar la instancia.
	 * 
	 * @return ContentInstance La instancia del objeto.
	 * */
	public static ContentInstance getRelatedCIByVCMID(String vcmId) {
		
		if (logger.isDebugEnabled()) 
			logger.debug("| RelatorUtils.getRelatedCIByVCMID | TRACE -->  vcmId="+vcmId);
		if (vcmId != null)
			try{
				return (ContentInstance) getManagedObjectNoCached(getManagedObjectVCMRefObjectNoCached(vcmId));
			}catch (Exception e){
				logger.error ("El id del contenido no existe: " + vcmId);
			}
		
		return null;
	
	}
	
	/** Este m�todo recupera una objeto dado su identificador.
	 * 
	 * @param moVCMRef identificador del objeto del que se quiere sacar la instancia.
	 * @return ManagedObject la instancia del objeto.
	 * */
	protected static ManagedObject getManagedObjectNoCached(ManagedObjectVCMRef moVCMRef)
	{		
		if (logger.isDebugEnabled()) 
			logger.debug("| RelatorUtils.getManagedObjecttNoCached | TRACE --> Creaci�n de un ManagedObject");		
		ManagedObject contenidoActual = null;
		
		try {
			contenidoActual = moVCMRef.retrieveManagedObject();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(null,e);
		}
		String name = null;
		if (contenidoActual!= null)
			try {
				name = contenidoActual.getName();
			} catch (ApplicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		if (logger.isDebugEnabled()) 
			logger.debug("| RelatorUtils.getManagedObjecttNoCached |TRACE --> Return: " + name);		
		return contenidoActual;
	}
	
	
	
	/**
	 * Obtiene el ContentInstance relacionado a partir de su clave primaria en base de datos, sin optimizar con cach�.
	 * y el nombre del tipo de datos.
	 * @param dbId clave primaria de la base de datos
	 * @param ctypeName cadena con el nombre del tipo 
	 * @return el ContentInstance.
	 */
	private static ContentInstance getRelatedCIByGUIDNoCached(String dbId, String ctypeName) {

		if (logger.isDebugEnabled()) 
			logger.debug("| RelatorUtils.getRelatedCIByGUID |TRACE -->  dbId="+dbId+", ctypeName="+ctypeName);
		
		if (isEmpty(dbId) || isEmpty(ctypeName)) return null;
		
		
		ContentInstanceWhereClause idClause = new ContentInstanceWhereClause();
		idClause.checkAttribute(SEARCH_FIELD, NumberQueryOp.EQUAL, dbId);
				
		try {
			ContentType ctype = (ContentType) ContentType.findByName(ctypeName);
			ContentInstanceDBQuery query = new ContentInstanceDBQuery(new ContentTypeRef(ctype.getId()));
			query.setWhereClause(idClause);
						
			IPagingList results = QueryManager.execute(query);
			
			ContentInstance valor = (ContentInstance) (results.size() <= 0 ? null : results.get(0));
			return valor;
			
		} catch (ApplicationException e) {
			if (logger.isDebugEnabled()) 
				logger.debug("| RelatorUtils.getRelatedCIByGUIDNoCached1 | TRACE --> Error: Excepcion");
			logger.error(e.getMessage(), e);
		} catch (ValidationException e) {
			if (logger.isDebugEnabled()) 
				logger.debug("| RelatorUtils.getRelatedCIByGUIDNoCached2 | TRACE --> Error: Excepcion");
			logger.error(e.getMessage());		
		}
		return null;	
	}

	
}