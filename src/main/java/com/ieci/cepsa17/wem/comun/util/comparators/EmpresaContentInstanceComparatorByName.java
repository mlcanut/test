package com.ieci.cepsa17.wem.comun.util.comparators;

import java.util.Comparator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.javabean.ContentInstance;

public class EmpresaContentInstanceComparatorByName implements Comparator<ContentInstance> {

	private static final Log logger = LogFactory.getLog(EmpresaContentInstanceComparatorByName.class);

	public int compare(ContentInstance o1, ContentInstance o2) {
		String nombre1 = "";
		try {
			nombre1 = (String)o1.getAttributeValue("NOMBRE");
		} catch (ApplicationException e) {
			logger.error("Error Error recuperando atributo NOMBRE en: " + o1, e);
		}
		String nombre2 = "";
		try {
			nombre2 = (String)o2.getAttributeValue("NOMBRE");
		} catch (ApplicationException e) {
			logger.error("Error Error recuperando atributo NOMBRE en: " + o1, e);
		}
		return nombre1.compareToIgnoreCase(nombre2);
	}
}
