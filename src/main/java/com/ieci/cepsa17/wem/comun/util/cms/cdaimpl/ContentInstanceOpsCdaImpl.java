package com.ieci.cepsa17.wem.comun.util.cms.cdaimpl;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ieci.cepsa17.util.DateUtils;
import com.ieci.cepsa17.wem.comun.util.cms.IContentInstanceOps;
import com.vignette.as.client.common.ContentInstanceDBQuery;
import com.vignette.as.client.common.ContentInstanceWhereClause;
import com.vignette.as.client.common.SearchFilter;
import com.vignette.as.client.common.SearchMgmtAttr;
import com.vignette.as.client.common.SearchQueryData;
import com.vignette.as.client.common.ref.ContentTypeRef;
import com.vignette.as.client.common.ref.ManagedObjectRef;
import com.vignette.as.client.common.ref.ManagedObjectVCMRef;
import com.vignette.as.client.common.types.SearchTypeEnum;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.exception.ValidationException;
import com.vignette.as.client.javabean.ContentType;
import com.vignette.as.client.javabean.IPagingList;
import com.vignette.as.client.javabean.ManagedObject;
import com.vignette.as.client.javabean.QueryManager;
import com.vignette.as.client.javabean.SearchQuery;
import com.vignette.util.DateQueryOp;



/**
 * Implementacion de las operaciones sobre instancias de contenido basada en el
 * API CDS.
 * 
 * @author Fernando Salazar
 * 
 */
public class ContentInstanceOpsCdaImpl extends ManagedObjectOpsCdaImpl
		implements IContentInstanceOps {

	/**
	 * Logger
	 */
	private static Log logger = LogFactory.getLog(ContentInstanceOpsCdaImpl.class);

	public ContentInstanceOpsCdaImpl() {
		
	}
	/**
	 * Recupera un java.util.List con las instancias de contenido del tipo
	 * indicado (nombre XML) que hayan sido modificadas entre las fechas
	 * startDate y endDate.
	 * <p>
	 * NOTA IMPORTANTE: esta funcion utiliza el API de SearchQuery en lugar del
	 * API de ManagedObjectDBQuery
	 * 
	 * @param strType
	 * @param startDate
	 * @param endDate
	 * @param fullDays
	 * @return
	 */
	public List searchContentInstancesByModTime(String strType, Date startDate,
			Date endDate, boolean fullDays) {

		if (startDate == null) {
			startDate = IContentInstanceOps.THE_FIRST_DAY.getTime();
			if (logger.isDebugEnabled())
				logger
						.debug("| ContentInstanceOpsCdaImpl.searchContentInstancesByModTime | TRACE --> startDate IS_NULL. startDate: "
								+ startDate);
		}

		if (endDate == null) {
			endDate = new Date();
			logger
					.debug("| ContentInstanceOpsCdaImpl.searchContentInstancesByModTime | TRACE --> endDate IS_NULL. endDate: "
							+ endDate);
		}

		if (fullDays) {
			startDate = DateUtils.getDayBeginning(startDate);
			endDate = DateUtils.getDayEnding(endDate);
		}

		if (logger.isDebugEnabled())
			logger
					.debug("| ContentInstanceOpsCdaImpl.searchContentInstancesByModTime | TRACE --> METHOD_BEGIN: strType: "
							+ strType
							+ ", startDate: "
							+ startDate
							+ ", endDate: " + endDate);

		SearchQuery searchQuery = new SearchQuery();
		SearchQueryData searchQueryData = searchQuery.getData();
		searchQueryData.setSearchType(SearchTypeEnum.CONTENT);
		searchQueryData.setObjectType(strType);
		searchQueryData.setQueryText("*");

		SearchFilter dateRangeFilter = new SearchFilter();
		dateRangeFilter.setAttr(SearchMgmtAttr.LAST_MOD_TIME);
		dateRangeFilter.setValue1(startDate);
		dateRangeFilter.setValue2(endDate);
		searchQueryData.addFilter(dateRangeFilter);
		try {
			IPagingList list = searchQuery.execute();
			return list.asList();
		} catch (ApplicationException e) {
			logger
					.error("| ContentInstanceOpsCdaImpl.searchContentInstancesByModTime | APPLICATION_EXCEPTION --> msg: "
							+ e.getMessage());
			e.printStackTrace();
		} catch (ValidationException e) {
			logger
					.error("| ContentInstanceOpsCdaImpl.searchContentInstancesByModTime | VALIDATION_EXCEPTION --> msg: "
							+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Recupera un java.util.List con las instancias de contenido del tipo
	 * indicado (nombre XML) que hayan sido modificadas entre las fechas
	 * startDate y endDate.
	 * <p>
	 * NOTA IMPORTANTE: solo funciona en el stage de Management
	 * 
	 * @param strType
	 *            nombre XML del tipo de contenido
	 * @param startDate
	 *            fecha inicial. Si startDate==null startDate se establece a 1
	 *            de enero de 1970
	 * @param endDate
	 *            fecha final. Si endDate==null endDate se establece a NOW
	 * @param fullDays
	 * @return List con los content instance correspondientes
	 */
	public List findContentInstancesByModTime(String strType, Date startDate,
			Date endDate, boolean fullDays) {

		if (startDate == null) {
			startDate = IContentInstanceOps.THE_FIRST_DAY.getTime();
			if (logger.isDebugEnabled())
				logger
						.debug("| ContentInstanceOpsCdaImpl.findContentInstancesByModTime | TRACE --> startDate IS_NULL. startDate: "
								+ startDate);
		}

		if (endDate == null) {
			endDate = new Date();
			logger
					.debug("| ContentInstanceOpsCdaImpl.findContentInstancesByModTime | TRACE --> endDate IS_NULL. endDate: "
							+ endDate);
		}

		if (fullDays) {
			startDate = DateUtils.getDayBeginning(startDate);
			endDate = DateUtils.getDayEnding(endDate);
		}

		if (logger.isDebugEnabled())
			logger
					.debug("| ContentInstanceOpsCdaImpl.findContentInstancesByModTime | TRACE --> METHOD_BEGIN: strType: "
							+ strType
							+ ", startDate: "
							+ startDate
							+ ", endDate: " + endDate);

		try {
			ContentInstanceWhereClause startDateClause = new ContentInstanceWhereClause();
			startDateClause.checkLastModTime(DateQueryOp.EQUAL_OR_AFTER,
					startDate);
			ContentInstanceWhereClause endDateClause = new ContentInstanceWhereClause();
			endDateClause
					.checkLastModTime(DateQueryOp.EQUAL_OR_BEFORE, endDate);
			startDateClause.addWhereClause(endDateClause);
			ContentType ciType = (ContentType) ContentType.findByName(strType);

			ContentInstanceDBQuery query = new ContentInstanceDBQuery(
					new ContentTypeRef(ciType.getId()));
			query.setWhereClause(startDateClause);
			IPagingList results = QueryManager.execute(query);

			if (logger.isDebugEnabled()) {
				logger
						.debug("| ContentInstanceOpsCdaImpl.findContentInstancesByModTime | ASSERT --> results!=null: "
								+ (results != null ? "ASSERT_TRUE"
										: "ASSERT_FAILED"));
				if (results != null)
					logger
							.debug("| ContentInstanceOpsCdaImpl.findContentInstancesByModTime | TRACE --> results.size(): "
									+ results.size());
			}

			return results.asList();
		} catch (ApplicationException e) {
			logger
					.error("| ContentInstanceOpsCdaImpl.findContentInstancesByModTime | APPLICATION_EXCEPTION --> "
							+ e.getMessage());
			e.printStackTrace();
		} catch (ValidationException e) {
			logger
					.error("| ContentInstanceOpsCdaImpl.findContentInstancesByModTime | VALIDATION_EXCEPTION --> "
							+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.ieci.micinn.common.util.cms.IContentInstanceOps#getManagedObject(com.vignette.as.client.common.ref.ManagedObjectRef)
	 */
	public ManagedObject getManagedObject(ManagedObjectRef moRef)
			throws ApplicationException {
		return moRef.retrieveManagedObject();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.ieci.micinn.common.util.cms.IContentInstanceOps#getManagedObject(com.vignette.as.client.common.ref.ManagedObjectVCMRef)
	 */
	public ManagedObject getManagedObject(ManagedObjectVCMRef vcmRef)
			throws ApplicationException {

		try {
			return vcmRef.retrieveManagedObject();
		} catch (RemoteException e) {
			logger.error("| ContentInstanceOpsCdaImpl.getManagedObject | REMOTE_EXCEPTION --> "
					+ e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
}
