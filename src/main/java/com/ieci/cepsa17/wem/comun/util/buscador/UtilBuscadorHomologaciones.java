package com.ieci.cepsa17.wem.comun.util.buscador;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.ieci.cepsa17.wem.comun.util.CIUtil;
import com.ieci.cepsa17.wem.constants.BuscadorHomologacionesConstants;
import com.vignette.as.client.api.bean.ManagedObjectBean;
import com.vignette.as.client.common.ContentInstanceWhereClause;
import com.vignette.as.client.javabean.ContentInstance;
import com.vignette.util.StringQueryOp;

import es.ieci.cepsa.utilities.common.cms.CMSUtils;

public class UtilBuscadorHomologaciones {

	protected static Logger logger = Logger.getLogger(UtilBuscadorHomologaciones.class);

	private UtilBuscadorHomologaciones() {
		super();
	}

	public static List<ManagedObjectBean> getHomologaciones(String reference) {

		if (logger.isDebugEnabled()) {
			logger.debug("UtilBuscadorHomologaciones -- getHomologaciones");
		}

		try {

			ArrayList<ContentInstanceWhereClause> query = new ArrayList<>();
			ContentInstanceWhereClause clause = new ContentInstanceWhereClause();
			clause.setMatchAny(true);

			List<String> combinations = new ArrayList<>();
			if (isNumeric(reference)) {
				combinations.add(reference);
			} else {
				combinations = obtainCombinations(reference);
			}

			/* Datos propios de la homologacion */
			for (int i = 0; i < combinations.size(); i++) {
				String currentComb = combinations.get(i);
				clause.checkAttribute(BuscadorHomologacionesConstants.HOMOLOGACION_ALIAS, StringQueryOp.CONTAINS,
						currentComb);
				clause.checkAttribute(BuscadorHomologacionesConstants.HOMOLOGACION_CODIGO_HOM, StringQueryOp.CONTAINS,
						currentComb);
			}

			/* Código y nombre producto */
			String[] fields = new String[2];
			fields[0] = BuscadorHomologacionesConstants.PRODUCTO_CODIGO;
			fields[1] = BuscadorHomologacionesConstants.PRODUCTO_NOMBRE;
			addClauses(clause, reference, combinations, fields, BuscadorHomologacionesConstants.CT_PRODUCTO,
					BuscadorHomologacionesConstants.PRODUCTO_CODIGO,
					BuscadorHomologacionesConstants.HOMOLOGACION_PRODUCTO_ID);

			/* Fabricante/Organismo homologador */
			fields = new String[1];
			fields[0] = BuscadorHomologacionesConstants.CATEGORIA_ALIAS;
			addClauses(clause, reference, combinations, fields, BuscadorHomologacionesConstants.CT_CATEGORIA,
					BuscadorHomologacionesConstants.CATEGORIA_ALIAS,
					BuscadorHomologacionesConstants.HOMOLOGACION_HOMOLOGADOR);

			query.add(clause);

			/* Ejecuta la query */
			ContentInstance[] resultado = CMSUtils.getContentInstancesByQuery(
					BuscadorHomologacionesConstants.CT_HOMOLOGACION, query,
					BuscadorHomologacionesConstants.HOMOLOGACION_ALIAS);
			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorHomologaciones -- Con el termino " + reference + ", recuperados "
						+ resultado.length + " resultados.");
			}

			return CIUtil.makeBeans(resultado);

		} catch (Exception e) {
			logger.error(
					"UtilBuscadorHomologaciones -- Se ha producido un error en la busqueda de homologaciones por el termino "
							+ reference + ": " + e.getMessage(),
					e);
		}

		return new ArrayList<>();

	}

	private static void addClauses(ContentInstanceWhereClause clause, String text, List<String> combinations,
			String[] fields, String subct, String subctOrderBy, String joinBy) throws Exception {

		try {

			ArrayList<ContentInstanceWhereClause> subquery = new ArrayList<>();
			ContentInstanceWhereClause subclause = new ContentInstanceWhereClause();
			subclause.setMatchAny(true);

			for (int j = 0; j < combinations.size(); j++) {
				String currentComb = combinations.get(j);
				for (int f = 0; f < fields.length; f++) {
					subclause.checkAttribute(fields[f], StringQueryOp.CONTAINS, currentComb);
				}
				subquery.add(subclause);
			}

			ContentInstance[] subresultado = CMSUtils.getContentInstancesByQuery(subct, subquery, subctOrderBy);
			for (int rp = 0; rp < subresultado.length; rp++) {
				ContentInstance ci = subresultado[rp];
				clause.checkAttribute(joinBy, StringQueryOp.EQUAL, ci.getContentManagementId().toString());
			}

			if (logger.isDebugEnabled()) {
				logger.debug("UtilBuscadorHomologaciones -- Con el termino " + text + ", recuperados "
						+ subresultado.length + " resultados de la tabla " + subct + ".");
			}

		} catch (Exception e) {
			throw e;
		}

	}

	private static List<String> obtainCombinations(String text) {

		List<String> combinations = new ArrayList<>();

		combinations.add(text.toUpperCase());
		combinations.add(text.toLowerCase());
		combinations.add(text.substring(0, 1).toUpperCase() + text.substring(1, text.length()).toLowerCase());

		StringBuilder auxText = new StringBuilder();
		StringTokenizer strtok = new StringTokenizer(text, " ");
		while (strtok.hasMoreTokens()) {
			String word = strtok.nextToken();
			auxText.append(word.substring(0, 1).toUpperCase() + word.substring(1, word.length()).toLowerCase());
		}

		return combinations;

	}

	private static Boolean isNumeric(String text) {

		try {
			double d = Double.parseDouble(text);
		} catch (NumberFormatException nfe) {
			return false;
		}

		return true;

	}

}