package com.ieci.cepsa17.wem.comun.util.cms.dpmimpl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.vignette.as.client.common.ManagedObjectDBQuery;
import com.vignette.as.client.common.Query;
import com.vignette.as.client.common.ref.ManagedObjectVCMRef;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.exception.ValidationException;
import com.vignette.as.client.javabean.IPagingList;
import com.vignette.as.client.javabean.ManagedObject;
import com.vignette.as.client.javabean.QueryManager;
import com.vignette.as.common.util.AdapterPagingList;
import com.vignette.ext.templating.util.ContentUtil;

/**
 * @author Fernando Salazar
 *
 */
public class DpmQueryManager {

	/** Log */
	private static final Log logger = LogFactory.getLog(DpmQueryManager.class);
	
	private static DpmQueryManager instance = null;
	
	/**
	 * Constructor
	 *
	 */
	private DpmQueryManager() {		
	}
	
	/**
	 * Obtiene una instancia
	 * @return DpmQueryManager de la instancia
	 */
	public static synchronized DpmQueryManager getInstance() {
		
		if (instance == null)
			instance = new DpmQueryManager();
		return instance;
	}
	
	/**
	 * Ejecuta una consulta
	 * @param query Query con la consulta
	 * @return IPagingList con el resultado de la consulta
	 */
	public IPagingList execute(Query query) {
		
		if (logger.isDebugEnabled())
			logger.debug("| DpmQueryManager.execute | TRACE --> query: "+query.toString());		
		
		// Precondicion
		if (! (query instanceof ManagedObjectDBQuery)) {
			logger.error("| DpmQueryManager.execute | ASSERT --> query instanceof ManagedObjectDBQuery: ASSERTION_FAILED");
			return null;
		}
		
		ManagedObjectDBQuery moDbQuery = (ManagedObjectDBQuery) query;
		moDbQuery.addReturnsContentManagementId();
		
		try {
			IPagingList lightMoList = QueryManager.execute(moDbQuery);
			
			return (IPagingList) new DpmQueryManager.ContentInstancePagingList(lightMoList);
			
		} catch (ApplicationException e) {
			logger.error("| DpmQueryManager.execute | APPLICATION_EXCEPTION --> msg: "+e.getMessage());
			e.printStackTrace();
		} catch (ValidationException e) {
			logger.error("| DpmQueryManager.execute | VALIDATION_EXCEPTION --> msg: "+e.getMessage());
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	class ContentInstancePagingList extends AdapterPagingList {

		public ContentInstancePagingList(IPagingList arg0) {
			super(arg0);
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		protected Object adapt(Object lightCi) throws ApplicationException {
			/*
			if (logger.isDebugEnabled())
				logger.debug("| DpmQueryManager.adapt | TRACE --> METHOD_BEGIN");
			
			
			
			if (logger.isDebugEnabled())
				logger.debug("| DpmQueryManager.adapt | TRACE --> lightCi: "+lightCiMO.getContentManagementId());
				*/		
			ManagedObject lightCiMO = (ManagedObject) lightCi;
			ManagedObjectVCMRef moVcmRef = new ManagedObjectVCMRef(lightCiMO);
			return ContentUtil.getManagedObject(moVcmRef);
		}
		
	}
}
