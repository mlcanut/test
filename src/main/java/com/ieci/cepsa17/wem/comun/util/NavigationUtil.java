package com.ieci.cepsa17.wem.comun.util;

import org.apache.log4j.Logger;

import com.ieci.cepsa17.wem.comun.util.cms.CMSUtils;
import com.ieci.cepsa17.wem.comun.util.cms.CmsOpsFactory;
import com.ieci.cepsa17.wem.comun.util.cms.IChannelOps;
import com.vignette.as.client.javabean.Channel;
import com.vignette.ext.templating.util.RequestContext;



public class NavigationUtil {

	private static Logger logger = Logger.getLogger(NavigationUtil.class);

	private NavigationUtil() {
		super();
	}

	public static Channel getPredecessor(RequestContext requestContext, String channelId, short levelToShow,
			short minLevelsUp) {

		Channel predecessor = null;

		try {

			int depth = CMSUtils.getCurrentChannelDepth(requestContext);
			int levelsUp = depth > levelToShow ? depth - levelToShow : minLevelsUp;

			IChannelOps channelOps = CmsOpsFactory.getInstance().createChannelOps();

			String predChannelId = channelId;

			for (int pos = 0; pos < levelsUp; pos++) {
				predecessor = channelOps.getChannelFromVcmId(predChannelId).getParentChannelRef().getChannel();
				predChannelId = predecessor.getContentManagementId().toString();
			}

			if (predecessor == null) {
				predecessor = channelOps.getChannelFromVcmId(channelId);
			}

		} catch (Exception e) {
			logger.error("NavigationUtil -- Se ha producido un error al obtener el predecesor de " + channelId + ": "
					+ e.getMessage(), e);
		}

		return predecessor;

	}

}