package com.ieci.cepsa17.wem.comun.util.cms;

public interface IManagedObjectOps {

	public static final int WRONG_TYPE = -1;
	public static final int MANAGEDOBJECTVCMREF = 1;
	public static final int MANAGEDOBJECTREF = 2;
	public static final int STATICFILE = 3;
	public static final int STRINGID = 4;
	
	/**
	 * Adivina que parametro representa el canal en la request.
	 * @param id String con el identeficado
	 * @return int con el resultado
	 */
	public int guessIdType(String id);
	
}