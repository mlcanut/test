package com.ieci.cepsa17.wem.comun.util.cms;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
//import org.apache.log4j.Category;
import com.vignette.as.client.common.ContentInstanceDBQuery;
import com.vignette.as.client.common.ContentRelationInstance;
import com.vignette.as.client.common.ManagedObjectWhereClause;
import com.vignette.as.client.common.ref.ChannelRef;
import com.vignette.as.client.common.ref.ContentTypeRef;
import com.vignette.as.client.common.ref.ManagedObjectVCMRef;
import com.vignette.as.client.common.ref.SiteRef;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.exception.ValidationException;
import com.vignette.as.client.javabean.AttributedObject;
import com.vignette.as.client.javabean.Channel;
import com.vignette.as.client.javabean.ContentInstance;
import com.vignette.as.client.javabean.ContentType;
import com.vignette.as.client.javabean.ManagedObject;
import com.vignette.ext.templating.util.RequestContext;

import com.ieci.cepsa17.wem.comun.util.cms.dpmimpl.DpmQueryManager;


/**
 * Clase de utilidades en la que se introducen numerosos m�todos para
 * el acceso y la recuperaci�n de informaci�n de VCM.
 * 
 * @author IECISA
 * @version 1.0
 */

public class CMSUtils {
	//private static final Category logger = Category.getInstance(CommonUtils.class);
	private static final Log logger = LogFactory.getLog(CMSUtils.class);
	private static final IChannelOps channelOps = CmsOpsFactory.getInstance().createChannelOps();
	public CMSUtils(){
		
	}
	
	/**
	 * 
	 * @param rc
	 * @param ci
	 * @param nombreRel
	 * @param strLang
	 * @param strField
	 * @return
	 */
	public static String getAttributeRelByLang(RequestContext rc, ContentInstance ci, String nombreRel, String strLang, String strField){
		String returnValue = "";
		ContentRelationInstance cri =null;
		String language = rc.getLocale().getLanguage();
		try {
			AttributedObject[] relacionesLang = ci.getRelations(nombreRel);
			for (int i=0; i < relacionesLang.length; i++){
				cri = (ContentRelationInstance) relacionesLang[i];
				if (language.equals(cri.getAttributeValue(strLang).toString())) {
					if (cri.getAttributeValue(strField) != null)
					returnValue = cri.getAttributeValue(strField).toString();
				}
			}
		}
		catch (ApplicationException e) {
			e.printStackTrace();
		}
		return returnValue;
	}
	
	
	/**
	 * Funci�n que recibe como par�metro la request para obtener el Idnetificador de VCM 
	 * del canal que tiene almacenado, que ser� el canal actual.
	 * 
	 * @param rc RequestContext.
	 * @return Identificador VCM del canal que estaba almacenado en la request.
	 * */
	public static String getCurrentChannelID(RequestContext rc) {
		
		if (logger.isDebugEnabled()) logger.debug("String CommonUtils.getCurrentChannelID(RequestContext rc)");
		
		String currentChannel = "";		
		Channel canal = null;
		try {
			canal = rc.getRequestedChannel();
			if (canal != null)
				currentChannel = canal.getContentManagementId().toString();			
		} catch (ApplicationException e) {
			logger.error(null,e);
		}	
		
		if (logger.isDebugEnabled()) logger.debug("String CommonUtils.getCurrentChannelID:return:"+currentChannel);
		
		return currentChannel;	
	}
	
	/**
	 * Funci�n que recibe como par�metro la request para obtener el identificador de VCM 
	 * del canal home del site al que corresponde la petici�n actual
	 * 
	 * @param rc RequestContext.
	 * @return Identificador VCM del canal Home
	 * */
	public static String getHomeChannel(RequestContext rc) {
		
		if (logger.isDebugEnabled()) logger.debug("String CommonUtils.getHomeChannel(RequestContext rc)");
		
		String homeChannel = "";		
		Channel canal = null;
		try {
			canal = rc.getCurrentSite().getHomeChannel();
			if (canal != null)
				homeChannel = canal.getContentManagementId().toString();			
		} catch (ApplicationException e) {
			logger.error(null,e);
		} catch (ValidationException e) {
			logger.error(null,e);
		}	
		
		if (logger.isDebugEnabled()) logger.debug("String CommonUtils.getHomeChannel:return:"+homeChannel);
		
		return homeChannel;	
	}


	/**
	 *  Este m�todo recupera un ManagedObjectVCMRef dado su identificador en VCM.
	 *
	 * @param VCMID cadena de caracteres que representa el id en VCM de un ManagedObjectVCMRef.
	 * @return ManagedObjectVCMRef 
	 */
	protected static ManagedObjectVCMRef getManagedObjectVCMRefObjectNoCached (String VCMID)
	{
		if (logger.isDebugEnabled()) logger.debug("ManagedObjectVCMRef CommonUtils.getManagedObjectVCMRefObject: Creaci�n de ManagedObjectVCMRefObject con VCMID: "+VCMID+"");
		return new ManagedObjectVCMRef (VCMID);
	}
	
	/**
	 * Metodo al que se le pasa una instancia de contenido, el nombre de un relator y el atributo del relator del que se quiere recuperar
	 * sus contenidos. Devuelve un ContentInstance con el primer contenido relacionado
	 *	 
	 * @param ci ContentInstance  a partir de la cual obtendremos los atributos relacionados.
	 * @param relationName String, nombre del Relator 
	 * @param attributeRelation String, nombre del atributo del Relator que se desea recuperar
	 * @return Retorna un ContentInstance con la primera instancia de contenido relacionada que tenga ese ContentInstance
	 * */
	public static ContentInstance[] getAllRelationsVCM(ContentInstance ci,String relationName, String attributeRelation)
	{
		if (logger.isDebugEnabled()) logger.debug("String[] CommonUtils.getAllRelationsVCM (ContentInstance ci="+ci+", String relationName="+relationName+", String attributeRelation="+attributeRelation);
		
		String[] s= getIdRelations (ci,relationName,attributeRelation);
		logger.debug("CommonUtils.getAllRelationsVCM relations: " + s.toString());
		
		ContentInstance[]result= new ContentInstance[s.length];
		if (s!=null && s.length>0) 
		{
			
			int i=0;
			ContentInstance ciR=null;
			
			do
			{
				
				ciR = RelatorUtils.getRelatedCIByVCMID(s[i]);
				//ciR= RelatorUtils.getRelatedCIByGUID(s[i], ci.getObjectType().getName());
				//logger.debug("Nombre Relacionado: "+ciR.getName());
				result[i]=ciR;
				i++;
			}
			while (i<s.length);
			
		}
		logger.debug("Fin CommonUtils.getAllRelationsVCM return array of: " + result.length);		
		return result; 
		
		
	}
	
	/**
	 * Metodo al que se le pasa una instancia de contenido, el nombre de un relator y el atributo del relator del que se quiere recuperar
	 * sus contenidos. Devuelve un array de ContentInstance con todos los contenidos relacionados
	 *	 
	 * @param ci ContentInstance  a partir de la cual obtendremos los atributos relacionados.
	 * @param relationName String, nombre del Relator 
	 * @param attributeRelation String, nombre del atributo del Relator que se desea recuperar
	 * @return Retorna un array de ContentInstance con todos las instancias de contenido relacionados que tenga ese ContentInstance
	 * */
	public static ContentInstance[] getRelations(ContentInstance ci,String relationName, String attributeRelation)
	{
		logger.debug("String[] CommonUtils.getRelations (ContentInstance ci="+ci+", String relationName="+relationName+", String attributeRelation="+attributeRelation);
		ContentInstance[] returnValue=null;
		try
		{
			String[] s= getIdRelations (ci,relationName,attributeRelation);
			logger.debug("CommonUtils.getRelations relations: " + s.toString());
			if (s!=null) 
			{
				
				int i=0;
				ContentInstance ciR=null;
				ContentInstance[]temp= new ContentInstance[s.length];
				do
				{
					
					ciR= RelatorUtils.getRelatedCIByGUID(s[i], ci.getObjectType().getName());
					temp[i]=ciR;
					i++;
				}
				while (i<s.length);
				returnValue=temp;
			}
			
			
			return returnValue;
		}
	catch (ApplicationException e) 
		{
			e.printStackTrace();
			return returnValue;
		} 
		
		
	}
	
	/**
	 * M�todo al que se le pasa una instancia de contenido, el nombre de un relator y el atributo del relator del que se quiere recuperar
	 * sus contenidos. Devuelve un ContentInstance con el primer contenido relacionado
	 *	 
	 * @param ci ContentInstance  a partir de la cual obtendremos los atributos relacionados.
	 * @param relationName String, nombre del Relator 
	 * @param attributeRelation String, nombre del atributo del Relator que se desea recuperar
	 * @return Retorna un ContentInstance con la primera instancia de contenido relacionada que tenga ese ContentInstance
	 * */
	public static ContentInstance getRelationsVCM(ContentInstance ci,String relationName, String attributeRelation)
	{
		if (logger.isDebugEnabled()) logger.debug("String[] CommonUtils.getRelationsVCM (ContentInstance ci="+ci+", String relationName="+relationName+", String attributeRelation="+attributeRelation);
		ContentInstance returnValue=null;
		
		String[] s= getIdRelations (ci,relationName,attributeRelation);
		
		if (s!=null) 
		{
			
			int i=0;
			ContentInstance ciR=null;
			ContentInstance[]temp= new ContentInstance[s.length];
			do
			{
				
				ciR = RelatorUtils.getRelatedCIByVCMID(s[i]);
				//ciR= RelatorUtils.getRelatedCIByGUID(s[i], ci.getObjectType().getName());
				//logger.debug("Nombre Relacionado: "+ciR.getName());
				temp[i]=ciR;
				i++;
			}
			while (i<s.length);
			returnValue=temp[0];
		}
				
		return returnValue; 
		
		
	}
	/**
	 * M�todo al que se le pasa una instancia de contenido, el nombre de un relato, el atributo del relator del que se quiere recuperar
	 * sus contenido, el Id de la relacion y el atributo que almacena el Id. Devuelve un ContentInstance con el contenido relacionado de la relacion cuyo Id coincide con el pasado por parametro
	 *	 
	 * @param ci ContentInstance  a partir de la cual obtendremos los atributos relacionados.
	 * @param relationName String, nombre del Relator 
	 * @param attributeRelation String, nombre del atributo del Relator que se desea recuperar
	 * @param Id String, Identificador de la relaci�n de la que deseamos recuperar un atributo
	 * @param IdAttribute String, Nombre del atributo que almacena el identificador para la relaci�n
	 * @return Retorna un ContentInstance con la primera instancia de contenido relacionada que tenga ese ContentInstance
	 * */
	public static ContentInstance getRelationsVCMId(ContentInstance ci,String relationName, String attributeRelation, String Id, String IdAttribute)
	{
		if (logger.isDebugEnabled()) logger.debug("String[] CommonUtils.getRelationsVCMId (ContentInstance ci="+ci+", String relationName="+relationName+", String attributeRelation="+attributeRelation+" Id:" + Id + " Atr: " + IdAttribute );
		ContentInstance returnValue=null;
		AttributedObject[] claves = null;
		ContentRelationInstance claveCri = null;
		String vcmId = null;
		
		try {
			claves = ci.getRelations(relationName);
		
			//String[] s= getIdRelations (ci,relationName,attributeRelation);
			boolean encontrado = false;
			int i = 0;
			if ((claves != null) && (claves.length > 0)) {
				do {
 
					claveCri = (ContentRelationInstance) claves[i];
					
					if (Id.equals(claveCri.getAttributeValue(IdAttribute).toString()))
					{
						encontrado = true;
					}
					i++;

				} while (i < claves.length && !encontrado);

				if (encontrado)
				{
					vcmId = (String)claveCri.getAttributeValue(attributeRelation);
				}
				else{
					vcmId = null;
				}
			}
			
		}catch (java.lang.NullPointerException e) {
			if (logger.isDebugEnabled())
				logger
						.debug("Excepcion lanzada, posiblemente no exista el valor del atributo para el lenguaje establecido. Se devuelve el mensaje por defecto.");
			e.printStackTrace();
		} catch (ApplicationException e) {
			if (logger.isDebugEnabled())
				logger
						.debug("Excepcion lanzada, posiblemente no exista el valor del atributo para el lenguaje establecido. Se devuelve el mensaje por defecto.");
			e.printStackTrace();
		}	
		if (vcmId != null)
			returnValue = RelatorUtils.getRelatedCIByVCMID(vcmId); 
		
		return returnValue;
		
		
	}
	/**
	 * M�todo al que se le pasa una instancia de contenido, el nombre de un relator y el atributo del relator del que se quiere recuperar
	 * sus contenidos. Devuelve un String con el valor para el atributo de dicha relacion
	 *	 
	 * @param ci ContentInstance  a partir de la cual obtendremos los atributos relacionados.
	 * @param relationName String, nombre del Relator 
	 * @param attributeRelation String, nombre del atributo del Relator que se desea recuperar
	 * @return Retorna un String con el valor del atributo deseado
	 * */
	public static String getRelationsValue(ContentInstance ci,String relationName, String attributeRelation)
	{
		if (logger.isDebugEnabled()) logger.debug("String[] CommonUtils.getRelationsValue (ContentInstance ci="+ci+", String relationName="+relationName+", String attributeRelation="+attributeRelation);
		String returnValue=null;
		
		String[] s= getIdRelations (ci,relationName,attributeRelation);
		
		if ((s!=null) && (s.length >0))
		{
			returnValue =  s[0];
			
		}
		
		
		return returnValue; 
		
		
	}
	
	
	
	
	/**
	 * M�todo al que se le pasa el nombre de un relator y el atributo del relator del que se quiere recuperar
	 * sus contenidos. Devuelve un array de String con todos los valores
	 *	 
	 * @param ci ContentInstance  a partir de la cual obtendremos los atributos relacionados.
	 * @param relationName String, nombre del Relator 
	 * @param attributeRelation String, nombre del atributo del Relator que se desea recuperar
	 * @return Retorna un array de String con todos los atributos relacionados que tenga ese relator
	 * */
	private static String[] getIdRelations (ContentInstance ci,String relationName, String attributeRelation)
	{
		Object valorRecuperado = null;
		if (logger.isDebugEnabled()) logger.debug("String[] CommonUtils.getIdRelations (ContentInstance ci="+ci+", String relationName="+relationName+", String attributeRelation="+attributeRelation);
		String[] returnValue=null;
		
		try
		
		{
			AttributedObject[] cri= null;
			cri= ci.getRelations(relationName);
			String[]temp= null;
			if ((cri!=null) &&(cri.length>0))
			{
				temp=new String[cri.length];
				for (int i=0;i<cri.length;i++)
					temp[i] = (String)cri[i].getAttributeValue(attributeRelation);
					
			}
			
			returnValue= temp;
			//logger.debug("returnValue[0]: "+returnValue[0]);
			return returnValue;	
		}
		
		catch (ClassCastException e)
		{
			logger.error(null,e);
			return returnValue;
		} 
		catch (ApplicationException e) 
		{
			e.printStackTrace();
			return returnValue;
		}
	catch (NullPointerException e)
		{
			logger.error(null,e);
			return returnValue;
		}

	}
	
	/**
	 * M�todo al que se le pasa el nombre de un relator. Devuelve un array de AttributedObject con las relaciones del tipo solicitado
	 *	 
	 * @param ci ContentInstance  a partir de la cual obtendremos las relaciones.
	 * @param relationName String, nombre del Relator a recuperar
	 * 
	 * @return Retorna un array de AttributedObject con las realciones
	 * */
	public static AttributedObject[] getRelationsObject (ContentInstance ci,String relationName)
	{
		
		if (logger.isDebugEnabled()) logger.debug("String[] CommonUtils.getIdRelations (ContentInstance ci="+ci+", String relationName="+relationName);
		AttributedObject[] returnValue=null;
		
		try
		
		{
			AttributedObject[] cri= null;
			cri= ci.getRelations(relationName);
			AttributedObject[]temp= null;
			if ((cri!=null) &&(cri.length>0))
			{
				temp=new AttributedObject [cri.length];
				for (int i=0;i<cri.length;i++)
					temp[i] = cri[i];
					
			}
			
			returnValue= temp;
			return returnValue;	
		}
		
		catch (ClassCastException e)
		{
			logger.error(null,e);
			return returnValue;
		} 
		catch (ApplicationException e) 
		{
			e.printStackTrace();
			return returnValue;
		}
	catch (NullPointerException e)
		{
			logger.error(null,e);
			return returnValue;
		}

	}
	/**
	 * Este m�todo devuelve una Valor determinada de un Array.
	 * 
	 * @param valores array que contiene relaciones en los valores.
	 * @param pos n�mero entero que se utiliza para devolver el valor en la posici�n del array.
	 * @param attributeRelation String, nombre del atributo del Relator que se desea recuperar
	 * @return Retorna el valor para un atributo de una relaci�n, dada la posici�n q ocupa la relacion en el vector de relaciones para la instancia
	 * */
	public static String getAttributeRel(AttributedObject[] valores, int pos, String attributeRelation) {
		if (logger.isDebugEnabled()) logger.debug("ContentInstance CMSUtils.getAttributeRel (ContentInstance[] contenidos, int pos="+pos+")");
		
		String returnValue = null;
		if (valores.length <=pos) {
			returnValue=null;		
			return returnValue;
		}
		else 
			try {
				Object temp = valores[pos].getAttributeValue(attributeRelation);
				if (temp instanceof Date) {
					returnValue = ((Date) temp).getTime() + "";
				} else {
					returnValue = (String) temp;
				}
				return returnValue;	
			} catch (ClassCastException e) {
				logger.error(null,e);
				return returnValue;
			} catch (ApplicationException e)  {
				e.printStackTrace();
				return returnValue;
			} catch (NullPointerException e) {
				logger.error(null,e);
				return returnValue;
			}
	}
	
	/**
	 * Este m�todo devuelve las instancias de un tipo determinado asociadas a un cana�
	 * 
	 * @param vcmId ID del canal
	 * @param strType Tipo de las instancias de contenido a buscar.
	 * @return Array de instancias
	 * */
	public static ContentInstance[] getContentInstancesByChannel( String vcmId, String strType){
	
		List listaContenidos= channelOps.findContentInstancesByChannel(channelOps.getChannelRefFromVcmId(vcmId), strType);
		Iterator iterator = listaContenidos.iterator();
		ContentInstance[] returnValue = new ContentInstance[listaContenidos.size()];
		int i=0;
		while (iterator.hasNext()) 
		{
			ManagedObject obj = (ManagedObject)iterator.next();
			ContentInstance ci = (ContentInstance) obj;
			returnValue[i]= ci;
			i++;
		}		
		return returnValue;
	}
	
	/**
	 * Este m�todo devuelve un ContentInstances determinado de un Array.
	 * 
	 * @param contenidos array que contiene instancias de contenido.
	 * @param pos N�mero entero que se utiliza para devolver la instancia de contenido del array.
	 * 
	 * @return Retorna la instancia que este en la posici�n 'pos' del array 'ContentInstance[]'.
	 * */
	public static ContentInstance getContentInstanceByPos(ContentInstance[] contenidos, int pos) {
		if (logger.isDebugEnabled()) logger.debug("ContentInstance CMSUtils.getContentInstanceByPos (ContentInstance[] contenidos, int pos="+pos+")");
		
		ContentInstance returnValue = null;
		if (contenidos.length <=pos) 
			returnValue=null;		
		else
			returnValue = contenidos[pos];		
		
		return returnValue;
	}
	 
	
	
	public static ContentInstance getContentInstanceByPos(ArrayList <ContentInstance> contenidos, int pos){
		ContentInstance returnValue = null;
		if (contenidos.size() <=pos) 
			returnValue=null;		
		else
			returnValue = contenidos.get(pos);		
		
		return returnValue;
	}

	/**
	 * Este m�todo devuelve el n�mero de elementos de una vector de Object.
	 * 
	 * @param Vector 
	 * @return int tama�o del vector o 0 si el objeto es null
	 * */
	public static int getVectorSize(Object[] vector) {
		if (vector == null) 
			return 0;		
		return vector.length;
		
	}
	
	public static int getSize(ArrayList vector){
		if(vector == null)
			return 0;
		return vector.size();
	}
	
	/**
	 * Este m�todo devuelve el n�mero de elementos de un Hashtable de Object.
	 * 
	 * @param Vector 
	 * @return int tama�o del vector o 0 si el objeto es null
	 * */
	public static int getTableSize(Hashtable table) {
		if (table == null) 
			return 0;		
		return table.size();	
	}	

	
	/**
	 * @param ci ContentInstance de la que queremos saber su Identificador en VCM.
	 * 
	 * @return indentificador VCM de la instancia que se pasa como par�metro.
	 * */
	public static String getVcmIdCi(ContentInstance ci) {
		if (logger.isDebugEnabled()) logger.debug("CMSUtils.getIdCi(ContentInstance ci)");
		
		String vcmId = null;		
		vcmId = ci.getContentManagementId().getId();
	
		if (logger.isDebugEnabled()) logger.debug("CMSUtils.getIdCi:return:"+vcmId);
		
		return vcmId;		
	}
	
	
	/**
	 * @param contexto para obtener el currentChannel
	 * 
	 * @return profundidad del canal en el site
	 * */
	public static int getCurrentChannelDepth(RequestContext rc) {
		int depth = 0;
		Channel currentChannel = null;
		Channel[] padresCanal = null;
		try {
			currentChannel = rc.getRequestedChannel();
			padresCanal = currentChannel.getBreadcrumbPath(true);			
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (padresCanal!=null) depth = padresCanal.length;
		return depth;
		
	}
	
	/**
	 * Devuelve un array (ContentInstance) de todos los contenidos de un determinado tipo .
	 * @param strType nombre XML del tipo de datos sobre el que se busca
	 * @param strOrder nombre XML del atributo que regir� la ordenaci�n
	 * @return Retorna un array de todos los ContentInstance de un determinado tipo
	 * */
	public static ContentInstance[] getContentInstancesByContentType(String strType, String strOrder){
		
		if(logger.isDebugEnabled())
            logger.debug((new StringBuilder()).append("Buscando contenidos de tipo: ").append(strType).toString());
	
		ManagedObjectWhereClause typeClause = new ManagedObjectWhereClause();
		try {
			ContentType contentType = (ContentType)ContentType.findByName(strType);
			ContentInstanceDBQuery moDBQuery = new ContentInstanceDBQuery(new ContentTypeRef(contentType.getId()));					
			moDBQuery.addOrderByAttribute (strOrder);
			List listaContenidos = DpmQueryManager.getInstance().execute(moDBQuery).asList();
			Iterator iterator = listaContenidos.iterator();
			ContentInstance[] returnValue = new ContentInstance[listaContenidos.size()];
			int i=0;
			while (iterator.hasNext()) 
			{
				//ManagedObject obj = (ManagedObject)iterator.next();
				ContentInstance ci = (ContentInstance) iterator.next();;
				returnValue[i]= ci;
				i++;
			}		
			return returnValue;
		} catch (ApplicationException e) {
			logger.error((new StringBuilder()).append("Error recuperando contenidos de tipo: ").append(strType).toString() + " | " + e.getMessage());
			e.printStackTrace();
		} catch (ValidationException e) {
			logger.error((new StringBuilder()).append("Error recuperando contenidos de tipo: ").append(strType).toString() + " | " + e.getMessage());
			e.printStackTrace();
		} 
		return null;
	}	
	
	/**
	 * Devuelve un array (ContentInstance) de todos los contenidos segun la lista de limitaciones indicada (ManagedObjectWhereClause).
	 * @param strType nombre XML del tipo de datos sobre el que se busca
	 * @param query La lista de clausulas
	 * @param strOrder nombre XML del atributo que regira la ordenacion
	 * @return Retorna un array de todos los ContentInstance que cumplan la clausula indicada
	 * */
	public static ContentInstance [] getContentInstancesByQuery (String strType, List query, String strOrder){ 
		
		if(logger.isDebugEnabled())
            logger.debug((new StringBuilder()).append("Buscando contenidos segun query: ").append(query).toString());
	
		Iterator clauses = query.iterator();
		ContentType contentType;
		try {
			contentType = (ContentType)ContentType.findByName(strType);
			ContentInstanceDBQuery moDBQuery = new ContentInstanceDBQuery(new ContentTypeRef(contentType.getId()));
			moDBQuery.addOrderByAttribute (strOrder);
			while (clauses.hasNext()){
				ManagedObjectWhereClause clause = (ManagedObjectWhereClause) clauses.next();
				moDBQuery.addWhereClause(clause);
			}
							
			List listaContenidos;
			listaContenidos = DpmQueryManager.getInstance().execute(moDBQuery).asList();
			Iterator iterator = listaContenidos.iterator();
			ContentInstance[] returnValue = new ContentInstance[listaContenidos.size()];
			int i=0;
			while (iterator.hasNext()) 
			{
			
				ContentInstance ci = (ContentInstance) iterator.next();
				returnValue[i]= ci;
				i++;
			}		
			return returnValue;
		} catch (ApplicationException e) {
			logger.error((new StringBuilder()).append("Error recuperando contenidos de tipo: ").append(strType).toString() + " | " + e.getMessage());
			e.printStackTrace();
		} catch (ValidationException e) {
			logger.error((new StringBuilder()).append("Error recuperando contenidos de tipo: ").append(strType).toString() + " | " + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	/**

	 * Para una instancia de contenido, devuelve el valor de un atributo dado
	 * para la relacion que cumpla la condicion
	 * de igualdad con el valor pasado por parametro para cierto atributo de dicha relacion
	 *
	 * @param ci - Instancia en la que queremos buscar la el valor
	 * @param rel - Relacion en la que queremos encontrar el valor
	 * @param att1 - Atributo que queremos obtener de la relacion "rel"
	 * @param att2 - Atributo de la relacion "rel" que queremos que coincida con el valor "valor"
	 * @param valor - Valor con el que queremos que coincida el att2 de la relaccion
	 * @return El valor de la propiedad att1
	 */
	public static String getAtt1FromRelWhereAtt2EqualsVal(ContentInstance ci, String rel, String att1, String att2, String valor) {
		if (logger.isDebugEnabled())
			logger.debug("getAtt1FromRelWhereAtt2EqualsVal"+" rel="+ rel + " att1="+ att1 + " att2=" + att2+ " valor=" + valor);
		AttributedObject[] relacionesCond = null;
		ContentRelationInstance cri = null;
		Object returnValue = "";
		try {
			relacionesCond = getRelationsObject(ci, rel);
			boolean encontrado = false;
			if ((relacionesCond != null) && (relacionesCond.length > 0)) {
				for (int i = 0; (i < relacionesCond.length && !encontrado); i++) {
					
					cri = (ContentRelationInstance) relacionesCond[i];
					if ((cri.getAttributeValue(att2) != null) && (valor.equals(cri.getAttributeValue(att2).toString()))) {
						// Si el valor coincide con al att2 de la traduccion
						returnValue = cri.getAttributeValue(att1);

						if (returnValue == null) {
							returnValue = ""; // en caso de existir la relaci�n pero no valor para ese campo se devuelve vacio
						}
						encontrado = true; // Para abandonar la b�squeda
					}
				}
			}
		} catch (ApplicationException e) {
			returnValue = "";
			if (logger.isWarnEnabled())
				logger.warn("getAtt1FromRelWhereAtt2EqualsVal |TRACE --> Error: ", e);
		}

		if (logger.isDebugEnabled())
			logger.debug(".getAtt1FromRelWhereAtt2EqualsVal |TRACE -->Return:" + returnValue + ")");
		
		//System.out.println ("MABEL: retorno " + returnValue);
		return returnValue.toString();
	}
	
	
	/**
	 * Indica si la instancia de contenido <code>vcmId</code> contiene la taxonomia <code>clasifications</code>.
	 * @param vcmId String con el identificador de la instancia de contenido
	 * @param clasificactions String con la clasificaci�n a buscar
	 * @return boolean que indica si la instancia tiene o no asociada la taxonomia.
	 */
	public static boolean getTaxonomyClassifications(String vcmId, String clasificactions){
		ManagedObjectVCMRef moVCMRef=new ManagedObjectVCMRef(vcmId); 
		try {
			ManagedObject contenidoActual= moVCMRef.retrieveManagedObject();
			ContentInstance ci = (ContentInstance) contenidoActual;
            if (ci!=null){ 
            	String[] taxonomias=ci.getTaxonomyClassifications();
                String taxonomia = ""; 
                for(int i=0;i<taxonomias.length;i++){ 
                	taxonomia=taxonomias[i].toString().substring(1);
                    if (taxonomia.compareTo(clasificactions)==0)
                            return true ;
                   }
            }
		} catch (ApplicationException e) {
				logger.error("Error obteniendo las Taxonomias de la instancia " + vcmId);
		} catch (RemoteException e) {
				logger.error("Error obteniendo las Taxonomias de la instancia " + vcmId);
		} 
		return false;
	}
	
	
	
	public static String getOfertasTrabajo (String contenido){
		return "<p> Contenido </p>";
	}
	
	public static Channel getFirstChannelInSite(ContentInstance contenido, String siteName) {
    	Channel canal=null;
    	try {
    		Channel canalTemp=null;
    		SiteRef sites[] = null;
    		ChannelRef[] canales=contenido.getChannelAssociations();
    		String siteNameTemp=null;
            for(int i=0;i<canales.length;i++) {
            	canalTemp=canales[i].getChannel();
            	sites=canalTemp.getSiteRefs();
            	siteNameTemp=sites[0].getSite().getName();
            	if(siteNameTemp.equals(siteName)) {
            		canal=canalTemp;
            		break;
            	}
            }
    	}
    	catch (Exception e) {
			logger.error("getFirstChannelInSite(ContentInstance, String) - Se ha producido una excepcion", e);
		}
    	
    	return canal;
    }
		
}	