package com.ieci.cepsa17.wem.comun.util.buscador.comparador;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import org.apache.log4j.Logger;

import com.ieci.cepsa17.wem.comun.util.buscador.bean.Certificado;

public class ComparadorFechaCaducidadCertificado implements Comparator<Certificado> {

	protected static Logger logger = Logger.getLogger(ComparadorFechaCaducidadCertificado.class);

	@Override
	public int compare(Certificado o1, Certificado o2) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		try {
			Date o1Date = dateFormat.parse(o1.getFechaCaducidad());
			Date o2Date = dateFormat.parse(o2.getFechaCaducidad());

			return o1Date.compareTo(o2Date);
		} catch (ParseException e) {
			logger.error("Se ha producido un error comparando las fechas de caducidad de dos certificados: "
					+ e.getMessage(), e);
		}

		return 0;

	}

}