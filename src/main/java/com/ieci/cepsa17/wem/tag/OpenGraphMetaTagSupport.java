package com.ieci.cepsa17.wem.tag;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.log4j.Logger;

import com.ieci.cepsa17.wem.comun.util.cms.RelatorUtils;
import com.ieci.cepsa17.wem.tag.bean.OpenGraphMetaBean;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.javabean.Channel;
import com.vignette.as.client.javabean.ContentInstance;
import com.vignette.as.client.javabean.ManagedObject;
import com.vignette.ext.templating.client.javabean.Page;
import com.vignette.ext.templating.commonlib.LinkTagSupport;
import com.vignette.ext.templating.taglib.TagUtil;
import com.vignette.ext.templating.util.ContentUtil;
import com.vignette.ext.templating.util.PageUtil;
import com.vignette.ext.templating.util.RequestContext;
import com.vignette.util.StringUtil;

public class OpenGraphMetaTagSupport extends TagUtil  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(OpenGraphMetaTagSupport.class);
	
	private static final String TYPE_WEBSITE = "website";
	private static final String TYPE_ARTICLE = "article";
	private static final String IMAGE_ATTR = "IMAGEN";
	
	PageContext pageContext = null;
	RequestContext requestContext = null;
	
	private int varScope = 1;
	
	private String varName = null;
	private String articleChannelId=null;
	private String currentChannelId=null;
	private String httpServletRequestUrl = null;
	private String favicon = null;
	private String imageAttr = IMAGE_ATTR;
	
	
	
	public String getImageAttr() {
		return imageAttr;
	}

	public void setImageAttr(String imageAttr) {
		this.imageAttr = imageAttr;
	}

	public String getFavicon() {
		return favicon;
	}

	public void setFavicon(String favicon) {
		this.favicon = favicon;
	}

	public String getArticleChannelId() {
		return articleChannelId;
	}

	public void setArticleChannelId(String articleChannelId) {
		this.articleChannelId = articleChannelId;
	}
	
	public String getCurrentChannelId() {
		return currentChannelId;
	}

	public URL getHttpServletRequestUrlObject() {
		try {
			return new URL(httpServletRequestUrl);
		} catch (MalformedURLException e) {
			logger.error(e,e);
		}
		return null;
	}

	public String getHttpServletRequestUrl() {
		return httpServletRequestUrl;
		
	}
	public void setHttpServletRequestUrl(String httpServletRequestUrl) {
		this.httpServletRequestUrl = httpServletRequestUrl;
	}

	public void setCurrentChannelId(String currentChannelId) {
		this.currentChannelId = currentChannelId;
	}

	
	public PageContext getPageContext() {
		return pageContext;
	}

	public void setPageContext(PageContext pageContext) {
		this.pageContext = pageContext;
		this.requestContext = PageUtil.getCurrentRequestContext(pageContext);
		this.requestContext.setPageContext(this.pageContext);
	}

	public RequestContext getRequestContext() {
		return requestContext;
	}

	public void setRequestContext(RequestContext requestContext) {
		this.requestContext = requestContext;
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	public int getVarScope() {
		return varScope;
	}

	public void setVarScope(String scope) throws JspException {
		
		if (scope.equalsIgnoreCase("page")) {
			this.varScope = 1;
		} else if (scope.equalsIgnoreCase("request")) {
			this.varScope = 2;
		} else if (scope.equalsIgnoreCase("session")) {
			this.varScope = 3;
		} else if (scope.equalsIgnoreCase("application")) {
			this.varScope = 4;
		} else {
			throw new JspException("Invalid scope: " + scope);
		}
		
	}
	
	protected void printString(String s)
			throws ApplicationException
	{
		this.requestContext.printString(s);
	}
	
	
	protected String getBaseURL(){
		String protocol = "https";
		String host ="www.cepsa.com";
		URL url = getHttpServletRequestUrlObject();
		if (url!=null){
			protocol = url.getProtocol();
			host = url.getHost();
		}
		return protocol + "://"+ host;
	}
	
	public int doStartTag()
			throws JspException
	{
		PageContext pc = requestContext.getPageContext();
		OpenGraphMetaBean bean = new OpenGraphMetaBean();
		String baseUrl = getBaseURL();
		ManagedObject mo = null;
			
		try {
			
			mo = requestContext.getPrimaryRequestedObject();
			if(mo == null) {
				mo = ContentUtil.getManagedObject(requestContext.getRequestOID());
			}
			if(mo!=null && (mo instanceof Page || mo instanceof Channel)) {
				//Canal o Página
				logger.debug(String.format("OpenGraphMetaTagSupport:: ManagedObject instance of Page or Channel : %s ",requestContext.getRequestOID().getId()));
				
				//tipo
				bean.setType(TYPE_WEBSITE);
				//page url			
				String pageUrl = LinkTagSupport.contentLink(requestContext, requestContext.getRequestOID().getId(), null, requestContext.getRequestOID().getId(), null, null, null, true, true, null, null, null, null);
				bean.setUrlPage(baseUrl+pageUrl);
				//imageurl
				bean.setImageUrl(baseUrl+getFavicon());
				
				logger.debug("OpenGraphMetaTagSupport:: OpenGraphMetaBean for Channel : " + bean.toString());
			}else if (mo!=null && mo instanceof ContentInstance){
				logger.debug(String.format("OpenGraphMetaTagSupport:: ManagedObject instance of ContentInstance : %s", requestContext.getRequestOID().getId()));
				//Comprobamos el canal
				//tipo
				bean.setType(TYPE_WEBSITE);
				//pageurl
				String pageUrl = LinkTagSupport.contentLink(requestContext, requestContext.getRequestOID().getId(), null, getCurrentChannelId(), null, null, null, true, true, null, null, null, null);
				bean.setUrlPage(baseUrl+pageUrl);
				//imageurl
				bean.setImageUrl(baseUrl+getFavicon());
				if (articleChannelId.equals(getCurrentChannelId())){
					//Estamos en el canal en el que podemos compartir imágenes del contenido
					//Content Instance
					
					//tipo
					bean.setType(TYPE_ARTICLE);
					String imagenVCMID = (String)((ContentInstance)mo).getAttributeValue(getImageAttr());
					logger.debug(String.format("OpenGraphMetaTagSupport:: ManagedObject in selected Channel %s and imageVCMId %s", getCurrentChannelId(),imagenVCMID ));
					if (imagenVCMID!=null && !"".equals(imagenVCMID)){
						ContentInstance cont = (ContentInstance) RelatorUtils.getRelatedCIByVCMID(imagenVCMID);
						//image url
						if (!StringUtil.isEmpty((String)cont.getAttributeValue("SOURCEPATH"))){
							bean.setImageUrl(baseUrl+(cont!=null?"/stfls"+(String)cont.getAttributeValue("SOURCEPATH"):getFavicon()));
							bean.setImageWidth(cont!=null?(Integer)cont.getAttributeValue("WIDTH"):0);
							bean.setImageHeight(cont!=null?(Integer)cont.getAttributeValue("HEIGHT"):0);
						}else{
							bean.setImageUrl(baseUrl+getFavicon());
						}
						
						//thumbnail
						if (!StringUtil.isEmpty((String)cont.getAttributeValue("THUMBNAILPATH"))){
							bean.setThumbnailUrl((baseUrl+"/stfls"+(String)cont.getAttributeValue("THUMBNAILPATH")));
						}
						
						logger.debug(String.format("OpenGraphMetaTagSupport:: Image from CI url: %s width %d height %d", bean.getImageUrl(),bean.getImageWidth(), bean.getImageHeight() ));
						
					}
					
				}
				logger.debug("OpenGraphMetaTagSupport:: OpenGraphMetaBean for ContentInstance : " + bean.toString());
			}
			
			pc.setAttribute(this.varName, bean, this.varScope);
		
		
		} catch (ApplicationException e) {
			logger.error(e,e);
		} 		
		
		this.requestContext.setPageContext(pc);
		return SKIP_BODY;
	}

	public int doEndTag()
			throws JspException
	{
		this.varName = null;
		this.articleChannelId = null;
		this.currentChannelId = null;
		this.httpServletRequestUrl = null;
		this.requestContext = null;
		this.favicon = null;
		this.imageAttr = null;
		return EVAL_PAGE;
	}
	
	
	
	
}
