package com.ieci.cepsa17.wem.tag;

import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.log4j.Logger;

import com.vignette.as.client.exception.ApplicationException;
import com.vignette.ext.templating.util.PageUtil;
import com.vignette.ext.templating.util.RequestContext;

public abstract class DPMBodyTagSupport extends BodyTagSupport {
	
	protected static Logger logger = Logger.getLogger(DPMBodyTagSupport.class);

	PageContext pageContext = null;
	RequestContext requestContext = null;
	  
	public void setPageContext(PageContext pageContext)	{
		this.pageContext = pageContext;
		this.requestContext = PageUtil.getCurrentRequestContext(pageContext);
		this.requestContext.setPageContext(this.pageContext);
	}

	public void printString(String s) throws ApplicationException {
		this.requestContext.printString(s);
	}
	
}
