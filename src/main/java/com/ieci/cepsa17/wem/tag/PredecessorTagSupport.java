package com.ieci.cepsa17.wem.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.log4j.Logger;

import com.ieci.cepsa17.wem.comun.util.NavigationUtil;
import com.ieci.cepsa17.wem.comun.util.cms.CmsOpsFactory;
import com.ieci.cepsa17.wem.comun.util.cms.IChannelOps;
import com.ieci.cepsa17.wem.constants.NavigationConstants;
import com.vignette.as.client.common.ref.AsLocaleRef;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.exception.ValidationException;
import com.vignette.as.client.javabean.Channel;
import com.vignette.ext.templating.taglib.TagUtil;
import com.vignette.ext.templating.util.PageUtil;
import com.vignette.ext.templating.util.RequestContext;



public class PredecessorTagSupport extends TagUtil {

	private static final long serialVersionUID = -6678839825976993447L;

	private static Logger logger = Logger.getLogger(PredecessorTagSupport.class);

	PageContext pageContext = null;
	RequestContext requestContext = null;

	private String channelId;
	private String varName;
	private int varScope = 1;
	private short levelToShow = NavigationConstants.PREDECESSOR_LEVEL_TO_SHOW;
	private short minLevelsUp = NavigationConstants.PREDECESSOR_MINIMUM_LEVELS_UP;
	private boolean furlName = false;
	

	
	public PageContext getPageContext() {
		return pageContext;
	}

	public void setPageContext(PageContext pageContext) {
		this.pageContext = pageContext;
		this.requestContext = PageUtil.getCurrentRequestContext(pageContext);
		this.requestContext.setPageContext(this.pageContext);
	}

	public RequestContext getRequestContext() {
		return requestContext;
	}

	public void setRequestContext(RequestContext requestContext) {
		this.requestContext = requestContext;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	public void setScope(String scope) throws JspException {

		if ("page".equalsIgnoreCase(scope)) {
			this.varScope = 1;
		} else if ("request".equalsIgnoreCase(scope)) {
			this.varScope = 2;
		} else if ("session".equalsIgnoreCase(scope)) {
			this.varScope = 3;
		} else if ("application".equalsIgnoreCase(scope)) {
			this.varScope = 4;
		} else {
			throw new JspException("Invalid scope: " + scope);
		}

	}

	public short getLevelToShow() {
		return levelToShow;
	}

	public void setLevelToShow(short levelToShow) {
		this.levelToShow = levelToShow;
	}

	public short getMinLevelsUp() {
		return minLevelsUp;
	}

	public void setMinLevelsUp(short minLevelsUp) {
		this.minLevelsUp = minLevelsUp;
	}
	

	public boolean isFurlName() {
		return furlName;
	}

	public void setFurlName(boolean furlName) {
		this.furlName = furlName;
	}

	@Override
	protected void printString(String s) throws ApplicationException {

		this.requestContext.printString(s);

	}

	@Override
	public int doStartTag() throws JspException {

		PageContext pc = requestContext.getPageContext();

		try {

			IChannelOps channelOps = CmsOpsFactory.getInstance().createChannelOps();
			Channel cha = channelOps.getChannelFromVcmId(this.channelId);

			if (logger.isDebugEnabled()) {
				logger.debug("Recuperando el predecesor de: " + this.channelId + " (" + cha.getName()
						+ "). Nivel a mostrar = " + this.levelToShow
						+ ". Numero minimo de niveles a subir = " + this.minLevelsUp);
			}

			Channel value = NavigationUtil.getPredecessor(this.requestContext, this.channelId,
					this.levelToShow, this.minLevelsUp);

			if (this.varName == null) {

				if (logger.isDebugEnabled()) {
					logger.debug("Escribiendo predecesor de: " + this.channelId + " (" + cha.getName() + ")" + " = "
							+ value.getName() + " idioma " + requestContext.getLocale().toString() );
				}
				AsLocaleRef locale = new AsLocaleRef(requestContext.getLocale().getLanguage(),requestContext.getLocale().getCountry());
				printString(furlName?value.getFurlName(locale):value.getName(locale));

			} else {

				if (logger.isDebugEnabled()) {
					logger.debug("Estableciendo variable: " + this.varName + " = " + value.getName() + " - scope: "
							+ this.varScope);
				}

				AsLocaleRef locale = new AsLocaleRef(requestContext.getLocale().getLanguage(),requestContext.getLocale().getCountry());
				pc.setAttribute(this.varName, furlName?value.getFurlName(locale):value.getName(locale), this.varScope);

			}

		} catch (ApplicationException ae) {

			throw new JspException(this.requestContext.getMessage("21"), ae);

		} catch (ValidationException e) {
			
			throw new JspException(this.requestContext.getMessage("21"), e);
		}

		this.requestContext.setPageContext(pc);

		return SKIP_BODY;

	}

	@Override
	public int doEndTag() throws JspException {

		this.channelId = null;
		this.varName = null;
		this.levelToShow = NavigationConstants.PREDECESSOR_LEVEL_TO_SHOW;
		this.minLevelsUp = NavigationConstants.PREDECESSOR_MINIMUM_LEVELS_UP;
		this.furlName = false;
		this.requestContext = null;
		return EVAL_PAGE;

	}

}