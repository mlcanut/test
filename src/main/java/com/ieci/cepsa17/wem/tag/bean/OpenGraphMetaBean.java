package com.ieci.cepsa17.wem.tag.bean;

public class OpenGraphMetaBean {

	private String type;
	private String imageUrl;
	private String thumbnailUrl;
	private String imageAlt;
	private Integer imageHeight;
	private Integer imageWidth;
	private String urlPage;
	
	public OpenGraphMetaBean(){
		type = new String();
		imageUrl = new String();
		thumbnailUrl = new String();
		imageAlt = new String();
		imageHeight = 0;
		imageWidth = 0;
		urlPage = new String();
	}
	
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}
	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getImageAlt() {
		return imageAlt;
	}
	public void setImageAlt(String imageAlt) {
		this.imageAlt = imageAlt;
	}
	public Integer getImageHeight() {
		return imageHeight;
	}
	public void setImageHeight(Integer imageHeight) {
		this.imageHeight = imageHeight;
	}
	public Integer getImageWidth() {
		return imageWidth;
	}
	public void setImageWidth(Integer imageWidth) {
		this.imageWidth = imageWidth;
	}
	public String getUrlPage() {
		return urlPage;
	}
	public void setUrlPage(String urlPage) {
		this.urlPage = urlPage;
	}
	
	@Override
	public String toString() {
		return "OpenGraphMetaBean [type=" + type + ", imageUrl=" + imageUrl + ", thumbnailUrl=" + thumbnailUrl
				+ ", imageAlt=" + imageAlt + ", imageHeight=" + imageHeight + ", imageWidth=" + imageWidth
				+ ", urlPage=" + urlPage + "]";
	}
	
}
