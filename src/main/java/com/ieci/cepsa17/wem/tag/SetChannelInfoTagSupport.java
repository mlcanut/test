package com.ieci.cepsa17.wem.tag;

import java.rmi.RemoteException;
import java.util.Locale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.log4j.Logger;

import com.ieci.cepsa17.wem.comun.util.NavigationUtil;
import com.ieci.cepsa17.wem.comun.util.cms.CmsOpsFactory;
import com.ieci.cepsa17.wem.comun.util.cms.IChannelOps;
import com.ieci.cepsa17.wem.constants.NavigationConstants;
import com.vignette.as.client.common.ref.AsLocaleRef;
import com.vignette.as.client.common.ref.ManagedObjectVCMRef;
import com.vignette.as.client.exception.ApplicationException;
import com.vignette.as.client.exception.ValidationException;
import com.vignette.as.client.javabean.Channel;
import com.vignette.ext.templating.taglib.TagUtil;
import com.vignette.ext.templating.util.PageUtil;
import com.vignette.ext.templating.util.RequestContext;


public class SetChannelInfoTagSupport extends TagUtil {

	private static final long serialVersionUID = -4842785869448570516L;

	private static Logger logger = Logger.getLogger(SetChannelInfoTagSupport.class);

	PageContext pageContext = null;
	RequestContext requestContext = null;

	private String channelId;
	private String segmentIds;

	public PageContext getPageContext() {
		return pageContext;
	}

	public void setPageContext(PageContext pageContext) {
		this.pageContext = pageContext;
		this.requestContext = PageUtil.getCurrentRequestContext(pageContext);
		this.requestContext.setPageContext(this.pageContext);
	}

	public RequestContext getRequestContext() {
		return requestContext;
	}

	public void setRequestContext(RequestContext requestContext) {
		this.requestContext = requestContext;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getSegmentIds() {
		return segmentIds;
	}

	public void setSegmentIds(String segmentIds) {
		this.segmentIds = segmentIds;
	}

	@Override
	protected void printString(String s) throws ApplicationException {

		this.requestContext.printString(s);

	}

	@Override
	public int doStartTag() throws JspException {

		PageContext pc = requestContext.getPageContext();

		try {

			IChannelOps channelOps = CmsOpsFactory.getInstance().createChannelOps();
			Channel cha = channelOps.getChannelFromVcmId(this.channelId);

			if (logger.isDebugEnabled()) {
				logger.debug("Almacenando en sesion el id y el nombre del predecesor de segundo nivel de: "
						+ this.channelId + " (" + cha.getName() + ")");
			}

			Channel value = NavigationUtil.getPredecessor(this.requestContext, this.channelId,
					NavigationConstants.SET_CHANNEL_INFO_LEVEL_TO_SHOW,
					NavigationConstants.SET_CHANNEL_INFO_MINIMUM_LEVELS_UP);

			String segmentId = value.getContentManagementId().toString();
			Locale esplocale = new Locale(NavigationConstants.LANGUAGE_ES, NavigationConstants.COUNTRY_ES);
			AsLocaleRef espaslocale = new AsLocaleRef(esplocale);
			AsLocaleRef locale = new AsLocaleRef(requestContext.getLocale().getLanguage(),requestContext.getLocale().getCountry());
			String segment = value.getName(espaslocale).toLowerCase();
			String segmentI18n = value.getName(locale);
			String segmentFurl = value.getFurlName(locale);
				
			if (this.segmentIds != null && !"".equals(this.segmentIds)) {
				
				Boolean inASegment = false;
				String[] availableSegments = this.segmentIds.split(",");
				for (int i = 0; i < availableSegments.length; i++) {
					if (availableSegments[i].equals(segmentId)) {
						inASegment = true;
					}
				}
				
				if (!inASegment) {
					segmentId = availableSegments[0];
					
					Channel defaultChannel = (Channel) (new ManagedObjectVCMRef(segmentId)).retrieveManagedObject();
					
					segment = defaultChannel.getName(locale).toLowerCase();
					segmentI18n = defaultChannel.getName(locale);
					segmentFurl = defaultChannel.getFurlName(locale);
				}
				
			} else {
				
				if (!NavigationConstants.VCMID_SEGMENTO_PARTICULAR.equals(segmentId)
						&& !NavigationConstants.VCMID_SEGMENTO_EMPRESAS.equals(segmentId)
						&& !NavigationConstants.VCMID_SEGMENTO_TRANSPORTISTA.equals(segmentId)
						&& !NavigationConstants.VCMID_SEGMENTO_DISTRIBUIDOR.equals(segmentId)) {

					segmentId = NavigationConstants.VCMID_SEGMENTO_PARTICULAR;
					segment = NavigationConstants.SEGMENTO_PARTICULAR;
					segmentI18n = NavigationConstants.SEGMENTO_PARTICULAR;
					
					Channel defaultChannel = (Channel) (new ManagedObjectVCMRef(segmentId)).retrieveManagedObject();
					segmentFurl = defaultChannel.getFurlName(locale);

				}
				
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Estableciendo variable: " + NavigationConstants.SEGMENT_ID + " = " + segmentId
						+ " - scope: " + NavigationConstants.SESSION_SCOPE);
			}
			pc.setAttribute(NavigationConstants.SEGMENT_ID, segmentId, NavigationConstants.SESSION_SCOPE);

			if (logger.isDebugEnabled()) {
				logger.debug("Estableciendo variable: " + NavigationConstants.SEGMENT + " = " + segment + " - scope: "
						+ NavigationConstants.SESSION_SCOPE);
			}
			pc.setAttribute(NavigationConstants.SEGMENT, segment, NavigationConstants.SESSION_SCOPE);
			pc.setAttribute(NavigationConstants.SEGMENT, segment, NavigationConstants.REQUEST_SCOPE);
			if (logger.isDebugEnabled()) {
				logger.debug("Estableciendo variable: " + NavigationConstants.SEGMENT_I18N + " = " + segmentI18n + " - scope: "
						+ NavigationConstants.SESSION_SCOPE);
			}
			pc.setAttribute(NavigationConstants.SEGMENT_I18N, segmentI18n, NavigationConstants.SESSION_SCOPE);
			pc.setAttribute(NavigationConstants.SEGMENT_I18N, segmentI18n, NavigationConstants.REQUEST_SCOPE);
			if (logger.isDebugEnabled()) {
				logger.debug("Estableciendo variable: " + NavigationConstants.SEGMENT_FURL + " = " + segmentFurl + " - scope: "
						+ NavigationConstants.SESSION_SCOPE);
			}
			pc.setAttribute(NavigationConstants.SEGMENT_FURL, segmentFurl, NavigationConstants.SESSION_SCOPE);
			pc.setAttribute(NavigationConstants.SEGMENT_FURL, segmentFurl, NavigationConstants.REQUEST_SCOPE);
			
			pc.setAttribute(NavigationConstants.LOCALE, this.requestContext.getLocale(),
					NavigationConstants.PAGE_SCOPE);

		} catch (ApplicationException | ValidationException | RemoteException ae) {

			throw new JspException(this.requestContext.getMessage("21"), ae);

		}

		this.requestContext.setPageContext(pc);

		return SKIP_BODY;

	}

	@Override
	public int doEndTag() throws JspException {

		this.channelId = null;
		this.segmentIds = null;
		this.requestContext = null;
		return EVAL_PAGE;

	}

}