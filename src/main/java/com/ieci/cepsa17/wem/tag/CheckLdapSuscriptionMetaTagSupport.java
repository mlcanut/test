package com.ieci.cepsa17.wem.tag;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.log4j.Logger;

import com.vignette.as.client.exception.ApplicationException;
import com.vignette.ext.templating.profile.IProfile;
import com.vignette.ext.templating.taglib.TagUtil;
import com.vignette.ext.templating.util.PageUtil;
import com.vignette.ext.templating.util.RequestContext;

import com.ieci.cepsa17.util.perfilado.CepsaLdapRequest;
import com.ieci.cepsa17.util.perfilado.LDapUser;

public class CheckLdapSuscriptionMetaTagSupport extends TagUtil {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String LDAPATRIBUTO_UID = "username";
	
	private static Logger logger = Logger.getLogger(CheckLdapSuscriptionMetaTagSupport.class);
	
	PageContext pageContext = null;
	RequestContext requestContext = null;
	
	private int varScope = 1;
	
	private String varName = null;
	private String ldapAttributeValue = null;
	private String uid = null;
	
	public PageContext getPageContext() {
		return pageContext;
	}

	public void setPageContext(PageContext pageContext) {
		this.pageContext = pageContext;
		this.requestContext = PageUtil.getCurrentRequestContext(pageContext);
		this.requestContext.setPageContext(this.pageContext);
	}

	public RequestContext getRequestContext() {
		return requestContext;
	}

	public void setRequestContext(RequestContext requestContext) {
		this.requestContext = requestContext;
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	public int getVarScope() {
		return varScope;
	}
	

	public String getLdapAttributeValue() {
		return ldapAttributeValue;
	}

	public void setLdapAttributeValue(String ldapAttributeValue) {
		this.ldapAttributeValue = ldapAttributeValue;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public void setVarScope(String scope) throws JspException {
		
		if (scope.equalsIgnoreCase("page")) {
			this.varScope = 1;
		} else if (scope.equalsIgnoreCase("request")) {
			this.varScope = 2;
		} else if (scope.equalsIgnoreCase("session")) {
			this.varScope = 3;
		} else if (scope.equalsIgnoreCase("application")) {
			this.varScope = 4;
		} else {
			throw new JspException("Invalid scope: " + scope);
		}
		
	}
	
	protected void printString(String s)
			throws ApplicationException
	{
		this.requestContext.printString(s);
	}
	
	public int doStartTag()
			throws JspException
	{
		logger.debug("CheckLdapSuscriptionMetaTagSupport doStartTag");
		PageContext pc = requestContext.getPageContext();
		//cepsaWebProperty:AR_ConsultaPedidos_USER_NIF
		logger.debug("CheckLdapSuscriptionMetaTagSupport getLdapAttributeValue: " + getLdapAttributeValue());
		String[] attrValues = getLdapAttributeValue().split(":");
		if (attrValues.length!=2){
			logger.debug("CheckLdapSuscriptionMetaTagSupport getLdapAttributeValue is not correct, return 0 ");
			pc.setAttribute(this.varName, "0", this.varScope);
			this.requestContext.setPageContext(pc);
			return SKIP_BODY;
		}
		
		if (uid==null){
			logger.debug("CheckLdapSuscriptionMetaTagSupport gettting uid....");
			IProfile profile = requestContext.getProfile();
			uid=(String) profile.getAttribute(LDAPATRIBUTO_UID);
		}
		
		logger.debug("CheckLdapSuscriptionMetaTagSupport uid: " + uid);
		
		// CALL LDAP method
		String suscripted = "0";
		
		try {
				
		LDapUser user = CepsaLdapRequest.consultarUsuario(uid);
		if(user == null) {
			logger.error("CheckLdapSuscriptionMetaTagSupport error conexion LDAP user==null");
		}
		else {
			logger.error("Consultando usuario : " + user.completeToString());
			List<Map<String,String>> listaWebProperties = null;
			logger.debug("CheckLdapSuscriptionMetaTagSupport buscar en atributo: " + attrValues[0]);
			
			if ("cepsaWebProperty".equals(attrValues[0])) {
				listaWebProperties = user.getWebProperty();
				logger.debug("CheckLdapSuscriptionMetaTagSupport buscar listaWebProperties class: " + listaWebProperties.getClass().getName());
			}
			
			logger.debug("CheckLdapSuscriptionMetaTagSupport buscamos el valor: " + attrValues[1]);

			if (!listaWebProperties.isEmpty()) {
				logger.debug("CheckLdapSuscriptionMetaTagSupport buscar listaWebProperties class: " + listaWebProperties.getClass().getName());
				Iterator<Map<String,String>> itr = listaWebProperties.iterator();
			    while (itr.hasNext()) {
			      Map<String,String> element = itr.next();
			      logger.debug("CheckLdapSuscriptionMetaTagSupport atributo name: " + element.get("name"));
			      if (attrValues[1].equals(element.get("name"))) {
						suscripted= "1";
						logger.debug("CheckLdapSuscriptionMetaTagSupport LDAP suscripted");
						break;
					}
			    }
			   
			    
					
				}
			}
		
		} catch (Exception e) {
			logger.debug("Error",e);
		}

		logger.debug("CheckLdapSuscriptionMetaTagSupport doStartTag " + suscripted + " " + this.varScope);
		pc.setAttribute(this.varName, suscripted, this.varScope);
					
		this.requestContext.setPageContext(pc);
		return SKIP_BODY;
	}

	public int doEndTag()
			throws JspException
	{
		this.varName = null;
		this.ldapAttributeValue = null;
		this.uid = null;
		this.requestContext = null;
		return EVAL_PAGE;
	}
	
}
