package com.ieci.cepsa17.wem.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.log4j.Logger;

import com.vignette.as.client.exception.ApplicationException;
import com.vignette.ext.templating.taglib.TagUtil;
import com.vignette.ext.templating.util.PageUtil;
import com.vignette.ext.templating.util.RequestContext;

import com.ieci.cepsa17.wem.comun.util.CIUtil;

public class LiteralTagSupport extends TagUtil {
	
	private static Logger logger = Logger.getLogger(LiteralTagSupport.class);
	
	PageContext pageContext = null;
	RequestContext requestContext = null;
	
	private String varName = null;
	private String literal = null;
	private String defecto = null;
	private String idioma = null;
	private int varScope = 1;
	
	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public RequestContext getRequestContext() {
		return requestContext;
	}

	public void setRequestContext(RequestContext requestContext) {
		this.requestContext = requestContext;
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	public String getLiteral() {
		return literal;
	}

	public void setLiteral(String literal) {
		this.literal = literal;
	}

	public String getDefecto() {
		return defecto;
	}

	public void setDefecto(String defecto) {
		this.defecto = defecto;
	}

	public PageContext getPageContext() {
		return pageContext;
	}

	public void setPageContext(PageContext pageContext)
	{
		this.pageContext = pageContext;
		this.requestContext = PageUtil.getCurrentRequestContext(pageContext);
		this.requestContext.setPageContext(this.pageContext);
	}
	 
	public void setScope(String scope)
			throws JspException
	{
		if (scope.equalsIgnoreCase("page")) {
			this.varScope = 1;
		} else if (scope.equalsIgnoreCase("request")) {
			this.varScope = 2;
		} else if (scope.equalsIgnoreCase("session")) {
			this.varScope = 3;
		} else if (scope.equalsIgnoreCase("application")) {
			this.varScope = 4;
		} else {
			throw new JspException("Invalid scope: " + scope);
		}
	}
	  
	protected void printString(String s)
			throws ApplicationException
	{
		this.requestContext.printString(s);
	}
	  
	public int doStartTag()
			throws JspException
	{
		PageContext pc = requestContext.getPageContext();
		try
		{
			if (logger.isDebugEnabled()) logger.debug("Recuperando literal: " + this.literal + "(" + this.defecto + ")");
			String value = CIUtil.getLiteral(this.requestContext, this.literal, this.defecto, this.idioma);
			
			if (this.varName == null) {
				if (logger.isDebugEnabled()) logger.debug("Escribiendo literal: " + this.literal + "=" + value);
				printString(value);
			} else {
				if (logger.isDebugEnabled()) logger.debug("Estableciendo variable: " + this.varName + " = " + value + " - scope: " + this.varScope);
				pc.setAttribute(this.varName, value, this.varScope);
			}
		}
		catch (ApplicationException ae)
		{
			throw new JspException(this.requestContext.getMessage("21"), ae);
		}
		this.requestContext.setPageContext(pc);
		return SKIP_BODY;
	}

	public int doEndTag()
			throws JspException
	{
		this.varName = null;
		this.literal = null;
		this.defecto = null;
		this.idioma = null;
		this.requestContext = null;
		return EVAL_PAGE;
	}
}
