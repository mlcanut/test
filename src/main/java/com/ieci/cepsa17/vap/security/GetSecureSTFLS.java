package com.ieci.cepsa17.vap.security;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.epicentric.common.website.SessionUtils;
import com.epicentric.user.User;




public class GetSecureSTFLS extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(GetSecureSTFLS.class);
	private static final String propertiesFileName = "es.ieci.cepsa.utilities.common.vap.GetSecureSTFLS";
	protected static ResourceBundle bundle;
	protected static MimetypesFileTypeMap mt =  new MimetypesFileTypeMap();
	
	private static final String CLEAN_GROUPS_START = "cn=";
	private static final String CLEAN_GROUPS_END = ",ou=ZONAS,o=WACcepsa,dc=cepsaweb,dc=es";
	
	       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetSecureSTFLS() {
        super();

    }
    
    private String cleanGroup(String group){
    	if (group==null) return group;
    	
		return group.replaceFirst(CLEAN_GROUPS_START, "").replaceFirst(CLEAN_GROUPS_END, "");
		
	}
    
    /**
	 * @see HttpServlet#init(ServletConfig config)
	 */
    public void init(ServletConfig config) throws ServletException{
    
    	
    	logger.debug("Inicializando GetSecureSTFLS..... ");
    	super.init(config);
    	
    	bundle = ResourceBundle.getBundle(propertiesFileName);
    	if (bundle == null) {
            throw new ServletException("Fichero de propiedades '" + propertiesFileName + "' no encontrado en el classpath");
        }

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		logger.debug("GetSecureSTFLS - do GET.......");
		
		HttpSession session = request.getSession(true);
		User user = SessionUtils.getCurrentUser(session);
		logger.debug("GetSecureSTFLS - user: " + user);
		if (user==null){
			logger.debug("El usuario NO está autenticado no tiene acceso al recurso ");
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return;
		}
		
		Set<String> properties = user.getPropertyIDs();
		for(String i : properties)
			logger.debug("GetSecureSTFLS - property: " + i);
		
		String groupMembership = null;
		Set<String> groups = new HashSet<String>();
		try {
			if (user.getProperty("cepsaWebMemberOf") != null){
				groupMembership = user.getProperty("cepsaWebMemberOf").toString();
				String att_value [] = groupMembership.split("\\|");
				for (int i = 0; i < att_value.length; i++){
					String group = cleanGroup(att_value [i]);
					
					logger.debug("El usuario " + user.getDisplayName() + " pertenece al grupo " + group);
					groups.add(group);							
				}
			}
		}catch ( com.epicentric.entity.PropertyNotFoundException e){
			logger.error("Error al recuperar la property cepsaWebMemberOf", e);
		}
		
		//Construimos un treeSet para conseguir case insesitive
		Set<String> userGroups = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
		userGroups.addAll(groups);
		
		String fileName = request.getParameter(bundle.getString("fileNameRequestProperty"));
		logger.debug("GetSecureSTFLS - fileName: " + fileName);
		try {			
			if (request.getCharacterEncoding()== null){
				byte bytes[] = new byte[fileName.length()];
				for(int i = 0; i < bytes.length; i++)
					bytes[i] = (byte) fileName.charAt(i);
				fileName = new String(bytes, "UTF-8");
			}

		} catch (UnsupportedEncodingException e) {
			logger.error("Error recuperando nombre del fichero. Codificaci�n no soportada", e);
		}
		
		//Call JSP crosscontext
		logger.debug("GetSecureSTFLS - fileName: " + fileName);
		request.setAttribute("uriPathPrefix", bundle.getString("uriPathPrefix"));
		request.setAttribute("filePath", fileName);
		request.setAttribute("groups", userGroups);
		request.setAttribute("fileSystemDirectory", bundle.getString("fileSystemDirectory"));
		ServletContext templatingServletContext = request.getSession().getServletContext().getContext(bundle.getString("dpm.context"));
		try {
			templatingServletContext.getRequestDispatcher(bundle.getString("dpm.servlet")).forward(request, response);
		} catch (ServletException e) {
			logger.error("Error haciendo el cross context", e);
		} catch (IOException e) {
			logger.error("Error haciendo el cross context", e);
		}
				
				
	}
	
    	

}
