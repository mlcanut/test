package com.ieci.cepsa17.vap.comun.util;

import java.net.URLDecoder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ieci.cepsa17.util.I18nHttpConstants;

import javax.servlet.http.HttpServletRequest;

public class VapUtil {

	/** The logger class*/
	private static final Log logger = LogFactory.getLog(VapUtil.class);
	
	
	/**
	 * Desde el area publica se envia a la privada el parametro origen con valor comercial:SEGMENT
	 * Este metodo tarta este valor y devuelve el valor de segmento
	 * @param origin
	 * @return segmento de origen
	 */
	public static String getSegmentFormComercialOrigin(String origin){
		String origenSegmento ="particular";
		String origen = "";
		String[] origenValue = origin.split(":");
		logger.debug("origenValue.length:"+origenValue.length);
		if (origenValue.length >1 ){
			origenSegmento = origenValue[1];
			origen = origenValue[0];
			
		}
		logger.debug("origen:"+origen+" origenSegmento:"+origenSegmento);
		return origenSegmento;
	}
	/**
	 * Obtenemos de la request el idioma, si no lo encuentre lo busca en el parametro TARGET de siteminder
	 * @param request
	 * @return
	 */
	public static String getLangParam(HttpServletRequest request){
		String lang=null;
		String langParamKey=I18nHttpConstants.I18N_LANGREQUESTKEY;
		try{
			if(request!=null){
				lang = request.getParameter(langParamKey);
				if(lang!=null) 
					return lang;
				else{ // Redirect from SM
					lang=getParamFromSMTarget(request,langParamKey);			
				}
			}
			
		}catch(Exception e){
			logger.error("ERROR localizando el parametro 'lang': "+e.getMessage());
			logger.error(e);
		}	
		return lang;
	}
	
	/**
	 * Obtenemos el parametro origen de la request, si no lo encuentra lo busca en el parametro TARGET de siteminder
	 * @param request
	 * @return
	 */
	public static String getOrigenAcceso(HttpServletRequest request){
		String origen=null;
		try{
			if(request!=null){
				origen = request.getParameter("origen");
				if(origen!=null) 
					return origen;
				else{ // Redirect from SM
					origen=getParamFromSMTarget(request,"origen");	
				}
			}
		}catch(Exception e){
			logger.error("ERROR localizando el parametro 'origen': "+e.getMessage());
			logger.error(e);
		}	
		return origen;
	}
	
	/**
	 * Obtiene un parametro del parametro TARGET de siteminder
	 * @param request
	 * @param paramName
	 * @return
	 */
	public static String getParamFromSMTarget(HttpServletRequest request, String paramName){
		String paramValue=null;
		String target=request.getParameter("TARGET");
		try{
			if(target!=null && target.contains(paramName)){
				// TARGET=-SM-HTTPS://priv.cepsa.com/portal/site/cepsa/template.SEND_ACTIVATION_EMAIL?lang_choosen=en&origen=comercial						
				
				String query = target.substring(target.indexOf("?")+1);
				for (String param : query.split("&")) {
					String[] pair = param.split("=");
					String key = URLDecoder.decode(pair[0], "UTF-8");
					String value = "";
	                if (pair.length > 1)             
	                	value = URLDecoder.decode(pair[1], "UTF-8");
	                
	                if(paramName.equals(key) && value!=null){      	
	                	paramValue=value;
	                	break;
	                }
				}
				
				//System.out.println("LanguageManagerPreDisplayAction.getParamFromSMTarget() --> "+paramName+": '" + paramValue+"'");
				if (logger.isDebugEnabled())
					logger.debug("TRACE --> "+paramName+": '" + paramValue+"'");
			
			}
		}catch(Exception e){
			logger.error("ERROR localizando el parametro '"+paramName+"': "+e.getMessage());
			logger.error(e);
		}		
		
		return paramValue;
	}
	
}
