package com.ieci.cepsa17.vap.comun.util;

import com.vignette.as.client.exception.ApplicationException;
import com.vignette.ext.templating.util.RequestContext;
import com.vignette.ext.templating.util.XSLPageUtil;

public class XSLUtils {
	 
	 public String buildLinkURI(RequestContext rc, String vcmId){
		 String url="";
		 try {
			url = XSLPageUtil.buildLinkURI(rc,vcmId,"","");
		} catch (ApplicationException e) {
			System.out.println("XSLUtils.buildLinkURI() --> "+e.getMessage());
			e.printStackTrace();
		}
		return url;
	 }
}
